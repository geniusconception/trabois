<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 13/07/2017
 * Time: 22:41
 */

namespace ActivityBundle\Block;

use Spy\Timeline\Notification\Unread\UnreadNotificationManager;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Spy\Timeline\Driver\ActionManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\BlockBundle\Block\BlockContextInterface;

class ActivityBlock extends AbstractBlockService{

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    /**
     * @var ActionManagerInterface
     */
    protected $actionManager;
    /**
     * @var UnreadNotificationManager
     */
    protected $unreadNotificationManager;
    /**
     * @var array
     */
    protected $contexts;

    public function __construct(ActionManagerInterface $actionManagerInterface,UnreadNotificationManager $unreadNotificationManager, TokenStorageInterface $tokenStorageInterface, array $contexts,$name = null, EngineInterface $templating = null)
    {
        parent::__construct($name, $templating);
        $this->actionManager = $actionManagerInterface;
        $this->unreadNotificationManager = $unreadNotificationManager;
        $this->tokenStorage = $tokenStorageInterface;
        $this->contexts = $contexts;
    }

    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'title' => "Activités récentes",
            'template' => "ActivityBundle:Block:unread.html.twig",
        ));
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null){
        $settings = $blockContext->getSettings();

        $token = $this->tokenStorage->getToken();
        if (!$token) {
            return new Response();
        }

        $subject = $this->actionManager->findOrCreateComponent($token->getUser());

        $count = $this->unreadNotificationManager->countKeys($subject);

        $entries = $this->unreadNotificationManager->getUnreadNotifications($subject);

        return $this->renderPrivateResponse($settings['template'],array(
            "actions" => $entries,
            "title" => $settings['title'],
            "count" => $count,
        ),$response);

    }
} 