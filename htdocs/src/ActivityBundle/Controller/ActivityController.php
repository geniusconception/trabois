<?php

namespace ActivityBundle\Controller;

use AppBundle\Controller\CoreController;

class ActivityController extends CoreController
{
    const MAX = 50;

    public function activitiesAction($page = 1)
    {
        $user  = $this->getUser();
        /** @var \Spy\Timeline\Driver\ActionManagerInterface $actionManager */
        $actionManager   = $this->get('spy_timeline.action_manager');
        /** @var \Spy\Timeline\Driver\TimelineManagerInterface $timelineManager */
        $timelineManager = $this->get('spy_timeline.timeline_manager');
        $subject         = $actionManager->findOrCreateComponent($user);

        $page = ($page > 0)? $page: 1;
        $timeline = $timelineManager->getTimeline($subject,[
            'page' => $page,
            'max_per_page' => self::MAX,
            'paginate' => true,
        ]);
        $count = ceil(count($timeline)/self::MAX);
        return $this->render('ActivityBundle:Timeline:list.html.twig',[
            "timeline" => $timeline,
            "page" => $page,
            "pages" => $count,
            "contexts" => $this->getParameter("activity_contexts"),
        ]);
    }
}
