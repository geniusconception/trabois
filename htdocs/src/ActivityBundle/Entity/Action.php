<?php

namespace ActivityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Spy\TimelineBundle\Entity\Action as BaseAction;
/**
 * Action
 *
 * @ORM\Table(name="action")
 * @ORM\Entity(repositoryClass="ActivityBundle\Repository\ActionRepository")
 */
class Action extends BaseAction
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="ActionComponent", mappedBy="action", cascade={"persist"})
     */
    protected $actionComponents;

    /**
     * @ORM\OneToMany(targetEntity="Timeline", mappedBy="action")
     */
    protected $timelines;
}