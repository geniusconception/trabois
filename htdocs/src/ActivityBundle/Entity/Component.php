<?php

namespace ActivityBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Spy\TimelineBundle\Entity\Component as BaseComponent;
/**
 * Component
 *
 * @ORM\Table(name="component")
 * @ORM\Entity(repositoryClass="ActivityBundle\Repository\ComponentRepository")
 */
class Component extends BaseComponent
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
}