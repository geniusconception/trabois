<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 16/07/2017
 * Time: 00:18
 */

namespace ActivityBundle\Manager;

use Spy\Timeline\Notification\Unread\UnreadNotificationManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Spy\Timeline\Driver\ActionManagerInterface;

class UnreadManager implements EventSubscriberInterface{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    /**
     * @var ActionManagerInterface
     */
    protected $actionManager;
    /**
     * @var UnreadNotificationManager
     */
    protected $unreadNotificationManager;

    function __construct(ActionManagerInterface $actionManager,TokenStorageInterface $tokenStorage,UnreadNotificationManager $unreadNotificationManager)
    {
        $this->actionManager = $actionManager;
        $this->tokenStorage = $tokenStorage;
        $this->unreadNotificationManager = $unreadNotificationManager;
    }

    public function markAsReadAction(array $actions){
        $token = $this->tokenStorage->getToken();
        if(!$token)
            return;

        $subject = $this->actionManager->findOrCreateComponent($token->getUser());

        $actions = array_map(function($a) use (&$subject){
            return ['GLOBAL',$subject,$a];
        },$actions);

        $this->unreadNotificationManager->markAsReadActions($actions);
    }

    public function markAllAsRead(){
        $token = $this->tokenStorage->getToken();
        if(!$token)
            return;

        $subject = $this->actionManager->findOrCreateComponent($token->getUser());

        $this->unreadNotificationManager->markAllAsRead($subject);
    }

    public static function getSubscribedEvents()
    {
        return array(
            "kernel.request" => "handleRequest",
        );
    }

    public function handleRequest(GetResponseEvent $event){
        if($event->isMasterRequest()){
            $request = $event->getRequest()->query;
            if($action = $request->get('activity_trash')){
                if($action == "trash")
                    $this->markAsReadAction($request->get('activity_actions'));
                elseif($action == "trash_all")
                    $this->markAllAsRead();
            }
        }
    }
} 