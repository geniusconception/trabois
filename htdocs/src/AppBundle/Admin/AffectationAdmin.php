<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Affectation;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AffectationAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('role',null,array(
                    'label' => "Role"
                ),
                'choice',array(
                    'choices' => Affectation::getRoles(),
                )
            )
            ->add('position',null,array(
                    'label' => "Role"
                ),
                'choice',array(
                    'choices' => Affectation::getPositions(),
                )
            )
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('user',null,array(
                "label" => "utilisateur",
                "route" => ["name" => "show"]
            ))
            ->add('role',"choice",["label" => "Rôle","choices" => Affectation::getRoles()])
            ->add('position',"choice",["label" => "Position","choices" => Affectation::getPositions()])
            ->add('exploitant',null,['label' => "Exploitant"])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('user',null,array(
                "label" => "Utilisateur",
            ))
            ->add('role','sonata_type_choice_field_mask',[
                "label" => "Rôle",
                "choices" => Affectation::getRoles(),
                "map" => [
                    Affectation::ROLE_EXPLOITANT => ["exploitant"]
                ]
            ])
            ->add('exploitant',null,['label' => "Exploitant"])
            ->add('position',"choice",["label" => "Position","choices" => Affectation::getPositions()])
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('user',null,array(
                "label" => "utilisateur",
                "route" => ["name" => "show"]
            ))
            ->add('role',"choice",["label" => "Rôle","choices" => Affectation::getRoles()])
            ->add('position',"choice",["label" => "Position","choices" => Affectation::getPositions()])
            ->add('exploitant',null,['label' => "Exploitant"])
        ;
    }

    public function preUpdate($object)
    {
        $this->setUserRole($object);
    }

    public function prePersist($object)
    {
        $this->setUserRole($object);
    }

    public function preRemove($object)
    {
        /** @var Affectation $object */
        $this->removeUserRole($object);
        $this->getModelManager()->update($object->getUser());
    }


    private function removeUserRole(Affectation $a){
        $roles = $a->getUser()->getRoles();
        for($i=0; $i < count($roles); $i++) {
            if(strpos($roles[$i],"ROLE_AFFECTED_") == 0){
                unset($roles[$i]);
            }
        }
        $a->getUser()->setRoles($roles);
    }

    private function setUserRole($object){
        $this->removeUserRole($object);
        /** @var Affectation $object */
        $object->getUser()->addRole($object->getAffectedRole());
        $object->setRole($object->getRole());
    }


}
