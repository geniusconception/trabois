<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 03/06/2017
 * Time: 12:44
 */

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;

abstract class ReversedOrderAdmin extends AbstractAdmin{

    public function __construct($code, $class, $baseControllerName)
    {
        parent::__construct($code, $class, $baseControllerName);

        $this->datagridValues['_sort_order'] = 'DESC';
    }


} 