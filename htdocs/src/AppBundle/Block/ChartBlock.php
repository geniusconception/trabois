<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 01/06/2017
 * Time: 21:45
 */

namespace AppBundle\Block;

use AppBundle\Model\ChartPool;
use Mukadi\ChartJSBundle\Model\Chart;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ChartBlock extends AbstractBlockService{

    /**
     * @var ChartPool
     */
    protected $pool;

    public function __construct($name = null, EngineInterface $templating = null, ChartPool $pool)
    {
        parent::__construct($name, $templating);
        $this->pool = $pool;
    }


    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'title' => "insert a title",
            'chart_title'  => array(
                "display" => true,
                "text" => "Sample Chart",
                "position" => "bottom"
            ),
            'chart' => "",
            'chart_type' => Chart::BAR,
            'chart_options' => array(),
            'template' => "AppBundle:Block:chart.html.twig",
        ));
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null){
        $settings = $blockContext->getSettings();
        $chart = $this->pool->getChart($settings['chart'],$settings['chart_type'],$settings['chart_options']);
        return $this->renderPrivateResponse($settings['template'],array(
            "chart" => $chart,
            "title" => $settings['title'],
        ),$response);
    }


} 