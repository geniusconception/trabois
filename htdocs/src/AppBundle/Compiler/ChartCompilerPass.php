<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 02/06/2017
 * Time: 12:14
 */

namespace AppBundle\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ChartCompilerPass implements CompilerPassInterface{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if(!$container->has("app.chart_pool")){
            return;
        }
        $def = $container->findDefinition("app.chart_pool");

        $tagged = $container->findTaggedServiceIds("app.chart");

        foreach ($tagged as $id => $tags) {
            foreach ($tags as $attr) {
                $def->addMethodCall("setChart",array($attr["id"], new Reference($id)));
            }
        }
    }


} 