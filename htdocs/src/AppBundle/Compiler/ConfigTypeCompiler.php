<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 27/06/2017
 * Time: 13:08
 */

namespace AppBundle\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ConfigTypeCompiler implements CompilerPassInterface{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if(!$container->has("app.config")){
            return;
        }
        $def = $container->findDefinition("app.config");

        $tagged = $container->findTaggedServiceIds("app.config_type");

        foreach ($tagged as $id => $tags) {
            foreach ($tags as $attr) {
                $def->addMethodCall("addType",array($attr["alias"], new Reference($id)));
            }
        }
    }


} 