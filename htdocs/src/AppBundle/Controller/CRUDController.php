<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 09/06/2017
 * Time: 16:18
 */

namespace AppBundle\Controller;

use AppBundle\Exception\ImportsException;
use Sonata\AdminBundle\Controller\CRUDController as CRUD;
use Sonata\AdminBundle\Exception\ModelManagerException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\PropertyAccess\PropertyPath;
use Symfony\Component\Validator\Constraints as Assert;
use AppBundle\Model\AvancedImportsInterface;

class CRUDController extends CRUD{

    const DELIMITER = ";";

    public function importAction(Request $request){

        $this->admin->checkAccess('create');

        $data = array(
            'file' => null,
        );
        $form = $this
            ->createFormBuilder($data)
            ->setMethod(Request::METHOD_POST)
            ->add('file','file',array(
                'label' => "Fichier CSV",
                'required' => true,
                'constraints' => array(
                    new Assert\File(array(
                        'mimeTypes' => array(
                            "text/csv",
                            "text/plain",
                            "application/vnd.ms-excel",
                        ),
                        'mimeTypesMessage' => ' veuillez soumettre un fichier CSV valide',
                    ))
                )
            ))
            ->getForm()
        ;
        $form->handleRequest($request);
        $args = array();
        if($form->isSubmitted() && $form->isValid()){
            $file = $form->getData();
            /** @var \Symfony\Component\HttpFoundation\File\UploadedFile $file */
            $file = $file['file'];
            try{
                $count = $this->bulkCreateFrom($file->getRealPath());
                $this->addFlash('sonata_flash_success',sprintf('importation réussi ! %d éléments ajoutés',$count));
                return $this->redirect($this->admin->generateUrl('list'));
            }catch (ImportsException $e){
                $args['errors'] = $e->getErrors();
            }
        }
        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->getTemplate('import'),array_merge(array(
            'action' => 'edit',
            'form' => $view,
            'hint' => $this->getHintTemplate(),
        ),$args));
    }

    public function importSampleAction(){
        $csv = $this->getCsvHeader();
        $response = new Response($this->arrayToCsv($csv));
        $disp = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $this->admin->getCode().'.csv'
        );
        $response->headers->set('Content-Disposition', $disp);
        $response->headers->set('Content-Type', 'text/csv');

        return $response;
    }

    public function statsAction(){
        $this->admin->checkAccess('list');

        $datagrid = $this->admin->getDatagrid();
        $formView = $datagrid->getForm()->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($formView, $this->admin->getFilterTheme());
        /** @var \StatsBundle\Model\StatsPool $pool */
        $pool = $this->get('stats.stat_pool');

        return $this->render("StatsBundle:CRUD:stats.html.twig",[
            'form' => $formView,
            'stats' => $pool->getAdminStats($this->admin->getCode()),
            'action' => 'stats'
        ]);
    }

    public function generateStatsAction(Request $request){
        $this->admin->checkAccess('list');

        /** @var \StatsBundle\Model\StatsPool $pool */
        $pool = $this->get('stats.stat_pool');

        $name = $request->query->get('get_stats');
        if(!$pool->statsExist($name))
            throw $this->createNotFoundException("Le rapport demandé est introuvable");

        $stats = $pool->getStats($name);
        $datagrid = $this->admin->getDatagrid();

        $view = $stats->buildStats($datagrid->getQuery());

        return $this->render("StatsBundle:CRUD:get_stats.html.twig",[
            'action' => 'generateStats',
            'view' => $view,
            'title' => $stats->getTitle(),
        ]);
    }

    private function arrayToCsv( array &$fields, $delimiter = self::DELIMITER, $enclosure = '"', $encloseAll = false, $nullToMysqlNull = false ) {
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        foreach ( $fields as $field ) {
            if ($field === null && $nullToMysqlNull) {
                $output[] = 'NULL';
                continue;
            }

            // Enclose fields containing $delimiter, $enclosure or whitespace
            if ( $encloseAll || preg_match( "/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field ) ) {
                $output[] = $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure;
            }
            else {
                $output[] = $field;
            }
        }

        return implode( $delimiter, $output );
    }

    /**
     * @param string
     * @throws ImportsException
     * @return integer
     */
    private function bulkCreateFrom($filePath){
        $row = 0;
        set_time_limit(0);
        $header = $this->getCsvHeader();
        if(($f = fopen($filePath,'r')) !== FALSE){
            /*- vérification de l'entête -*/
            if(($data = fgetcsv($f,0,self::DELIMITER)) === FALSE)
                throw new ImportsException(array("Aucune donnée trouvée dans le fichier"));
            else{
                $c = count($header);
                if($c != count($data))
                    throw new ImportsException(array(
                        "L'entête de votre fichier n'a pas la bonne taille, veuillez consulter le fichier exemple"
                    ));
                for($i=0 ;$i<$c ;$i++){
                    if($data[$i] != $header[$i])
                        throw new ImportsException(array(
                            "L'entête de votre fichier n'est pas conforme, veuillez consulter le fichier exemple"
                        ));
                    else
                        continue;
                }
                /*- si l'entête est correcte -*/
                while(($data = fgetcsv($f,0,self::DELIMITER)) !== FALSE){
                    $object = $this->getNewInstance();
                    if($this->admin instanceof AvancedImportsInterface){
                        $d = $this->admin->preHydrate($object,$header,$data);
                        if(is_array($d)){
                            $data = $d;
                            unset($d);
                        }
                        $object = $this->hydrateObject($object,$header,$data);
                        $this->admin->postHydrate($object,$header);
                    }else
                        $object = $this->hydrateObject($object,$header,$data);

                    $errors = $this->get('validator')->validate($object);
                    if(count($errors) > 0){
                        $e = new ImportsException();
                        foreach ($errors as $error) {
                            $e->addError(sprintf("Ligne %d: %s",($row+1),$error->getMessage()));
                        }
                        throw $e;
                    }else{
                        try{
                            $this->admin->create($object);
                            $row++;
                        }catch (ModelManagerException $e){
                            $this->handleModelManagerException($e);
                            throw new ImportsException(array($e->getMessage()));
                        }
                    }
                }
            }
        }
        return $row;
    }

    /**
     * @param string $key
     * @return string|null
     */
    private function getTemplate($key){
        switch($key) {
            case 'import':
                return "AppBundle:CRUD:import.html.twig";
            default:
                return null;
        }
    }

    /**
     * @return array
     */
    private function getCsvHeader(){
        $interface = '\AppBundle\Model\EnableImportsInterface';
        return ($this->admin instanceof $interface)? $this->admin->configureImportFields() : array('id');
    }

    /**
     * @param $object
     * @param array $fields
     * @param array $data
     * @return object
     */
    private function hydrateObject($object,array $fields, array $data){
        $i = 0;
        foreach ($fields as $f) {
            $setter = str_replace('_',' ',$f);
            $setter = 'set'.str_replace(' ','',ucwords($setter));
            if(method_exists($object,$setter)){
                $object->$setter($data[$i]);
            }
            $i++;
        }
        return $object;
    }

    protected function getHintTemplate(){
        if(($this->admin instanceof AvancedImportsInterface) && ($template = $this->admin->getHintTemplate())) {
            return $template;
        }else{
            return "AppBundle:CRUD:import_hint.html.twig";
        }
    }

    protected function getNewInstance(){
        $object = $this->admin->getNewInstance();
        /** @var \Sonata\AdminBundle\Admin\AbstractAdmin $admin */
        $admin = $this->admin;
        if($admin->isChild() && $admin->getParentAssociationMapping()){
            $parent = $admin->getParent()->getObject($this->getRequest()->get($admin->getParent()->getIdParameter()));
            $propertyAccessor = $admin->getConfigurationPool()->getPropertyAccessor();
            $propertyPath = new PropertyPath($admin->getParentAssociationMapping());

            $value = $propertyAccessor->getValue($object, $propertyPath);
            if (is_array($value) || ($value instanceof \Traversable && $value instanceof \ArrayAccess)) {
                $value[] = $parent;
                $propertyAccessor->setValue($object, $propertyPath, $value);
            } else {
                $propertyAccessor->setValue($object, $propertyPath, $parent);
            }
        }

        return $object;
    }
} 