<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 27/06/2017
 * Time: 15:25
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class CoreController extends Controller{


    public function configAction(Request $request){
        if(!$this->isGranted("ROLE_CONFIG") && !$this->isGranted('ROLE_SUPER_ADMIN'))
            throw $this->createAccessDeniedException("Vous n'êtes pas authoriser à accéder à cette page");
        /** @var \AppBundle\Model\Configurator $config */
        $config = $this->get('app.config');
        $form = $config->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $config->saveData($form);
            $this->addFlash('sonata_flash_success',"parametrès enregistrés !");
        }

        $view = $form->createView();
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->getFormTheme());

        return $this->render("AppBundle:Config:form.html.twig",array(
            'form' => $view,
        ));
    }

    private function getBaseTemplate(){
        return $this->get('sonata.admin.pool')->getTemplate('layout');
    }

    public function render($view, array $parameters = array(), Response $response = null)
    {
        $parameters = array_merge(array(
            "base_template" => $this->getBaseTemplate(),
        ),$parameters);
        return parent::render($view, $parameters, $response);
    }

    private function getFormTheme(){
        return array(
            "SonataDoctrineORMAdminBundle:Form:form_admin_fields.html.twig"
        );
    }
} 