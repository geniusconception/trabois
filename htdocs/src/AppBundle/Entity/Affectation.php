<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Affectation
 *
 * @ORM\Table(name="affectation")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AffectationRepository")
 * @UniqueEntity("user")
 */
class Affectation
{
    const ROLE_EXPLOITANT = "EXPLOITANT";
    const ROLE_DIAF = "DIAF";
    const ROLE_DGF = "DGF";
    const ROLE_MINISTRY = "MINISTRY";
    const ROLE_SG = "SG";

    const POSITION_MASTER = "MASTER";
    const POSITION_OPERATOR = "OPERATOR";
    const POSITION_DIRECTOR = "DIRECTOR";
    const POSITION_CHEF = "CHEF";

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=25)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=25)
     */
    private $position;
    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\OneToOne(targetEntity="Application\Sonata\UserBundle\Entity\User",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;
    /**
     * @var \CorporateBundle\Entity\Entreprise
     *
     * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
     * @ORM\JoinColumn(nullable=true)
     */
    private $exploitant;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Affectation
     */
    public function setRole($role)
    {
        $this->role = $role;
        if($role !== self::ROLE_EXPLOITANT)
            $this->exploitant = null;
        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return Affectation
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    public static function getRoles(){
        return [
            self::ROLE_EXPLOITANT => "Exploitant",
            self::ROLE_DIAF => "DIAF",
            self::ROLE_DGF => "DGF",
            self::ROLE_MINISTRY => "Ministère",
            self::ROLE_SG => "Sec Gen.",
        ];
    }

    public static function getPositions(){
        return [
            self::POSITION_DIRECTOR => "Directeur",
            self::POSITION_CHEF => "Chef",
            self::POSITION_MASTER => "Superviseur",
            self::POSITION_OPERATOR => "Opérateur",
        ];
    }

    public function getAffectedRole(){
        return sprintf("ROLE_AFFECTED_%s_%s",strtoupper($this->role),strtoupper($this->position));
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Affectation
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set exploitant
     *
     * @param \CorporateBundle\Entity\Entreprise $exploitant
     * @return Affectation
     */
    public function setExploitant(\CorporateBundle\Entity\Entreprise $exploitant = null)
    {
        $this->exploitant = $exploitant;

        return $this;
    }

    /**
     * Get exploitant
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getExploitant()
    {
        return $this->exploitant;
    }

    function __toString()
    {
        return ($this->getId())? sprintf("%s - %s (%s)",$this->user,$this->role,$this->position):"n-a";
    }


}
