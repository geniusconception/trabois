<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Compteur
 *
 * @ORM\Table(name="compteur")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CompteurRepository")
 */
class Compteur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type_compteur", type="string", length=10, unique=true)
     */
    private $typeCompteur;

    /**
     * @var string
     *
     * @ORM\Column(name="numero", type="string", length=255)
     */
    private $numero;


    public function __construct($type=null)
    {                      
        $this->typeCompteur = $type;
        $this->numero = "0000";    
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeCompteur
     *
     * @param string $typeCompteur
     * @return Compteur
     */
    public function setTypeCompteur($typeCompteur)
    {
        $this->typeCompteur = $typeCompteur;

        return $this;
    }

    /**
     * Get typeCompteur
     *
     * @return string 
     */
    public function getTypeCompteur()
    {
        return $this->typeCompteur;
    }

    /**
     * Set numero
     *
     * @param string $numero
     * @return Compteur
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Get numero
     *
     * @return string 
     */
    public function incrementCompteur()
    {
        $somme = (int)$this->numero + 1;
        $this->numero = str_pad($somme, 4, "0", STR_PAD_LEFT);//0001+1 = 0002   
        return $this->numero;

    }
}
