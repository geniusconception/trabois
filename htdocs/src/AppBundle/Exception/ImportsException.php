<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 12/06/2017
 * Time: 21:06
 */

namespace AppBundle\Exception;


use Exception;

class ImportsException extends \Exception{

    /**
     * @var array
     */
    private $errors;

    public function __construct($errors = array(),$message = "", $code = 0, Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

    public function addError($message){
        $this->errors[] = $message;
    }

    /**
     * @param array $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

} 