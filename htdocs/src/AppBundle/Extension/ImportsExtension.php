<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 09/06/2017
 * Time: 16:05
 */

namespace AppBundle\Extension;

use Sonata\AdminBundle\Admin\AbstractAdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class ImportsExtension extends AbstractAdminExtension{

    public function configureRoutes(AdminInterface $admin, RouteCollection $collection)
    {
        $collection->add('import',"import");
        $collection->add('importSample',"import/sample");
    }

    public function configureActionButtons(AdminInterface $admin, $list, $action, $object)
    {
        if($action == 'list'){
            $list['import'] = array(
                'template' => 'AppBundle:Button:action_imports.html.twig',
            );
        }
        return $list;
    }


} 