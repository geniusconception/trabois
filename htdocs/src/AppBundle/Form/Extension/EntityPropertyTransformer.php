<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 04/09/2017
 * Time: 13:19
 */

namespace AppBundle\Form\Extension;

use Symfony\Component\Form\DataTransformerInterface;
use Doctrine\ORM\EntityManagerInterface;

class EntityPropertyTransformer implements DataTransformerInterface{

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var string
     */
    private $class;
    /**
     * @var string
     */
    private $property;

    function __construct(EntityManagerInterface $em,$class,$property)
    {
        $this->em = $em;
        $this->class = $class;
        $this->property = $property;
    }

    public function transform($value)
    {
        if(!$value)
            return null;
        $out = $this->em
            ->getRepository($this->class)
            ->findOneBy([
                $this->property => $value,
            ])
            ;
        return $out;
    }

    public function reverseTransform($value)
    {
        if(!$value)
            return null;
        $out = $this->attribute($value,$this->property);
        return $out;
    }

    private function attribute($object,$name){
        if(is_array($object)){
            return $object[$name];
        }
        $name = str_replace("_"," ",$name);
        $name = str_replace(" ","",ucwords($name));
        $get = 'get'.$name;
        $is = 'is'.$name;
        if(method_exists($object,$get)){
            return $object->$get();
        }elseif(method_exists($object,$is)){
            return $object->$is();
        }else{
            return $object->$name;
        }
    }
} 