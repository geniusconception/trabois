<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 30/07/2017
 * Time: 15:10
 */

namespace AppBundle\Form\Extension;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EntityTypeExtension extends AbstractTypeExtension{

    const TYPE_CLASS='Symfony\Bridge\Doctrine\Form\Type\EntityType';
    /**
     * @var EntityManagerInterface
     */
    private $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if(isset($options['get_property'])){
            $class = $options['class'];
            $property = $options['get_property'];
            $builder->addModelTransformer(new EntityPropertyTransformer($this->em,$class,$property));
        }
    }

    public function getExtendedType()
    {
        return self::TYPE_CLASS;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefined(['get_property']);
    }

} 