<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 06/07/2017
 * Time: 15:06
 */

namespace AppBundle\Manager;

use AppBundle\Entity\Affectation;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Doctrine\ORM\EntityManagerInterface;

class AffectionManager {

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /** @var EntityManagerInterface  */
    private $em;

    function __construct(TokenStorageInterface $tokenStorage,EntityManagerInterface $em)
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
    }

    /**
     * @return Affectation
     */
    public function getAffectation(){
        return $this->em->getRepository("AppBundle:Affectation")->findOneBy(array(
            "user" => $this->tokenStorage->getToken()->getUser(),
        ));
    }

    /**
     * @return bool
     */
    public function isExploitant(){
        return $this->isRole(Affectation::ROLE_EXPLOITANT);
    }

    /**
     * @param $position
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function isPosition($position){
        $user = $this->tokenStorage->getToken()->getUser();
        $p = $this->em
            ->createQuery("SELECT a.position AS pos FROM AppBundle:Affectation a WHERE a.user = :user")
            ->setParameter("user",$user)
            ->getOneOrNullResult()
        ;
        return ($p)? current($p) == $position: false;
    }

    public function isRole($_role){
        $user = $this->tokenStorage->getToken()->getUser();
        $role = $this->em
            ->createQuery("SELECT a.role AS role FROM AppBundle:Affectation a WHERE a.user = :user")
            ->setParameter("user",$user)
            ->getOneOrNullResult()
        ;
        return ($role)? current($role) == $_role: false;
    }

    public function check($role,$position){
        $user = $this->tokenStorage->getToken()->getUser();
        $r = $this->em
            ->createQuery("SELECT a.position AS pos, a.role AS role FROM AppBundle:Affectation a WHERE a.user = :user")
            ->setParameter("user",$user)
            ->getOneOrNullResult()
        ;
        return (!empty($r))? ( $r['pos'] == $position && $r['role'] == $role): false;
    }
} 