<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 02/06/2017
 * Time: 14:05
 */

namespace AppBundle\Model;

use Doctrine\ORM\EntityManagerInterface;
use Mukadi\ChartJSBundle\Model\ChartBuilder;
use Mukadi\ChartJSBundle\Model\Worker;

abstract class AbstractChart extends ChartBuilder{

    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em, new Worker());
    }

} 