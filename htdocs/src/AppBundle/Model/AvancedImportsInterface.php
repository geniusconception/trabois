<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 07/08/2017
 * Time: 14:48
 */

namespace AppBundle\Model;



interface AvancedImportsInterface extends EnableImportsInterface{
    /**
     * @param $object
     * @param array $header
     * @param array $data
     * @return array
     */
    public function preHydrate($object, array $header,array $data);

    /**
     * @param $object
     * @param array $header
     */
    public function postHydrate($object, array $header);

    /**
     * @return string
     */
    public function getHintTemplate();
}