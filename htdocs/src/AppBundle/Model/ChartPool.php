<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 02/06/2017
 * Time: 12:04
 */

namespace AppBundle\Model;


use Mukadi\ChartJSBundle\Model\ChartBuilder;

class ChartPool {

    /**
     * @var array
     */
    protected $charts;

    public function __construct(){
        $this->charts = array();
    }

    /**
     * @param $id
     * @param ChartBuilder $cb
     */
    public function setChart($id, ChartBuilder $cb){
        $this->charts[$id] = $cb;
    }

    /**
     * @param $id
     * @return null| ChartBuilder
     */
    public function getBuilder($id){
        if(isset($this->charts[$id])){
            return $this->charts[$id];
        }
        return null;
    }

    /**
     * @param $id
     * @param null|string $type
     * @param null|array $options
     * @return \Mukadi\ChartJSBundle\Model\Chart|null
     */
    public function getChart($id,$type=null,$options=null){
        /** @var ChartBuilder $b */
        if($cb = $this->getBuilder($id)){
            return $cb->buildChart($id,$type,$options);
        }
        return null;
    }
} 