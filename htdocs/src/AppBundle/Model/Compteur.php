<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 14/06/2017
 * Time: 15:45
 */

namespace AppBundle\Model;

use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Entity\Compteur as C;


class Compteur {

    /** @var EntityManagerInterface  */
    private $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function nextValue($type){
        $c = $this->em->getRepository('AppBundle:Compteur')->findOneBy(array(
            "typeCompteur" =>$type,
        ) );
        if($c==null)
        {
            $c = new C($type);
        }

        $val = $c->incrementCompteur();

        $this->em->persist($c);
        $this->em->flush();

        return $val;
    }

} 