<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 19/06/2017
 * Time: 15:33
 */

namespace AppBundle\Model;

use AppBundle\Type\ParamType;
use AppBundle\Entity\Param;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;

class Configurator {


    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var array
     */
    private $paramters;
    /**
     * @var array
     */
    private $types;
    /**
     * @var array
     */
    private $data;
    /**
     * @var FormFactoryInterface
     */
    private $ffactory;

    public function __construct(FormFactoryInterface $ffactory,EntityManagerInterface $em,$params = array()){
        $this->em = $em;
        $this->paramters = $params;
        $this->types = array();
        $this->ffactory = $ffactory;
        $this->loadData();
    }

    private function loadData(){
        $this->data = [];
        $result = $this->em
            ->createQuery("SELECT p FROM AppBundle:Param p")
            ->getResult()
            ;
        /** @var Param $p */
        foreach ($result as $p) {
            $this->data[$p->getName()] = $p->getValue();
        }
        $data = array_map(function($e){
            return null;
        },$this->paramters);
        $this->data = array_merge($data,$this->data);
    }

    public function addType($alias, ParamType $type){
        $this->types[$alias] = $type;
    }

    public function get($key){
        if(!isset($this->data[$key]))
            return null;

        $value = $this->data[$key];

        if(isset($this->paramters[$key])){
            $config = $this->paramters[$key];
            $alias = $config['type'];
            $param = $this->getType($alias);
            $param->configure($config['options']);
            return $param->processValue($value);
        }else{
            return $value;
        }
    }

    public function saveData(FormInterface $form){
        $data = array_merge($this->data,(array)$form->getData());
        foreach ($data as $name => $value) {
            $p  = new Param($name,$value);
            $this->saveParam($p);
        }
        $this->data = $data;
    }

    private function saveParam(Param $p){
        /** @var Param $param */
        $param = $this->em->getRepository("AppBundle:Param")->findOneBy(array("name" => $p->getName()));
        if($param){
            $param->setValue($p->getValue());
        }else{
            $param = $p;
        }
        $this->em->persist($param);
        $this->em->flush();
    }

    /**
     * @return \Symfony\Component\Form\FormInterface
     */
    public function getForm(){
        $fb = $this->ffactory
            ->createBuilder('Symfony\Component\Form\Extension\Core\Type\FormType',$this->data)
            ->setMethod("POST")
        ;
        foreach ($this->paramters as $key => $config) {
            $type = $config['type'];
            $paramType = $this->getType($type);
            $paramType->configure($config['options']);
            $fb->add($key,$paramType->getFormType(),$paramType->getOptions());
            $fb->get($key)->addModelTransformer($paramType);
        }

        return $fb->getForm();
    }

    /**
     * @param $alias
     * @return ParamType
     */
    public function getType($alias){
        if(!isset($this->types[$alias]))
            throw new \InvalidArgumentException(sprintf("Unable to find Configuration Type with alias '%s'",$alias));
        return $this->types[$alias];
    }
} 