<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 10/06/2017
 * Time: 23:04
 */

namespace AppBundle\Model;


interface EnableImportsInterface {

    /**
     * @return array
     */
    public function configureImportFields();
} 