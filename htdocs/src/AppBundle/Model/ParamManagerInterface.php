<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 19/06/2017
 * Time: 15:03
 */

namespace AppBundle\Model;


interface ParamManagerInterface {

    /**
     * @param string $value
     * @return mixed
     */
    public function processValue($value);
}