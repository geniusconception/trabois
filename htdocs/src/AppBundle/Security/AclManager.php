<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 28/05/2017
 * Time: 18:32
 */

namespace AppBundle\Security;


use Application\Sonata\UserBundle\Entity\User;
use Sonata\AdminBundle\Admin\Pool;
use Sonata\AdminBundle\Security\Handler\AclSecurityHandlerInterface;
use Symfony\Component\Security\Acl\Domain\ObjectIdentity;
use Symfony\Component\Security\Acl\Domain\UserSecurityIdentity;

class AclManager {

    /** @var Pool  */
    private $adminPool;

    function __construct(Pool $adminPool)
    {
        $this->adminPool = $adminPool;
    }

    /**
     * @param Object $objet
     */
    public function saveAcl($objet, User $user = null){
        $class = get_class($objet);
        $admin = $this->adminPool->getAdminByClass($class);
        if($admin === null)
            return;
        $sec = $admin->getSecurityHandler();
        if($sec === null || ! $sec instanceof AclSecurityHandlerInterface)
            return;
        $acl = $sec->createAcl(ObjectIdentity::fromDomainObject($objet));

        if(! is_null($user)){
            $uid = UserSecurityIdentity::fromAccount($user);
            $sec->addObjectOwner($acl,$uid);
        }

        $sec->addObjectClassAces($acl,$sec->buildSecurityInformation($admin));

        try{
            $sec->updateAcl($acl);
        }catch (\Exception $e){
            return;
        }
    }
} 