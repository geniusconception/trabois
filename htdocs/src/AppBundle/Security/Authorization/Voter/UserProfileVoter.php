<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 25/06/2017
 * Time: 15:50
 */

namespace AppBundle\Security\Authorization\Voter;

use Application\Sonata\UserBundle\Entity\User;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Permission\PermissionMapInterface;
use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class UserProfileVoter extends AclVoter{

    const ROLE = "ROLE_USER_PROFILE_ONLY";
    /**
     * @var RoleHierarchyInterface
     */
    protected $rh;

    public function __construct(RoleHierarchyInterface $rh,AclProviderInterface $aclProvider, ObjectIdentityRetrievalStrategyInterface $oidRetrievalStrategy, SecurityIdentityRetrievalStrategyInterface $sidRetrievalStrategy, PermissionMapInterface $permissionMap, LoggerInterface $logger = null, $allowIfObjectIdentityUnavailable = true)
    {
        parent::__construct($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap, $logger, $allowIfObjectIdentityUnavailable);
        $this->rh = $rh;
    }


    public function supportsClass($class)
    {
        return $class == 'Application\Sonata\UserBundle\Entity\User' || $class == 'Sonata\UserBundle\Admin\Entity\UserAdmin';
    }

    public function supportsAttribute($attribute)
    {
        return $attribute === 'VIEW' || $attribute === 'EDIT' || $attribute === 'LIST' || $attribute === 'CREATE';
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }

        if(! $this->isGranted($token->getUser()))
            return self::ACCESS_ABSTAIN;

        foreach ($attributes as $attribute) {
            if ($this->supportsAttribute($attribute)) {
                /** @var \Application\Sonata\UserBundle\Entity\User  $object */
                if($object instanceof User && in_array($attribute,array('EDIT','VIEW')) && ($token->getUsername() == $object->getUsername()))
                    return self::ACCESS_GRANTED;
                if(in_array($attribute,array('LIST','CREATE')))
                    return self::ACCESS_DENIED;
            }
        }

        return self::ACCESS_ABSTAIN;
    }

    protected function isGranted(User $user){
        $role = new Role(self::ROLE);
        foreach ($user->getRoles() as $uRole) {
            if(in_array($role,$this->rh->getReachableRoles(array(new Role($uRole))))){
                return true;
            }
        }
        return false;
    }

} 