<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 29/05/2017
 * Time: 19:50
 */

namespace AppBundle\Twig;


use Symfony\Component\Security\Acl\Domain\ObjectIdentity;

class AppExtension extends \Twig_Extension
{
    public function getFunctions()
    {
        return array(
             new \Twig_SimpleFunction("app_class_id", array($this, 'classId'))
        );
    }

    public function classId($class)
    {
        return new ObjectIdentity('class', $class);
    }

    public function getName()
    {
        return 'app_twig_extension';
    }
}