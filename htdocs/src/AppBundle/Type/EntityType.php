<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 27/06/2017
 * Time: 21:58
 */

namespace AppBundle\Type;


use Doctrine\ORM\EntityManagerInterface;

class EntityType  extends ParamType{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    function __construct(EntityManagerInterface $em)
    {
        parent::__construct();
        $this->em = $em;
    }


    /**
     * @param string $value
     * @return mixed
     */
    public function processValue($value)
    {
        return $this->transform($value);
    }

    public function getFormType()
    {
        return 'Symfony\Bridge\Doctrine\Form\Type\EntityType';
    }

    public function transform($value)
    {
        if(!$value)
            return null;

        return $this->em
            ->getRepository($this->options['class'])
            ->find($value)
            ;
    }

    public function reverseTransform($value)
    {
        if(!$value)
            return null;

        return $value->getId();
    }

} 