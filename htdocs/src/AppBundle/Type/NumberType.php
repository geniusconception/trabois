<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 27/06/2017
 * Time: 21:49
 */

namespace AppBundle\Type;


class NumberType extends ParamType{

    public function transform($value)
    {
        return (double) $value;
    }


    public function reverseTransform($value)
    {
        return $value;
    }

    /**
     * @param string $value
     * @return mixed
     */
    public function processValue($value)
    {
        return (double) $value;
    }

    public function getFormType()
    {
        return 'Symfony\Component\Form\Extension\Core\Type\NumberType';
    }

} 