<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 19/06/2017
 * Time: 15:07
 */

namespace AppBundle\Type;


use AppBundle\Model\ParamManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataTransformerInterface;

abstract class ParamType implements ParamManagerInterface,DataTransformerInterface{

    /**
     * @var array
     */
    protected $options;

    function __construct()
    {
        $this->options = [];
    }


    abstract public function getFormType();

    /**
     * @param array $options
     */
    public function configure(array $options){
        $this->options = $options;
    }

    /**
     * @return array
     */
    public function getOptions(){
        return $this->options;
    }

} 