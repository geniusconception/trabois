<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 27/06/2017
 * Time: 21:35
 */

namespace AppBundle\Type;


class TextareaType extends TextType{
    public function getFormType()
    {
        return 'Symfony\Component\Form\Extension\Core\Type\TextareaType';
    }


} 