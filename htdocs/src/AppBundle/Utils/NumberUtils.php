<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 08/08/2017
 * Time: 14:00
 */

namespace AppBundle\Utils;


class NumberUtils {
    /**
     * @param $val
     * @return float
     */
    public static function toNumber($val){
        return $number = floatval(str_replace(',', '.', str_replace('.', '', $val)));
    }
} 