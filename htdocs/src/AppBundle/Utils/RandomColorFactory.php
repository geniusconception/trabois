<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 03/06/2017
 * Time: 14:40
 */

namespace AppBundle\Utils;


class RandomColorFactory {

    /**
     * @return int
     */
    public static function randomColorFactor(){
        return rand(0,255);
    }

    /**
     * @return string
     */
    public static function getRandomRGBColor(){
        return "rgb(".self::randomColorFactor().",".self::randomColorFactor().",".self::randomColorFactor().")";
    }

    /**
     * @param integer $number
     * @return array
     */
    public static function getRandomColors($number){
        $color = array();
        for($i= 0;$i<$number;$i++)
            $color[] = self::getRandomRGBColor();
        return $color;
    }
} 