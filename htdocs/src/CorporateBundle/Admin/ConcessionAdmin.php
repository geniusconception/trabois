<?php

namespace CorporateBundle\Admin;

use CorporateBundle\Entity\Concession;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use CorporateBundle\Model\ExploitantFiltred;
use Sonata\AdminBundle\Route\RouteCollection;

class ConcessionAdmin extends AbstractAdmin implements ExploitantFiltred
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('entreprise',null,['label' => "Exploitant"])
            ->add('province')
            ->add('ville')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nom',"text",array('label' => "Nom"))
            ->add('superficie',"number",array('label' => "Superficie"))
            ->add('province',null,array('label' => "Province"))
            ->add('ville',null,array('label' => "Ville"))
            ->add('entreprise',null,array('label' => "Exploitant"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nom',"text",array('label' => "Nom"))
            ->add('fichier','file',array("label" => "Fichier","required" => false))
            ->add('superficie',"number",array('label' => "Superficie"))
            ->add('province',null,array("label" => "Province","required" => true))
            ->add('ville',null,array("label" => "Ville/Territoire","required" => true))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('entreprise',null,['label' => "Exploitant"])
            ->add('nom')
            ->add('superficie')
            ->add('province')
            ->add('ville')
        ;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection){
        if(!$this->isChild())
            $collection->clearExcept(array('list','show','edit','delete','export','batch'));
    }
    
    public function getParentAssociationMapping()
    {
        return 'entreprise';
    }

    /**
     * @return string
     */
    public function getExploitantField()
    {
        return "entreprise";
    }

    public function prePersist($object)
    {
        $exp = $object->getEntreprise();
        
        $object->setEntreprise($exp);         
    }
}
