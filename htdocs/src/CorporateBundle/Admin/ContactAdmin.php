<?php

namespace CorporateBundle\Admin;

use CorporateBundle\Entity\Contact;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use CorporateBundle\Model\ExploitantFiltred;
use Sonata\AdminBundle\Route\RouteCollection;

class ContactAdmin extends AbstractAdmin implements ExploitantFiltred
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('noms')
            ->add('sexe',null,array(
                'label' => "Sexe",
                'choice',array(
                    'choices' => Contact::getTypes(),
                )
            ))
            ->add('entreprise')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('noms',null,array('label' => "Noms"))
            ->add('entreprise',null,array("label" => "Exploitant"))
            ->add('sexe',"choice",array("choices" => Contact::getTypes(),"label" => "Sexe"))
            ->add('fonction',null,array('label' => "Fonction"))
            ->add('telephone1',null,array('label' => "Téléphone 1"))
            ->add('telephone2',null,array('label' => "Téléphone 2"))
            ->add('email',null,array('label' => "Email"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('noms',"text",array('label' => "Noms"))
            ->add('sexe',"choice",array("choices" => Contact::getTypes(),"label" => "Sexe"))
            ->add('fonction',"text",array('label' => "Fonction"))
            ->add('telephone1',"text",array('label' => "Téléphone 1"))
            ->add('telephone2',"text",array('label' => "Téléphone 2", "required" => false))
            ->add('email',"text",array('label' => "Email", "required" => false))
            ->add('commentaire',"textarea",array('label' => "Commentaire", 'required'=>false))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('entreprise',null,array("label" => "Exploitant"))
            ->add('noms',null,array('label' => "Noms"))
            ->add('sexe',"choice",array("choices" => Contact::getTypes(),"label" => "Sexe"))
            ->add('fonction',null,array('label' => "Fonction"))
            ->add('telephone1',null,array('label' => "Téléphone 1"))
            ->add('telephone2',null,array('label' => "Téléphone 2"))
            ->add('email',null,array('label' => "Email"))
            ->add('commentaire',null,array('label' => "Commentaire"))
        ;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection){
        if(!$this->isChild())
            $collection->clearExcept(array('list','show','edit','delete','export','batch'));
    }

    public function getParentAssociationMapping()
    {
        return 'entreprise';
    }

    /**
     * @return string
     */
    public function getExploitantField()
    {
        return "entreprise";
    }

    public function prePersist($object)
    {
        $exp = $object->getEntreprise();
        
        $object->setEntreprise($exp);         
    }
}
