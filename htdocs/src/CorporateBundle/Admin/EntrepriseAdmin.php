<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 23/05/2017
 * Time: 18:20
 */

namespace CorporateBundle\Admin;

use CorporateBundle\Entity\Entreprise;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use AppBundle\Manager\AffectionManager;
 
class EntrepriseAdmin extends AbstractAdmin
{

    /**
     * @var AffectionManager
     */
    private $affm;

    public function configureShowFields(ShowMapper $mapper){
        $mapper
            ->add("nom","text",array('label' => "Nom de l'Entreprise"))
            ->add("type","choice",array("choices" => Entreprise::getTypes(),"label" => "Type"))
            ->add("tel","text",array("label" => "Téléphone"))
            ->add("email","email",array("label" => "e-Mail"))
            ->add("web","text",array("label" => "site web"))
            ->add("nif","text",array("label" => "No. Impôt Foncier"))            
            ->add("adresse","textarea",array("label" => "Adresse"))
            ->add("province","text",array("label" => "Province"))
            ->add("ville","text",array("label" => "Ville/Territoire"))
            ->add("pays","text",array("label" => "Pays"))
            ->add("commentaire","textarea",array("label" => "Commentaire"))
          ;
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add("nom","text",array('label' => "Nom de l'Entreprise"))
            ->add("type","choice",array("choices" => Entreprise::getTypes(),"label" => "Type"))
            ->add("tel","text",array("label" => "Téléphone","required" => false))
            ->add("email","email",array("label" => "e-Mail","required" => false))
            ->add("web","text",array("label" => "Site web","required" => false))
            ->add("nif","text",array("label" => "No. Impôt Foncier","required" => false))
            ->add("adresse","textarea",array("label" => "Adresse","required" => false))
            ->add('province','entity', array(
                'class' => 'LocationBundle\Entity\Province',
                'property' => 'libprovince',
                'multiple' => false,
                'required' => false
            ),array("label" => "Province","required" => false))
            ->add('ville','entity', array(
                'class' => 'LocationBundle\Entity\Ville',
                'property' => 'libville',
                'multiple' => false,
                'required' => false
            ),array("label" => "Ville/Territoire","required" => false))
            ->add('pays','entity', array(
                'class' => 'LocationBundle\Entity\Pays',
                'property' => 'libcountry',
                'multiple' => false,
                'required' => false
            ),array("label" => "Pays","required" => false))
            ->add("directeur","sonata_type_model_list",array("label" => "Personne physique","required" => false))
            ->add("commentaire","textarea",array("label" => "Commentaire","required" => false));
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->addIdentifier('nom',null,array(
                "route" => array("name" => "show")
            ))
            ->add('type','choice',array(
                'choices' => Entreprise::getTypes()
            ))
            ->add('tel')
            ->add('email')
            ->add("_action","actions",array(
                "actions" =>array(
                    "show" => array(),
                    "edit" => array(),
                    "delete" => array(),
                )
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('type')
         ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        if(isset($list['create']))
            unset($list['create']);

        if(in_array($action,['show'])){
            $list['concession'] = [
                'template' => 'CorporateBundle:CRUD:action_concession.html.twig'
            ];
            $list['site'] = [
                'template' => 'CorporateBundle:CRUD:action_site.html.twig'
            ];
            $list['contact'] = [
                'template' => 'CorporateBundle:CRUD:action_contact.html.twig'
            ];
            $list['partenaire'] = [
                'template' => 'CorporateBundle:CRUD:action_partenaire.html.twig'
            ];
        }

        return $list;
    }

    /**
     * @param AffectionManager $affm
     */
    public function setAffm(AffectionManager $affm)
    {
        $this->affm = $affm;
    }

    public function createQuery($context = 'list')
    {
        $query = parent::createQuery($context);

        if($this->affm->isExploitant() && $context == 'list'){
            $query
                ->andWhere("o.id = :id")
                ->setParameter("id",$this->affm->getAffectation()->getExploitant()->getId())
            ;
        }
        return $query;
    }


} 