<?php

namespace CorporateBundle\Admin;

use CorporateBundle\Entity\Site;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use CorporateBundle\Model\ExploitantFiltred;
use Sonata\AdminBundle\Route\RouteCollection;

class SiteAdmin extends AbstractAdmin implements ExploitantFiltred
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('nom')
            ->add('type',null,[],'choice',[
                'choices' => Site::getTypes()
            ])
            ->add('province')
            ->add('ville')
            ->add('entreprise',null,['label' => "Exploitant"])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('nom',"text",array('label' => "Nom"))
            ->add('type',"choice",array("choices" => Site::getTypes(),"label" => "Type"))
            ->add('entreprise',null,['label' => "Exploitant"])
            ->add('province',null,array('label' => "Province"))
            ->add('ville',null,array('label' => "Ville"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('nom',"text",array('label' => "Nom"))
            ->add('type',"choice",array("choices" => Site::getTypes(),"label" => "Type"))
            ->add('adresse',"textarea",array('label' => "Adresse"))
            ->add('province',null,array("label" => "Province","required" => false))
            ->add('ville',null, array("label" => "Ville/Territoire","required" => false))
            ->add('longitude',"number",array('label' => "Longitude"))
            ->add('lattitude',"number",array('label' => "Latitude"))
            ->add('commentaire',"textarea",array('label' => "Commentaire",'required'=>false))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('entreprise',null,array("label" => "Exploitant"))
            ->add('nom')
            ->add('type',"choice",array("choices" => Site::getTypes(),"label" => "Type"))
            ->add('adresse')
            ->add('province')
            ->add('ville')
            ->add('longitude')
            ->add('lattitude')
         ;
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection){
        if(!$this->isChild())
            $collection->clearExcept(array('list','show','edit','delete','export','batch'));
    }

    public function getParentAssociationMapping()
    {
        return 'entreprise';
    }

    /**
     * @return string
     */
    public function getExploitantField()
    {
        return "entreprise";
    }

    public function prePersist($object)
    {
        $exp = $object->getEntreprise();
        
        $object->setEntreprise($exp);         
    }

}
