<?php

namespace CorporateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('CorporateBundle:Default:index.html.twig');
    }
}
