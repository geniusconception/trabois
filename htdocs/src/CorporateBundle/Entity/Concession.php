<?php

namespace CorporateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Concession
 *
 * @ORM\Table(name="concession")
 * @ORM\Entity(repositoryClass="CorporateBundle\Repository\ConcessionRepository")
 * @Vich\Uploadable
 */
class Concession
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255,nullable=true)
     */
    private $name;

    /**
     * @var File
     * @Vich\UploadableField(mapping="document_attach", fileNameProperty="name")
     */
    private $fichier;

    /**
     * @var int
     *
     * @ORM\Column(name="superficie", type="decimal", precision=10, scale=2)
     */
    private $superficie;

    /**
     *
     *  @ORM\ManyToOne(targetEntity="LocationBundle\Entity\Province")
     */
    private $province;

    /**
     *
     *  @ORM\ManyToOne(targetEntity="LocationBundle\Entity\Ville")
     */
    private $ville;

    /**
    * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
    * 
    */
    private $entreprise;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Concession
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Annotation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set superficie
     *
     * @param integer $superficie
     * @return Concession
     */
    public function setSuperficie($superficie)
    {
        $this->superficie = $superficie;

        return $this;
    }

    /**
     * Get superficie
     *
     * @return integer 
     */
    public function getSuperficie()
    {
        return $this->superficie;
    }

    /**
     * Set entreprise
     *
     * @param \CorporateBundle\Entity\Entreprise $entreprise
     * @return Concession
     */
    public function setEntreprise(\CorporateBundle\Entity\Entreprise $entreprise = null)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    function __toString()
    {
        return ($this->getId())? $this->getNom(): "n/a";
    }

    /**
     * Set province
     *
     * @param \LocationBundle\Entity\Province $province
     * @return Concession
     */
    public function setProvince(\LocationBundle\Entity\Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return \LocationBundle\Entity\Province 
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set ville
     *
     * @param \LocationBundle\Entity\Ville $ville
     * @return Concession
     */
    public function setVille(\LocationBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \LocationBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setFichier(File $image = null)
    {
        $this->fichier = $image;

        //if ($image) {
            // It is required that at least one field changes if you are using doctrine
            // otherwise the event listeners won't be called and the file is lost
         //   $this->updatedAt = new \DateTimeImmutable();
        //}
        
        //return $this;
    }

    /**
     * @return File|null
     */
    public function getFichier()
    {
        return $this->fichier;
    }
}
