<?php

namespace CorporateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Site
 *
 * @ORM\Table(name="site")
 * @ORM\Entity(repositoryClass="CorporateBundle\Repository\SiteRepository")
 */
class Site
{
    const TYPE_EXPLOITATION = 1;
    const TYPE_EXPORTATION = 2;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     *
     *  @ORM\ManyToOne(targetEntity="LocationBundle\Entity\Province")
     */
    private $province;

    /**
     *
     *  @ORM\ManyToOne(targetEntity="LocationBundle\Entity\Ville")
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="longitude", type="decimal", precision=9, scale=7)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="lattitude", type="decimal", precision=10, scale=7)
     */
    private $lattitude;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=255, nullable=true)
     */
    private $commentaire;

     /**
    * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
    * 
    */
    private $entreprise;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return Site
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Site
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return Site
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return Site
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Set lattitude
     *
     * @param string $lattitude
     * @return Site
     */
    public function setLattitude($lattitude)
    {
        $this->lattitude = $lattitude;

        return $this;
    }

    /**
     * Get lattitude
     *
     * @return string 
     */
    public function getLattitude()
    {
        return $this->lattitude;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     * @return Site
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string 
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set entreprise
     *
     * @param \CorporateBundle\Entity\Entreprise $entreprise
     * @return Site
     */
    public function setEntreprise(\CorporateBundle\Entity\Entreprise $entreprise = null)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @return array
     */
    public static function getTypes(){
        return array(
            self::TYPE_EXPLOITATION => "Exploitation",
            self::TYPE_EXPORTATION => "Exportation",
         );
    }

    /**
     * @return array
     */
    public static function getTypesKeys(){
        return array_keys(self::getTypes());
    }

    function __toString()
    {
        return ($this->getId())? $this->getNom(): "n/a";
    }

    /**
     * Set province
     *
     * @param \LocationBundle\Entity\Province $province
     * @return Site
     */
    public function setProvince(\LocationBundle\Entity\Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return \LocationBundle\Entity\Province 
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set ville
     *
     * @param \LocationBundle\Entity\Ville $ville
     * @return Site
     */
    public function setVille(\LocationBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \LocationBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }
}
