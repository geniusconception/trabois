<?php

namespace CorporateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SousTraitant
 *
 * @ORM\Table(name="sous_traitant")
 * @ORM\Entity(repositoryClass="CorporateBundle\Repository\SousTraitantRepository")
 */
class SousTraitant
{
    const TYPE_TRANSPORT = 1;
    const TYPE_SCIERIE = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255)
     */
    private $adresse;

    /**
     *
     *  @ORM\ManyToOne(targetEntity="LocationBundle\Entity\Province")
     */
    private $province;

    /**
     *
     *  @ORM\ManyToOne(targetEntity="LocationBundle\Entity\Ville")
     */
    private $ville;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=10)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=255,nullable=true)
     */
    private $commentaire;

    /**
    * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
    * 
    */
    private $entreprise;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     * @return SousTraitant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string 
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return SousTraitant
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     * @return SousTraitant
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string 
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     * @return SousTraitant
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string 
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set commentaire
     *
     * @param string $commentaire
     * @return SousTraitant
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Get commentaire
     *
     * @return string 
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Set entreprise
     *
     * @param \CorporateBundle\Entity\Entreprise $entreprise
     * @return SousTraitant
     */
    public function setEntreprise(\CorporateBundle\Entity\Entreprise $entreprise = null)
    {
        $this->entreprise = $entreprise;

        return $this;
    }

    /**
     * Get entreprise
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @return array
     */
    public static function getTypes(){
        return array(
            self::TYPE_TRANSPORT => "Transport",
            self::TYPE_SCIERIE => "Scierie",
         );
    }

    /**
     * @return array
     */
    public static function getTypesKeys(){
        return array_keys(self::getTypes());
    }

    function __toString()
    {
        return ($this->getId())? $this->getNom(): "n/a";
    }

    /**
     * Set province
     *
     * @param \LocationBundle\Entity\Province $province
     * @return SousTraitant
     */
    public function setProvince(\LocationBundle\Entity\Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return \LocationBundle\Entity\Province 
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set ville
     *
     * @param \LocationBundle\Entity\Ville $ville
     * @return SousTraitant
     */
    public function setVille(\LocationBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \LocationBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }
}
