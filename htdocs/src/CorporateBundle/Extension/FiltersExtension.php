<?php

namespace CorporateBundle\Extension;

use AppBundle\Manager\AffectionManager;
use CorporateBundle\Model\ExploitantFiltred;
use CorporateBundle\Model\AdvancedExploitantFiltred;
use Sonata\AdminBundle\Admin\AbstractAdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;

class FiltersExtension extends AbstractAdminExtension{

    /**
     * @var AffectionManager
     */
	private $am;

    function __construct(AffectionManager $am)
    {
        $this->am = $am;
    }

    public function configureQuery(AdminInterface $admin, ProxyQueryInterface $query, $context = 'list')
    {
        $alias = "o";
    	if($context=='list' && $this->am->isExploitant() && $admin instanceof ExploitantFiltred){

            $e = $this->am->getAffectation()->getExploitant();
            if($admin instanceof AdvancedExploitantFiltred){
                foreach ($admin->getJoinedRules($alias) as $rule => $rule_alias) {
                    $query->leftJoin($rule,$rule_alias);
                }
                $query->andWhere(
                    sprintf('%s = :exp',$admin->getExploitantField())
                );
            }else{
                $query->andWhere(
                    sprintf('%s.%s = :exp',$alias,$admin->getExploitantField())
                );
            }
            $query->setParameter('exp',$e);
    	}
    }
} 