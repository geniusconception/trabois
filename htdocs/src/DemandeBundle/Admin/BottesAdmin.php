<?php

namespace DemandeBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class BottesAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('essence',null,array('label' => "Essence"))
            ->add('qualite',null,array('label' => "Qualité"))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper           
            ->add('nbreBotte',null,array("label" => "Nombre des bottes"))
            ->add('nbrePiece',null,array("label" => "Nombre des pièces"))
            ->add('epaisseur',null,array("label" => "Epaisseur (mm)"))
            ->add('qualite',null,array("label" => "Qualité"))
            ->add('cubage',null,array("label" => "Cubage (m3)"))
            ->add('tonnage',null,array("label" => "Tonnage"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper            
            ->add('nbreBotte','number',array("label" => "Nombre des bottes"))
            ->add('nbrePiece','number',array("label" => "Nombre des pièces"))
            ->add('epaisseur','number',array("label" => "Epaisseur (mm)"))
            ->add('qualite','text',array("label" => "Qualité"))
            ->add('cubage','number',array("label" => "Cubage (m3)"))
            ->add('tonnage','number',array("label" => "Tonnage"))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper             
            ->add('nbreBotte','number',array("label" => "Nombre des bottes"))
            ->add('nbrePiece','number',array("label" => "Nombre des pièces"))
            ->add('epaisseur','number',array("label" => "Epaisseur (mm)"))
            ->add('qualite','text',array("label" => "Qualité"))
            ->add('cubage','number',array("label" => "Cubage (m3)"))
            ->add('tonnage','number',array("label" => "Tonnage"))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
    }
}
