<?php

namespace DemandeBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class ColisageAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('essence')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('essence')
            ->add('type')
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
 
        $formMapper             
            //->add('essence')
            ->add('essence','entity',array(
                  'class'=>'ExploitationBundle\Entity\Essence',
                  'choice_label'=>'nom_pilote',
                  /*'empty_value' => 'Choisissez un Essence',
                  'expanded'=>false,
                  'multiple'=>false,
                  'required' => true,*/
                  'label' => 'Essence'))
            ->add('liste',"sonata_type_collection",array(
                        'type_options' => array(
                            "delete" => true,
                        ),
                        'by_reference' => false,
                        'label' => 'Liste'
                    ),array(
                        'edit' => 'inline',
                        'inline' => 'table',
                        'sortable' => 'position',
                ));            
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('essence')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
    }

    public function preUpdate($object)
    {
        $object->setBoisEssence();
    }

    public function prePersist($object)
    {
        $object->setBoisEssence();
    }


}
