<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 26/05/2017
 * Time: 15:42
 */

namespace DemandeBundle\Admin;

use AppBundle\Admin\ReversedOrderAdmin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class DemandeAdmin extends ReversedOrderAdmin{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var \AppBundle\Model\Compteur
     */
    private $compteur;
    /**
     * @var \TrajetBundle\Model\CheckpointManager
     */
    private $cpm;

    public function configureShowFields(ShowMapper $mapper){
        $mapper
            ->add('reference',null,array("label" => "Référence"))
            ->add("source",null,array("label" => "Exploitant"))
            ->add("dateRedigee",'date',array("label" => "Rédigé le","format" => "d-m-Y"))
            ->add('user',null,array("label" => "Encodé par"))
            ->add("dateEnregistree",'datetime',array("label","encodé le","format" => "d-m-Y"))
            ->add('liste',"text",array('label' => "Liste de colisage"))
        ;
    }

    public function configureFormFields(FormMapper $mapper){
        /** @var \DemandeBundle\Entity\Demande $dmd */
        $dmd = $this->getSubject();
        if($dmd == null || $dmd->getListe() == null)
            $add = "ajouter";
        else
            $add = false;
        $mapper
            ->add("source",null,array("label" => "Exploitant"))
            ->add("dateRedigee",'sonata_type_datetime_picker',array("label" => "Rédigé le",'format' => 'y-MM-dd'))
            ->add('liste',"sonata_type_model_list",array(
                'label' => "Liste de colisage",
                'btn_add' => $add,
                'btn_list' => false,
                'btn_delete' => false,
            ))
        ;
    }

    protected function configureListFields(ListMapper $list){
        $list
            ->addIdentifier('reference',null,array(
                "label" => "Référence",
                "route" => array("name" => "show"),
            ))
            ->add('source',null,array("label" => "Exploitant","route" => array("name" => "show")))
            ->add('liste',"text",array('label' => "Liste de colisage"))
            ->add("user",null,array("label" => "Encodé par"))
            ->add("dateEnregistree",'datetime',array("label" => "Encodé le","format" => "d-m-Y"))
            ->add('locked','boolean',array("label" => "Soumis ?"))
            ->add("_action","actions",array(
                "actions" =>array(
                    "show" => array(),
                    "edit" => array(),
                    "delete" => array(),
                    "submit" => array(
                        'template' => "DemandeBundle:CRUD:list__action_submit.html.twig"
                    )
                )
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('source',null,array('label' => "Exploitant"))
            ->add('locked',null)
            ->add('user',null,array("label" => "encodeur"))
        ;
    }

    /**
     * @param mixed $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist($object)
    {
        $object->setUser($this->tokenStorage->getToken()->getUser());
        /** @var \DateTime $d */
        $d = $object->getDateEnregistree();
        $prefix = "ENC";
        $ref = $prefix;
        $ref .= $d->format('m').$d->format('Y');
        $ref .= "-".$this->compteur->nextValue($prefix);
        $object->setReference($ref);
    }

    public function postPersist($object)
    {
        $this->cpm->addStep($object,$object->getReference(),'presystem');
    }


    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('submit', $this->getRouterIdParameter().'/submit');
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);
        if(
            in_array($action,array('show','edit',))
            //&& $this->canAccessObject('show', $object)
        ){
            $list['verify'] = array(
                'template' =>  'DemandeBundle:CRUD:action_submit.html.twig'
            );
        }

        return $list;
    }

    /**
     * @param \AppBundle\Model\Compteur $compteur
     */
    public function setCompteur($compteur)
    {
        $this->compteur = $compteur;
    }

    /**
     * @param \TrajetBundle\Model\CheckpointManager $cpm
     */
    public function setCheckpointManager($cpm)
    {
        $this->cpm = $cpm;
    }

    public function getTemplate($name)
    {
        if($name == 'show')
            return "DemandeBundle:CRUD:show.html.twig";
        return parent::getTemplate($name);
    }


} 