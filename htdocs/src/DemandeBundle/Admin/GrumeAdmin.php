<?php

namespace DemandeBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class GrumeAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('numGrume','text',array("label" => "N° Grume"))
            ->add('numAbattage','number',array("label" => "N° Abattage"))
            ->add('numPermis','text',array("label" => "Permis de Coupe"))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper             
            ->addIdentifier('numGrume','text',array(
                "label" => "N° Grume",
                "route" => array("name" => "show"),
            ))
            ->add('numAbattage','number',array("label" => "N° Abattage"))
            ->add('numPermis','text',array("label" => "Permis de Coupe"))
            ->add('longueur','number',array("label" => "Longueur (m)"))
            ->add('diamAA','number',array("label" => "Diamètre AA (cm)"))
            ->add('diamSA','number',array("label" => "Diamètre SA (cm)"))
            ->add('tonnage','number',array("label" => "Tonnage (T)"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper             
            ->add('numGrume','text',array("label" => "N° Grume", "required"=>false))
            ->add('numAbattage','text',array("label" => "N° Abattage", "required"=>false))
            ->add('numPermis','text',array("label" => "Permis de Coupe"))
            ->add('longueur','number',array("label" => "Longueur (m)"))
            ->add('diamAA','number',array("label" => "Diamètre AA (cm)"))
            ->add('diamSA','number',array("label" => "Diamètre SA (cm)"))
            ->add('tonnage','number',array("label" => "Tonnage (T)"))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('numGrume','text',array("label" => "N° Grume"))
            ->add('numAbattage','number',array("label" => "N° Abattage"))
            ->add('numPermis','text',array("label" => "Permis de Coupe"))
            ->add('longueur','number',array("label" => "Longueur (m)"))
            ->add('diamAA','number',array("label" => "Diamètre AA (cm)"))
            ->add('diamSA','number',array("label" => "Diamètre SA (cm)"))
            ->add('cubage','number',array("label" => "Volume AA (m3)"))
            ->add('cubageSA','number',array("label" => "Volume SA (m3)"))
            ->add('tonnage','number',array("label" => "Tonnage (T)"))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
    }
}
