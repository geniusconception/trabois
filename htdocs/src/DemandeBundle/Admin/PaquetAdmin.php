<?php

namespace DemandeBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class PaquetAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper            
            ->add('nbrePaquet')
            ->add('piece')
            ->add('hauteur')
            ->add('largeur')
            ->add('epaisseur_p')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper            
            ->add('nbrePaquet','number',array("label" => "Nombre des Paquets"))
            ->add('piece','number',array("label" => "Nombre des Pièces"))
            ->add('hauteur','number',array("label" => "Hauteur"))
            ->add('largeur','number',array("label" => "Largeur"))
            ->add('epaisseur_p','number',array("label" => "Epaisseur"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper             
            ->add('nbrePaquet','number',array("label" => "Nombre des Paquets"))
            ->add('piece','number',array("label" => "Nombre des Pièces"))
            ->add('hauteur','number',array("label" => "Longueur (m)"))
            ->add('largeur','number',array("label" => "Largeur (m)"))
            ->add('epaisseur_p','number',array("label" => "Epaisseur (m)"))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('nbrePaquet','number',array("label" => "Nombre des Paquets"))
            ->add('piece','number',array("label" => "Nombre des Pièces"))
            ->add('hauteur','number',array("label" => "Longueur (m)"))
            ->add('largeur','number',array("label" => "Largeur (m)"))
            ->add('epaisseur_p','number',array("label" => "Epaisseur (m)"))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
    }
}
