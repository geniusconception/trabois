<?php

namespace DemandeBundle\Admin;

use DemandeBundle\Entity\Recovery;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Admin\ReversedOrderAdmin;
use DemandeBundle\Entity\RecoveryGrume;

class RecoveryAdmin extends ReversedOrderAdmin
{
     /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var \AppBundle\Model\Compteur
     */
    private $compteur;
    /**
     * @var \TrajetBundle\Model\CheckpointManager
     */
    private $cpm;

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('exploitant',null,array("label" => "Exploitant"))
            ->add('reference',null,[
                'label' => 'Référence'
            ])
            ->add('dateRedigee', 'doctrine_orm_date_range', [
                'field_type'=>'sonata_type_date_range_picker',
                'field_options' => [
                    'field_options' => [
                        'format' => 'yyyy-MM-dd'
                    ]
                ],
                'label' => 'Date'
            ])
            ->add('verified',null,array("label" => "Vérifié ?"))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('reference',null,array(
                "label"=>"Référence",
                'route' => array(
                    'name' => 'show'
                )
            ))
            ->add('exploitant',null,array(
                "label"=>"Exploitant"
            ))
            ->add('getGrumeCount','text',array('label'=>'N° Grume'))
            ->add('cubage','number',[
                'label' => "Volume -A",
            ])
            ->add('site','text',array("label"=>"Site de contrôle"))
            ->add('dateApprobation','date',array("label"=>"Période","format" => "m-Y"))
            ->add('verified',null,array('label'=>'Vérifié?'))
            ->add('dateEnregistree','date',array('format' => 'd-m-Y',"label"=>"Date"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'submit' => array(
                        'template' => 'DemandeBundle:Recovery:list__action_submit.html.twig',
                    ),
                    'verify' => array(
                        'template' => 'DemandeBundle:Recovery:list__action_verify.html.twig',
                    ),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('exploitant',null,array("label"=>"Exploitant"))
            ->add('nombreGrumeSaisie','number',array("label"=>"Nombre grume saisie"))
            ->add('site','text',array("label"=>"Site de contrôle", "required"=>false))
            ->add('dateApprobation','sonata_type_date_picker',array(
                    'label' => "Date d'Approbation",
                    'required' => true,
                    'format' => 'y-MM'))              
            ->add('grume',"sonata_type_collection",array(
                    'type_options' => array(
                        "delete" => true,
                    ),
                    'by_reference' => false,
                    'label' => 'Liste des colisages'
                ),array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                ))             
           ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('reference','text',array('label'=>"Référence"))
            ->add('exploitant','text',array("label"=>"Exploitant"))
            ->add('verified',null,array('label'=>'Vérifié?'))
            ->add('getGrumeCount','text',array('label'=>'No. Total'))
            ->add('nombreGrumeSaisie','text',array('label'=>'No. Grume saisie'))
            ->add('getNormalGrumeCount','text',array('label' => 'No. Grume'))
            ->add('site','text',array('label'=>'Site de contrôle'))
            ->add('cubage','number',[
                'label' => "Volume +A",
            ])
            ->add('cubageSA','number',[
                'label' => "Volume -A",
            ])
            ->add('dateApprobation','date',array('label'=>"Periode",'format' => 'm-Y'))
            ->add('user',null,array("label" => "Encodé par"))
            ->add('controller',null,array("label" => "Controllé par"))
            ->add('step','choice',array(
                'label' => "Phase",
                'choices' => Recovery::getSteps()
            ))
         ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('submit', $this->getRouterIdParameter().'/submit');
        $collection->add('verify', $this->getRouterIdParameter().'/verify');
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);
        if(
            in_array($action,array('show','edit',))
        ){
            $list['submit'] = array(
                'template' =>  'DemandeBundle:Recovery:action_submit.html.twig'
            );
            $list['verify'] = array(
                'template' =>  'DemandeBundle:Recovery:action_verify.html.twig'
            );
        }

        return $list;
    }

    /**
     * @param mixed $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $b = $this->tokenStorage = $tokenStorage;

    }

    public function prePersist($object)
    {
        $object->setUser($this->tokenStorage->getToken()->getUser());
        /** @var \DateTime $d */
        $d = $object->getDateEnregistree();
        $prefix = "ENC";
        $ref = $prefix;
        $ref .= $d->format('m').$d->format('Y');
        $ref .= "-".$this->compteur->nextValue($prefix);
        $object->setReference($ref);
    }

    public function postPersist($object)
    {
        $this->cpm->addStep($object,$object->getReference(),'presystem');
    }

    /**
     * @param \AppBundle\Model\Compteur $compteur
     */
    public function setCompteur($compteur)
    {
        $this->compteur = $compteur;
    }

    /**
     * @param \TrajetBundle\Model\CheckpointManager $cpm
     */
    public function setCheckpointManager($cpm)
    {
        $this->cpm = $cpm;
    }

}
