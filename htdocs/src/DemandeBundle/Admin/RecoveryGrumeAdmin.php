<?php

namespace DemandeBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use DemandeBundle\Entity\RecoveryGrume;

class RecoveryGrumeAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('numAbattage')
            ->add('longueur')
            ->add('numPermis')
            ->add('diamAA')
            ->add('diamSA')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('numAbattage','text',array("label"=>"N° Abattage"))
            ->add('essence','text',array("label"=>"Essence"))
            ->add('longueur','text',array("label"=>"Longueur"))
            ->add('numPermis','text',array("label"=>"Numero de permis"))
            ->add('diamAA','text',array("label"=>"Diamètre AA"))
            ->add('diamSA','text',array("label"=>"Diamètre SA"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('numAbattage','text',array("label"=>"N° Abattage"))
            ->add('essence',null,array("label"=>"Essence"))
            ->add('longueur','text',array("label"=>"Longueur"))
            ->add('numPermis','text',array("label"=>"Numero de permis","required"=>false))
            ->add('diamAA','text',array("label"=>"Diamètre AA"))
            ->add('diamSA','text',array("label"=>"Diamètre SA","required"=>false))
            ->add('etat','choice',array('choices' => RecoveryGrume::getAvailableStatus(),"label"=>"Etat"))
         ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('numAbattage')
            ->add('longueur')
            ->add('numPermis')
            ->add('diamAA')
            ->add('diamSA')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
    }
}
