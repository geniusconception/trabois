<?php

namespace DemandeBundle\Admin;

use DemandeBundle\Entity\Synthese;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use DemandeBundle\Entity\SyntheseGrume;
use AppBundle\Admin\ReversedOrderAdmin;

class SyntheseAdmin extends ReversedOrderAdmin
{
     /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var \AppBundle\Model\Compteur
     */
    private $compteur;
    /**
     * @var \TrajetBundle\Model\CheckpointManager
     */
    private $cpm;

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            //->add('permis',null,array("label" => "N° PCB"))
            ->add('exploitant',null,array("label" => "Exploitant"))
            //->add('user',null,array("label" => "Encodeur"))
            //->add('reference',null,array("label" => "Référence"))
            /*->add('step',null,array(
                    'label' => 'Phase'
                ),'choice',array(
                    'choices' => Synthese::getSteps()
                )
            )*/
            //->add('dateRedigee','doctrine_orm_date_range',array(),null,array('required' => false,  'attr' => array('class' => 'datepicker')))
            ->add('dateRedigee', 'doctrine_orm_date_range', [
                    'field_type'=>'sonata_type_date_range_picker',
                    'field_options' => [
                        'field_options' => [
                            'format' => 'yyyy-MM-dd'
                        ]
                    ],
                    'label' => 'Date'

            ]);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('reference',null,array(
                "label" => "Référence",
                "route" => array("name" => "show")
            ))
            ->add('exploitant',null,array("label" => "Exploitant"))
            ->add('permis',null,array("label" => "N° PCB"))            
            ->add('volume',null,array("label" => "Volume T"))
            ->add('grumeDeclare',null,array("label" => "N° Total"))
            ->add('verified',null,array("label" => "Vérifiée ?"))
            ->add('user',null,array("label" => "Encodé par"))
            ->add('dateRedigee',null,array("label" => "Date Approuvée",'format' => 'd-m-Y'))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'submit' => array(
                        'template' => 'DemandeBundle:Synthesis:list__action_submit.html.twig',
                    ),
                    'verify' => array(
                        'template' => 'DemandeBundle:Synthesis:list__action_verify.html.twig',
                    ),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('exploitant',null,array("label" => "Exploitant"))
            ->add('permis','text',array("label" => "N° Permis de coup", "required"=>false))
            //->add('tige','sonata_type_model_list',array("label" => "Essence"))            
            ->add('site','text',array("label" => "Site"))
            ->add('volume',null,array("label" => "Volume"))
            ->add('dateRedigee','sonata_type_datetime_picker',array("label" => "Date d'Approbation DCVI",'format' => 'y-MM-dd'))
            ->add('tige',"sonata_type_collection",array(
                    'type_options' => array(
                        "delete" => true,
                    ),
                    'by_reference' => false,
                    'label' => 'Essence'
                ),array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                ))
        ;

        $object = $this->getSubject();
        if($object instanceOf SyntheseGrume)       
            $formMapper
                ->add('grumeDeclare',null,array("label" => "Déclaré"))
                ->add('grumeDepasse',null,array("label" => "Dépassé"))                    
            ;
    }

    /**
     * @param mixed $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $b = $this->tokenStorage = $tokenStorage;

    }

    public function prePersist($object)
    {
        $object->setUser($this->tokenStorage->getToken()->getUser());
        /** @var \DateTime $d */
        $d = $object->getDateEnregistree();
        $prefix = "ENC";
        $ref = $prefix;
        $ref .= $d->format('m').$d->format('Y');
        $ref .= "-".$this->compteur->nextValue($prefix);
        $object->setReference($ref);
    }

    public function postPersist($object)
    {
        $this->cpm->addStep($object,$object->getReference(),'presystem');
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('submit', $this->getRouterIdParameter().'/submit');
        $collection->add('verify', $this->getRouterIdParameter().'/verify');
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);
        if(
            in_array($action,array('show','edit',))
        ){
            $list['submit'] = array(
                'template' =>  'DemandeBundle:Synthesis:action_submit.html.twig'
            );
            $list['verify'] = array(
                'template' =>  'DemandeBundle:Synthesis:action_verify.html.twig'
            );
        }

        return $list;
    }

    /**
     * @param \AppBundle\Model\Compteur $compteur
     */
    public function setCompteur($compteur)
    {
        $this->compteur = $compteur;
    }

    /**
     * @param \TrajetBundle\Model\CheckpointManager $cpm
     */
    public function setCheckpointManager($cpm)
    {
        $this->cpm = $cpm;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('exploitant',null,array("label" => "Exploitant"))
            ->add('permis',null,array("label" => "N° PCB"))
            ->add('site',null,array("label" => "Site de contrôle"))
            ->add('volume',null,array("label" => "Volume Total"))            
            ->add('reference',null,array("label" => "Référence"))
            ->add('user',null,array("label" => "Encodé par"))
            ->add('controller',null,array("label" => "Controllé par"))
            ->add('step','choice',array(
                'label' => "Phase",
                'choices' => Synthese::getSteps()
            ))
            ->add('verified',null,array("label" => "Vérifiée ?"))
         ;
        $object = $this->getSubject();
        if($object instanceOf SyntheseGrume)
            $showMapper
                ->add('grumeDeclare',null,array("label" => "Déclaré"))
                ->add('grumeDepasse',null,array("label" => "Dépassé"))
                ->add('totalBois',null,array("label" => "Normal"))
            ;
    }

    public function getTemplate($name)
    {
        if($name == 'show')
            return "DemandeBundle:Synthesis:show.html.twig";
        return parent::getTemplate($name);
    }
}
