<?php

namespace DemandeBundle\Admin;

use DemandeBundle\Entity\Tagger;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use TrajetBundle\Model\CheckpointManager;
use Sonata\AdminBundle\Route\RouteCollection;

class TaggerAdmin extends AbstractAdmin
{
    protected $classnameLabel  = "Générateur d'Etiquette";
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var CheckpointManager
     */
    private $trajet;
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('invoice',null,array("label" => "Facture"))
            ->add('qte',null,array("label" => "Nombre d'étiquette"))
            ->add('locked',null,array("label" => "Généré"))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('invoice',null,array(
                'label' => "Facture",
                'route' => array('name' => 'show')
            ))
            ->add('qte',"text",array('label' => "Nbre d'Etiquettes"))
            ->add('dep',"text",array('label' => "Nbre d'Etiquettes (Saisie)"))
            ->add('locked','boolean',array("label" => "Généré"))
            ->add('user',null,array('label' => 'crée par'))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'generate' => array(
                        'template' => "DemandeBundle:CRUD:list__action_generate.html.twig"
                    )
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $q = $this->modelManager
            ->createQuery("InvoiceBundle:Invoice","i")
            ->where("i.locked = :locked")
            ->setParameter("locked",false)
        ;

        $formMapper
            //->add('invoice',null,array("label" => "Facture"))
            ->add('invoice',null,array(
                'label' => "Facture",
                'query_builder' => $q
            ))
            ->add('qte','integer',array("label" => "Nombre d'étiquette"))
            ->add('dep','integer',array("label" => "Nombre d'étiquette (Saisie)"))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('invoice',null,array("label" => "Facture","required" => true))
            ->add('qte','integer',array("label" => "Nombre d'étiquette"))
            ->add('dep','integer',array("label" => "Nombre d'étiquette (Saisie)"))
            ->add('locked','boolean',array("label" => "Généré"))
            ->add('user',null,array('label' => 'crée par'))
        ;
    }

    /**
     * @param mixed $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist($object)
    {
        $object->setUser($this->tokenStorage->getToken()->getUser());
    }

    /**
     * @param CheckpointManager $trajet
     */
    public function setTrajet(CheckpointManager $trajet)
    {
        $this->trajet = $trajet;
    }

    public function postPersist($object)
    {
        /** @var \InvoiceBundle\Entity\Invoice $invoice */
        $invoice = $object->getInvoice();
        $trace = $this->trajet->retreiveTrace($invoice,Tagger::TYPE);
        $trace = ($trace)? : $invoice->getRef();

        $this->trajet->addStep($object,$trace,Tagger::TYPE);
        $this->trajet->lock($invoice);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('generateTags',$this->getRouterIdParameter().'/generate-tags');
    }

}
