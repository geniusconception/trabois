<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 26/05/2017
 * Time: 16:20
 */

namespace DemandeBundle\Admin;

use AppBundle\Admin\ReversedOrderAdmin;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Sonata\AdminBundle\Route\RouteCollection;


class VerificationAdmin extends ReversedOrderAdmin{

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    public function configureShowFields(ShowMapper $mapper){
        $mapper
            ->add('reference',null,array("label" => "Référence"))
            ->add("demande.source",null,array("label" => "Exploitant"))
            ->add("demande.dateRedigee",'date',array("label" => "Rédigé le","format" => "d-m-Y"))
            ->add('demande.user',null,array("label" => "Encodé par"))
            ->add("demande.dateEnregistree",'datetime',array("label","encodé le","format" => "d-m-Y"))
            ->add('user',null,array("label" => "Vérifié par"))
            ->add("dateEnregistree",'datetime',array("label","Vérifié le","format" => "d-m-Y"))
            ->add('demande.liste','text',array('label' => "Liste de colisage"))
        ;
    }

    public function configureFormFields(FormMapper $mapper){
        $mapper
            ->add("demande.source",'entity',array(
                "label" => "Exploitant",
                'class' => "CorporateBundle:Entreprise",

            ))
            ->add("demande.dateRedigee",'sonata_type_datetime_picker',array("label" => "Rédigé le",'format' => 'y-MM-dd'))
            ->add("demande.liste","sonata_type_model_list",array(
                'label' => "Liste de colisage",
                'btn_add' => false,
                'btn_list' => false,
                'btn_delete' => false,
            ))
        ;
    }

    protected function configureListFields(ListMapper $list){
        $list
            ->addIdentifier('reference',null,array(
                "label" => "Référence",
                "route" => array("name" => "show")
            ))
            ->add('source',null,array("label" => "Exploitant"))
            ->add('liste','text',array('label' => "Liste de colisage"))
            ->add("dateRedigee",'date',array("label" => "Rédigé le","format" => "d-m-Y"))
            ->add("demande.user",null,array("label" => "encodeur"))
            ->add('locked','boolean',array("label" => "Vérifiée ?"))
            ->add("_action","actions",array(
                "actions" =>array(
                    "show" => array(),
                    "edit" => array(),
                    "delete" => array(),
                    "verify" => array(
                        'template' => "DemandeBundle:CRUD:list__action_verify.html.twig"
                    )
                )
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('demande.source',null,array('label' => "Exploitant"))
            ->add('demande.user',null,array('label' => "Encodeur"))
            ->add('locked',null,array('label' => "Verifié ?"))
        ;
    }

    /**
     * @param mixed $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist($object)
    {
        $object->setUser($this->tokenStorage->getToken()->getUser());
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('delete');
        $collection->add('verify', $this->getRouterIdParameter().'/verify');
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);
        if(
            in_array($action,array('show','edit',))
            //&& $this->canAccessObject('show', $object)
        ){
            $list['verify'] = array(
                'template' =>  'DemandeBundle:CRUD:action_verify.html.twig'
            );
        }

        return $list;
    }

    public function getTemplate($name)
    {
        if($name == 'show')
            return "DemandeBundle:CRUD:show.html.twig";
        return parent::getTemplate($name);
    }
} 