<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 01/06/2017
 * Time: 15:11
 */

namespace DemandeBundle\Block;


use Doctrine\ORM\EntityManagerInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UnverifiedDemandeBlock extends AbstractBlockService{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {

        $q = $this->em->createQuery("
            SELECT v,d
            FROM DemandeBundle:Verification v
            JOIN v.demande d
            WHERE v.locked = :locked
            ORDER BY d.dateEnregistree DESC
        ");
        $q->setParameter('locked',false);
        $list = $q->setMaxResults(10)->getResult();


        return $this->renderResponse("DemandeBundle:Block:demande_list.html.twig",array(
           "list" => $list,
        ));
    }

    /**
     * @param EntityManagerInterface $em
     */
    public function setEntityManager(EntityManagerInterface $em)
    {
        $this->em = $em;
    }



}