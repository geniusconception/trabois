<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 01/06/2017
 * Time: 17:45
 */

namespace DemandeBundle\Chart;


use AppBundle\Model\AbstractChart;
use AppBundle\Utils\RandomColorFactory;
use Mukadi\ChartJSBundle\Model\WorkerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class DemandeChart extends AbstractChart{

    /**
     * @var TokenStorageInterface
     */
    protected $token_storage;
    /** @var  boolean */
    protected $oneUser;


    /**
     * @param WorkerInterface $wk ;
     */
    public function factory(WorkerInterface $wk)
    {
        $wk
            ->setModel("DemandeBundle:Demande",'d')
            ->func("COUNT(d)",'total')
            ->func("DATE(d.dateEnregistree)","day")
            ->dataset($wk->values('total'),"Total des demandes encodées",array(
                "backgroundColor" => RandomColorFactory::getRandomColors(7),
                "borderWidth" => 1
            ))
            ->labels($wk->values('day'))
            ->groupBy('day')
            ->order(array(
                "d.dateEnregistree" => "ASC"
            ));
        if($this->oneUser){
            $user = $this->token_storage->getToken()->getUser();
            $wk
                ->joinModel("d.user","u")
                ->selector("u.id","user_id")
                ->conditions()
                    ->_and()
                        ->set('DATEDIFF(:now,d.dateEnregistree) < 7',array("now" => new \DateTime()))
                        ->set("u.id = :user_id",array("user_id" => $user->getId()))
                    ->end()
                ->endConditions();
            ;
        }else{
            $wk
                ->conditions()
                    ->set('DATEDIFF(:now,d.dateEnregistree) < 7',array("now" => new \DateTime()))
                ->endConditions();
            ;
        }

    }

    /**
     * @param TokenStorageInterface $token_storage
     */
    public function setTokenStorage($token_storage)
    {
        $this->token_storage = $token_storage;
    }

    /**
     * @param boolean $oneUser
     */
    public function setOneUser($oneUser)
    {
        $this->oneUser = $oneUser;
    }


} 