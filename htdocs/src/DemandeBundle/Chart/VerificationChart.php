<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 02/06/2017
 * Time: 21:36
 */

namespace DemandeBundle\Chart;


use AppBundle\Utils\RandomColorFactory;
use Mukadi\ChartJSBundle\Model\WorkerInterface;

class VerificationChart extends DemandeChart{

    public function factory(WorkerInterface $wk)
    {
        $wk
            ->setModel("DemandeBundle:Verification",'v')
            ->func("COUNT(v)",'total')
            ->func("DATE(v.dateEnregistree)","day")
            ->dataset($wk->values('total'),"Total des demandes vérifiées",array(
                "backgroundColor" => RandomColorFactory::getRandomColors(7),
                "borderWidth" => 1
            ))
            ->labels($wk->values('day'))
            ->groupBy('day')
            ->order(array(
                "v.dateEnregistree" => "ASC"
            ));
        if($this->oneUser){
            $user = $this->token_storage->getToken()->getUser();
            $wk
                ->joinModel("v.user","u")
                ->selector("v.id","user_id")
                ->conditions()
                ->_and()
                    ->set('DATEDIFF(:now,v.dateEnregistree) < 7',array("now" => new \DateTime()))
                    ->set("u.id = :user_id",array("user_id" => $user->getId()))
                    ->set('v.locked = :locked',array("locked" => true))
                ->end()
                ->endConditions();
            ;
        }else{
            $wk
                ->conditions()
                ->_and()
                    ->set('DATEDIFF(:now,v.dateEnregistree) < 7',array("now" => new \DateTime()))
                    ->set('v.locked = :locked',array("locked" => true))
                ->end()
                ->endConditions();
            ;
        }
    }

} 