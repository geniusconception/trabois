<?php

namespace DemandeBundle\Controller;

use DemandeBundle\Entity\Demande;
use DemandeBundle\Entity\Tagger;
use DemandeBundle\Entity\Verification;
use AppBundle\Controller\CRUDController as Controller;
use Sonata\AdminBundle\Security\Handler\AclSecurityHandlerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Acl\Permission\MaskBuilder;

class CRUDController extends Controller
{
    const PRESYSTEME_TRAJET = Tagger::TYPE;

    public function verifyAction(Request $request, $id){
        /** @var \DemandeBundle\Entity\Verification $v */
        $v = $this->admin->getSubject();

        if (!$v) {
            throw new NotFoundHttpException(sprintf('unable to find the demande with id : %s', $id));
        }

        if($v->isLocked()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autorisé à valider cet enregistrement");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        $this->admin->checkAccess('edit', $v);

        $url = $this->admin->generateObjectUrl('verify',$v);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){

            $this->verify($v);

            $this->addFlash('sonata_flash_success', 'La demande a été marqué comme vérifié');

            return $this->redirect($this->admin->generateUrl('show',array("id" => $v->getId())));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $v,
            "message" => sprintf('Êtes-vous sûr de vouloir valider la demande "%s" ?',$v->__toString()),
        ));
    }

    private function verify(Verification $v){
        /** @var \InvoiceBundle\Manager\InvoiceManager $im */
        $im = $this->get("invoice.manager.invoice_manager");
        $v->setUser($this->getUser());
        $v->setDateEnregistree(new \DateTime());
        $f = $im->create($v);
        $this->save($f);

        /** @var \TrajetBundle\Model\CheckpointManager $cm */
        $cm = $this->get("trajet.trajet_manager");
        $cm->addStep($f,$v->getReference(),self::PRESYSTEME_TRAJET);
        $v = $cm->lock($v);

        $this->admin->update($v);
    }

    public function submitAction(Request $request,$id)
    {
        /** @var \DemandeBundle\Entity\Demande $dmd */
        $dmd = $this->admin->getSubject();

        if (!$dmd) {
            throw new NotFoundHttpException(sprintf('unable to find the demande with id : %s', $id));
        }

        if($dmd->isLocked()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autorisé à soumettre cet enregistrement");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        $this->admin->checkAccess('edit', $dmd);

        $url = $this->admin->generateObjectUrl('submit',$dmd);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->submit($dmd);

            $this->addFlash('sonata_flash_success', 'La demande a été soumise avec succès');

            return $this->redirect($this->admin->generateUrl('show',array("id" => $dmd->getId())));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $dmd,
            "message" => sprintf('Êtes-vous sûr de vouloir soumettre la demande "%s" ?',$dmd->__toString()),
        ));
    }

    private function submit(Demande $dmd){
        $v = new Verification($dmd);
        $v->setUser(null);
        $v = $this->save($v);

        /** @var \TrajetBundle\Model\CheckpointManager $cm */
        $cm = $this->get("trajet.trajet_manager");
        $cm->addStep($v,$dmd->getReference(),self::PRESYSTEME_TRAJET);
        $dmd = $cm->lock($dmd);

        $this->admin->update($dmd);
    }

    /**
     * @param $object
     * @param bool $owner
     * @return object
     */
    protected function save($object,$owner =true){
        $em  = $this->getDoctrine()->getManager();
        $em->persist($object);
        $em->flush();

        $this->get("app.acl_manager")->saveAcl($object,($owner)? $this->getUser(): null);

        return $object;
    }

    public function generateTagsAction(Request $request,$id){
        /** @var \DemandeBundle\Entity\Tagger $t */
        $t = $this->admin->getSubject();

        if (!$t) {
            throw new NotFoundHttpException(sprintf('unable to find the Tagger with id : %s', $id));
        }

        if($t->isLocked()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autorisé à traiter cet enregistrement");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        $this->admin->checkAccess('edit', $t);

        $url = $this->admin->generateObjectUrl('generateTags',$t);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            /** @var \DemandeBundle\Manager\TaggerManager $manager */
            $manager = $this->get("demande.manager.tagger");

            $t = $manager->generateTags($t);

            $this->addFlash('sonata_flash_success', sprintf("Opération réussie, %d étiquettes ont été générées",$t->getQte()));

            return $this->redirect($this->admin->generateUrl('show',array("id" => $t->getId())));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $t,
            "message" => sprintf('Êtes-vous sûr de vouloir générer %d étiquettes depuis la facture %s ?',$t->getQte(),$t->getInvoice()),
        ));
    }
}
