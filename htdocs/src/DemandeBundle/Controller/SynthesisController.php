<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 04/07/2017
 * Time: 13:04
 */

namespace DemandeBundle\Controller;

use DemandeBundle\Entity\Synthese;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

class SynthesisController extends CRUDController{


    public function submitAction(Request $request, $id)
    {
        /** @var \DemandeBundle\Entity\Synthese $s */
        $s = $this->admin->getSubject();

        if (!$s) {
            throw new NotFoundHttpException(sprintf('unable to find the synthse with id : %s', $id));
        }

        if($s->isLocked()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autorisé à soumettre cet enregistrement");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        $this->admin->isGranted('SUBMIT',$s);

        $url = $this->admin->generateObjectUrl('submit',$s);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->submit($s);

            $this->addFlash('sonata_flash_success', 'L\'enregistrement a été soumis avec succès');

            return $this->redirect($this->admin->generateUrl('list'));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $s,
            "message" => sprintf('Êtes-vous sûr de vouloir soumettre l\'enregistrement "%s" ?',$s->__toString()),
        ));
    }

    public function verifyAction(Request $request, $id)
    {
        /** @var \DemandeBundle\Entity\Synthese $s */
        $s = $this->admin->getSubject();

        if (!$s) {
            throw new NotFoundHttpException(sprintf('unable to find the synthse with id : %s', $id));
        }

        if($s->isLocked()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autorisé à soumettre cet enregistrement");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        $this->admin->isGranted('VERIFY',$s);

        $url = $this->admin->generateObjectUrl('verify',$s);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $this->verify($s);

            $this->addFlash('sonata_flash_success', 'L\'enregistrement a été validé avec succès');

            return $this->redirect($this->admin->generateUrl('list'));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $s,
            "message" => sprintf('Êtes-vous sûr de vouloir valider l\'enregistrement "%s" ?',$s->__toString()),
        ));
    }

    private function submit(Synthese $s){
        $s->setStep(Synthese::STEP_VERIFYING);
        /** @var \TrajetBundle\Model\CheckpointManager $cm */
        $cm = $this->get("trajet.trajet_manager");
        $cm->addStep($s,$s->getReference(),self::PRESYSTEME_TRAJET);

        $this->admin->update($s);
    }

    private function verify(Synthese $s){
        /** @var \InvoiceBundle\Manager\InvoiceManager $im */
        $im = $this->get("invoice.manager.invoice_manager");
        $s->setController($this->getUser());
        $f = $im->create($s);
        $this->save($f);

        /** @var \TrajetBundle\Model\CheckpointManager $cm */
        $cm = $this->get("trajet.trajet_manager");
        $cm->addStep($f,$s->getReference(),self::PRESYSTEME_TRAJET);
        $cm->lock($s);

        $this->admin->update($s);
    }

} 