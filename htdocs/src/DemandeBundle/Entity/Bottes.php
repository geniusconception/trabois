<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Bottes
 *
 * @ORM\Entity()
 */
class Bottes extends Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="nbre_botte", type="integer")
     */
    private $nbreBotte;

    /**
     * @var int
     *
     * @ORM\Column(name="nbre_piece", type="integer")
     */
    private $nbrePiece;

    /**
     * @var int
     *
     * @ORM\Column(name="epaisseur", type="integer")
     */
    private $epaisseur;

    /**
     * @var string
     *
     * @ORM\Column(name="qualite", type="string", length=255)
     */
    private $qualite;

    /**
     * @var string
     *
     * @ORM\Column(name="cubage", type="decimal", precision=10, scale=3)
     */
    private $cubage;

     /**
     * @var string
     *
     * @ORM\Column(name="tonnage", type="decimal", precision=10, scale=3)
     */
    private $tonnage;

    /**
     * @var Colisage
     *
     * @ORM\ManyToOne(targetEntity="DemandeBundle\Entity\ColisageBotte",inversedBy="liste")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $colisage;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbreBotte
     *
     * @param integer $nbreBotte
     * @return Bottes
     */
    public function setNbreBotte($nbreBotte)
    {
        $this->nbreBotte = $nbreBotte;

        return $this;
    }

    /**
     * Get nbreBotte
     *
     * @return integer 
     */
    public function getNbreBotte()
    {
        return $this->nbreBotte;
    }

    /**
     * Set nbrePiece
     *
     * @param integer $nbrePiece
     * @return Bottes
     */
    public function setNbrePiece($nbrePiece)
    {
        $this->nbrePiece = $nbrePiece;

        return $this;
    }

    /**
     * Get nbrePiece
     *
     * @return integer 
     */
    public function getNbrePiece()
    {
        return $this->nbrePiece;
    }

    /**
     * Set epaisseur
     *
     * @param integer $epaisseur
     * @return Bottes
     */
    public function setEpaisseur($epaisseur)
    {
        $this->epaisseur = $epaisseur;

        return $this;
    }

    /**
     * Get epaisseur
     *
     * @return integer 
     */
    public function getEpaisseur()
    {
        return $this->epaisseur;
    }

    /**
     * Set qualite
     *
     * @param string $qualite
     * @return Bottes
     */
    public function setQualite($qualite)
    {
        $this->qualite = $qualite;

        return $this;
    }

    /**
     * Get qualite
     *
     * @return string 
     */
    public function getQualite()
    {
        return $this->qualite;
    }

    /**
     * Set cubage
     *
     * @param string $cubage
     * @return Bottes
     */
    public function setCubage($cubage)
    {
        $this->cubage = $cubage;

        return $this;
    }


    /**
     * Get cubage
     *
     * @return string 
     */
    public function getCubage()
    {
        return $this->cubage;
    }

     /**
     * Set tonnage
     *
     * @param string $tonnage
     * @return Bottes
     */
    public function setTonnage($tonnage)
    {
        $this->tonnage = $tonnage;

        return $this;
    }

    /**
     * Get tonnage
     *
     * @return string 
     */
    public function getTonnage()
    {
        return $this->tonnage;
    }

    public function __toString(){
         
        return ($this->getNbrePiece())? sprintf("Botte %d (%s)",$this->getNbrePiece(),$this->getEssence()):"N/A";
    }

}
