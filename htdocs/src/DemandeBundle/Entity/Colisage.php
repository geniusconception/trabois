<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="colisage")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap( {"list_grume" = "ColisageGrume", "list_botte" = "ColisageBotte", "list_paquet" = "ColisagePaquet"} )
 */
abstract class Colisage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
    * @ORM\ManyToOne(targetEntity="ExploitationBundle\Entity\Essence" ,cascade={"persist"})
    * 
    */
    protected $essence;

    /**
     * @var array
     *
     * 
     */
    protected $liste;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set liste
     *
     * @param string $liste
     * @return Colisage
     */
    public function setListe($liste)
    {
        $this->liste = $liste;

        return $this;
    }

    /**
     * Get liste
     *
     * @return string 
     */
    public function getListe()
    {
        return $this->liste;
    }

    /**
     * Add liste
     *
     * @param \DemandeBundle\Entity\Produit $liste
     * @return Liste_grume
     */
    public function addListe(\DemandeBundle\Entity\Produit $liste)
    {
        $this->liste[] = $liste;
        $liste->setColisage($this);
        return $this;
 
    }

    /**
     * Remove liste
     *
     * @param \DemandeBundle\Entity\Produit $liste
     */
    public function removeListe(\DemandeBundle\Entity\Produit $liste)
    {
        $this->liste->removeElement($liste);
        $liste->setColisage(null);

    }


    public function setBoisEssence()
    {
        /** @var Produit $bois */
        foreach ($this->liste as $bois) {
            $bois->setEssence($this->essence);
        }
    }


    /**
     * Set essence
     *
     * @param \ExploitationBundle\Entity\Essence $essence
     * @return Colisage
     */
    public function setEssence(\ExploitationBundle\Entity\Essence $essence = null)
    {
        $this->essence = $essence;

        return $this;
    }

    /**
     * Get essence
     *
     * @return \DemandeBundle\Entity\Essence 
     */
    public function getEssence()
    {
        return $this->essence;
    }

    public function getCubage(){
        $c= 0.0;
        /** @var Produit $p */
        foreach ($this->liste as $p) {
            $c += (double) $p->getCubage();
        }

        return $c;
    }
}
