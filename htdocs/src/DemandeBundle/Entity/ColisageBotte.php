<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColisageBotte
 *
 * @ORM\Entity()
 */
class ColisageBotte extends Colisage
{
    /**
    * @ORM\OneToMany(targetEntity="DemandeBundle\Entity\Bottes",mappedBy="colisage",orphanRemoval=true,cascade={"persist","remove"})
    * 
    */
    protected $liste;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->liste = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getTotal(){

        $t = 0;
        /** @var Bottes $i */
        foreach ($this->liste as $i) {
            $t += $i->getNbrePiece();
        }
        return $t;
    }

    public function __toString(){
         
        return ($this->getId())? sprintf("%d pieces - Botte %s (%s m3)",$this->getTotal(),$this->getEssence(),$this->getCubage()):"N/A";
    }
}
