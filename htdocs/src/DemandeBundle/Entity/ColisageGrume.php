<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColisageGrume
 *
 * @ORM\Entity()
 */
class ColisageGrume extends Colisage
{

    /**
     * 
     * @ORM\OneToMany(targetEntity="DemandeBundle\Entity\Grume",mappedBy="colisage",orphanRemoval=true,cascade={"persist","remove"})
     */ 
     protected $liste;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->liste = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getCubageSA(){

        $t = 0;
        /** @var Grume $i */
        foreach ($this->liste as $i) {
            $t += (double) $i->getCubageSA();
        }
        return $t;
    }

    public function __toString(){
         
        return ($this->getId())? sprintf("%d Grumes %s - %s m3 (AA), %s m3 (SA)",count($this->liste),$this->getEssence(),$this->getCubage(), $this->getCubageSA()):"N/A";
    }

    
}
