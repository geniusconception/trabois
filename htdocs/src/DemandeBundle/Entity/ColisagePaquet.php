<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ColisagePaquet
 *
 * @ORM\Entity()
 */
class ColisagePaquet extends Colisage
{

    /**
    * @ORM\OneToMany(targetEntity="DemandeBundle\Entity\Paquet",mappedBy="colisage",orphanRemoval=true,cascade={"persist","remove"})
    * 
    */
    protected $liste;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->liste = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function getPaquets(){
        $d=0;
        /** @var Paquet $p */
        foreach ($this->liste as $p) {
            $d += $p->getNbrePaquet();
        }

        return $d;
    }

    public function __toString(){
         
        return ($this->getId())? sprintf("%d paquets %s (%s m3)",$this->getPaquets(),$this->getEssence(),$this->getCubage()):"N/A";
    }

}
