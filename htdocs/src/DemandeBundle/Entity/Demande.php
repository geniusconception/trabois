<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use DemandeBundle\Model\DemandeInterface;
use TrajetBundle\Model\Checkpoint;

/**
 * Demande
 *
 * @ORM\Table(name="demande")
 * @ORM\Entity(repositoryClass="DemandeBundle\Repository\DemandeRepository")
 */
class Demande implements DemandeInterface,Checkpoint
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * 
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_redigee", type="datetime")
     */
    private $dateRedigee;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_enregistree", type="datetime")
     */
    private $dateEnregistree;

    /**
     * 
     *
     * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
     */
    private $source;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked;
    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string",length=255,unique = true)
     */
    private $reference;
    /**
     * @var Colisage
     *
     * @ORM\OneToOne(targetEntity="DemandeBundle\Entity\Colisage",orphanRemoval=true,cascade={"all"})7
     * @ORM\JoinColumn(nullable=false)
     */
    private $liste;

    public function __construct(){
        $this->dateEnregistree = new \DateTime();
        $this->locked = false;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateRedigee
     *
     * @param \DateTime $dateRedigee
     * @return Demande
     */
    public function setDateRedigee($dateRedigee)
    {
        $this->dateRedigee = $dateRedigee;

        return $this;
    }

    /**
     * Get dateRedigee
     *
     * @return \DateTime 
     */
    public function getDateRedigee()
    {
        return $this->dateRedigee;
    }

    /**
     * Set dateEnregistree
     *
     * @param \DateTime $dateEnregistree
     * @return Demande
     */
    public function setDateEnregistree($dateEnregistree)
    {
        $this->dateEnregistree = $dateEnregistree;

        return $this;
    }

    /**
     * Get dateEnregistree
     *
     * @return \DateTime 
     */
    public function getDateEnregistree()
    {
        return $this->dateEnregistree;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Demande
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set source
     *
     * @param \CorporateBundle\Entity\Entreprise $source
     * @return Demande
     */
    public function setSource(\CorporateBundle\Entity\Entreprise $source = null)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getSource()
    {
        return $this->source;
    }

    function __toString()
    {
        return ($this->getId())? sprintf("De %s [REF: %s]",$this->getSource()->getNom(),$this->getReference()):"n/a";
    }


    public function getName()
    {
       return "demande";

    }

    public function isLocked()
    {

        return $this->locked;
    }

    public function setLocked($lock)
    {
        $this->locked = $lock;
    }

    public function getIdentifier()
    {
        return array("id" => $this->getId());
    }

    /**
     * Get locked
     *
     * @return boolean 
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Demande
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    public function getListe()
    {
        return $this->liste;
    }

    public function setListe(\DemandeBundle\Entity\Colisage $liste)
    {
        $this->liste = $liste;
        return $this;
    }


}
