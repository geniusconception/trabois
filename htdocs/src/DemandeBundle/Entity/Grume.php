<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Grume
 *
 * @ORM\Entity()
 */
class Grume extends Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="num_grume", type="string", length=255)
     */
    private $numGrume;

    /**
     * @var string
     *
     * @ORM\Column(name="num_abattage", type="string", length=255)
     */
    private $numAbattage;

    /**
     * @var string
     *
     * @ORM\Column(name="num_permis", type="string", length=255)
     */
    private $numPermis;

    /**
     * @var string
     *
     * @ORM\Column(name="longueur", type="decimal", precision=10, scale=1)
     */
    private $longueur;

    /**
     * @var int
     *
     * @ORM\Column(name="diamAA", type="decimal", precision=10, scale=3)
     */
    private $diamAA;

    /**
     * @var int
     *
     * @ORM\Column(name="diamSA", type="decimal", precision=10, scale=3)
     */
    private $diamSA;

     /**
     * @var string
     *
     * @ORM\Column(name="tonnage", type="decimal", precision=10, scale=3)
     */
    private $tonnage;

    /**
     * @var Colisage
     *
     * @ORM\ManyToOne(targetEntity="DemandeBundle\Entity\ColisageGrume",inversedBy="liste")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $colisage;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numGrume
     *
     * @param string $numGrume
     * @return Grume
     */
    public function setNumGrume($numGrume)
    {
        $this->numGrume = $numGrume;

        return $this;
    }

    /**
     * Get numGrume
     *
     * @return string 
     */
    public function getNumGrume()
    {
        return $this->numGrume;
    }

    /**
     * Set numAbattage
     *
     * @param string $numAbattage
     * @return Grume
     */
    public function setNumAbattage($numAbattage)
    {
        $this->numAbattage = $numAbattage;

        return $this;
    }

    /**
     * Get numAbattage
     *
     * @return string 
     */
    public function getNumAbattage()
    {
        return $this->numAbattage;
    }

    /**
     * Set numPermis
     *
     * @param string $numPermis
     * @return Grume
     */
    public function setNumPermis($numPermis)
    {
        $this->numPermis = $numPermis;

        return $this;
    }

    /**
     * Get numPermis
     *
     * @return string 
     */
    public function getNumPermis()
    {
        return $this->numPermis;
    }

    /**
     * Set longueur
     *
     * @param string $longueur
     * @return Grume
     */
    public function setLongueur($longueur)
    {
        $this->longueur = $longueur;

        return $this;
    }

    /**
     * Get longueur
     *
     * @return string 
     */
    public function getLongueur()
    {
        return $this->longueur;
    }

    /**
     * Set diamAA
     *
     * @param integer $diamAA
     * @return Grume
     */
    public function setDiamAA($diamAA)
    {
        $this->diamAA = $diamAA;

        return $this;
    }

    /**
     * Get diamAA
     *
     * @return integer 
     */
    public function getDiamAA()
    {
        return $this->diamAA;
    }

    /**
     * Set diamSA
     *
     * @param integer $diamSA
     * @return Grume
     */
    public function setDiamSA($diamSA)
    {
        $this->diamSA = $diamSA;

        return $this;
    }

    /**
     * Get diamSA
     *
     * @return integer 
     */
    public function getDiamSA()
    {
        return $this->diamSA;
    }

    /**
     * Set cubage
     *
     * @param string $cubage
     * @return Bottes
     */
    public function setCubage($cubage){}

    /**
     * Get cubage
     *
     * @return string 
     */

    public function getCubage(){
        return $this->calcul($this->diamAA);
    }

     public function getCubageSA(){
        return $this->calcul($this->diamSA);
    }

    private function calcul($d){

        $v = $this->longueur * $d * $d * (pi()/4) * pow(10,-4);
        return number_format($v,3,'.',',');
    }

    /**
     * Set tonnage
     *
     * @param string $tonnage
     * @return Bottes
     */
    public function setTonnage($tonnage)
    {
        $this->tonnage = $tonnage;

        return $this;
    }

    /**
     * Get tonnage
     *
     * @return string 
     */
    public function getTonnage()
    {
        return $this->tonnage;
    }

    public function __toString(){
        return ($this->getId())? "Grume: ".$this->getNumAbattage()." ".$this->getEssence():"N/A";
    }

}
