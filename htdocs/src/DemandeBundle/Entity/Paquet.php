<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Paquet
 *
 * @ORM\Entity()
 */
class Paquet extends Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="nbre_paquet", type="integer")
     */
    private $nbrePaquet;

    /**
     * @var integer
     *
     * @ORM\Column(name="piece", type="integer")
     */
    private $piece;

    /**
     * @var string
     *
     * @ORM\Column(name="hauteur", type="decimal", precision=10, scale=2)
     */
    private $hauteur;

    /**
     * @var string
     *
     * @ORM\Column(name="largeur", type="decimal", precision=10, scale=2)
     */
    private $largeur;

    /**
     * @var string
     *
     * @ORM\Column(name="epaisseur_p", type="decimal", precision=10, scale=3)
     */
    private $epaisseur_p;

    /**
     * @var Colisage
     *
     * @ORM\ManyToOne(targetEntity="DemandeBundle\Entity\ColisagePaquet",inversedBy="liste")
     * @ORM\JoinColumn(nullable=false)
     */
    protected $colisage;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nbrePaquet
     *
     * @param integer $nbrePaquet
     * @return Paquet
     */
    public function setNbrePaquet($nbrePaquet)
    {
        $this->nbrePaquet = $nbrePaquet;

        return $this;
    }

    /**
     * Get nbrePaquet
     *
     * @return integer 
     */
    public function getNbrePaquet()
    {
        return $this->nbrePaquet;
    }

    /**
     * Set cubage
     *
     * @param string $cubage
     * @return Paquet
     */
    public function setCubage($cubage){}

    /**
     * Get cubage
     *
     * @return string 
     */
    public function getCubage(){
        $v = $this->nbrePaquet * $this->piece * $this->hauteur * $this->largeur * $this->epaisseur_p;
        return number_format($v,3,'.',',');
    }

    /**
     * Set piece
     *
     * @param integer $piece
     * @return Paquet
     */
    public function setPiece($piece)
    {
        $this->piece = $piece;

        return $this;
    }

    /**
     * Get piece
     *
     * @return integer 
     */
    public function getPiece()
    {
        return $this->piece;
    }

    /**
     * Set hauteur
     *
     * @param string $hauteur
     * @return Paquet
     */
    public function setHauteur($hauteur)
    {
        $this->hauteur = $hauteur;

        return $this;
    }

    /**
     * Get hauteur
     *
     * @return string 
     */
    public function getHauteur()
    {
        return $this->hauteur;
    }

    /**
     * Set largeur
     *
     * @param string $largeur
     * @return Paquet
     */
    public function setLargeur($largeur)
    {
        $this->largeur = $largeur;

        return $this;
    }

    /**
     * Get largeur
     *
     * @return string 
     */
    public function getLargeur()
    {
        return $this->largeur;
    }

    /**
     * Set epaisseurP
     *
     * @param string $epaisseurP
     * @return Paquet
     */
    public function setEpaisseurP($epaisseur_p)
    {
        $this->epaisseur_p = $epaisseur_p;

        return $this;
    }

    /**
     * Get epaisseurP
     *
     * @return string 
     */
    public function getEpaisseurP()
    {
        return $this->epaisseur_p;
    }

    public function __toString(){
        return ($this->getId())? "Paquet: ".$this->getNbrePaquet()." ".$this->getEssence():"N/A";
    }

}
