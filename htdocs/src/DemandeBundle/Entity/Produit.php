<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * @ORM\Entity
 * @ORM\Table(name="produit")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap( {"grume" = "Grume", "botte" = "Bottes", "paquet" = "Paquet"} )
 */
abstract class Produit
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="ExploitationBundle\Entity\Essence" ,cascade={"persist"})
    * 
    */
    protected $essence;

    protected $colisage;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set essence
     *
     * @param string $essence
     * @return Bois
     */
    public function setEssence($essence)
    {
        $this->essence = $essence;

        return $this;
    }

    /**
     * Get essence
     *
     * @return string 
     */
    public function getEssence()
    {
        return $this->essence;
    }

     /**
     * Set cubage
     *
     * @param string $cubage
     * @return Bottes
     */
    abstract public function setCubage($cubage);

    /**
     * Get cubage
     *
     * @return string 
     */
    abstract public function getCubage();  

    /**
     * Set colisage
     */
    public function setColisage($colisage)
    {
        $this->colisage = $colisage;

        return $this;
    }

    /**
     * Get colisage
     * 
     */
    public function getColisage()
    {
        return $this->colisage;
    }  

}
