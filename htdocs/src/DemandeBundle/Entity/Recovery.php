<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvoiceBundle\Model\InvoiceTemplate;
use TrajetBundle\Model\Checkpoint;
use TrajetBundle\Model\Snapshot;

/**
 * Recovery
 *
 * @ORM\Table(name="recovery")
 * @ORM\Entity(repositoryClass="DemandeBundle\Repository\RecoveryRepository")
 */
class Recovery implements Snapshot,InvoiceTemplate{
    const STEP_ENCODING = 0;
    const STEP_VERIFYING = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
    * 
    */
    private $exploitant;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $controller;

    /**
     * @var int
     *
     * @ORM\Column(name="step", type="integer")
     */
    private $step;

    /**
     * @var bool
     *
     * @ORM\Column(name="verified", type="boolean", options={"default":false})
     */
    private $verified;

    /**
     * @var int
     *
     * @ORM\Column(name="nombre_grume_saisie", type="integer")
     */
    private $nombreGrumeSaisie;

    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=255, nullable=true)
     */
    private $site;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_approbation", type="datetime")
     */
    private $dateApprobation;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=100,unique=true)
     */
    private $reference;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_enregistree", type="datetime")
     */
    private $dateEnregistree;

    /**
     * 
     * @ORM\OneToMany(targetEntity="DemandeBundle\Entity\RecoveryGrume",mappedBy="recovery",orphanRemoval=true,cascade={"persist","remove"})
     */ 
    private $grume;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dateEnregistree = new \DateTime();  
        $this->verified = false;
        $this->step = self::STEP_ENCODING;
        $this->controller = null; 
        $this->grume = new \Doctrine\Common\Collections\ArrayCollection();      
    }

    public function __toString()
    {
        return ($this->getId())? sprintf("De %s [REF: %s]",$this->getExploitant(),$this->getReference()):"n/a";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exploitant
     *
     * @param \CorporateBundle\Entity\Entreprise $exploitant
     * @return Recovery
     */
    public function setExploitant($exploitant)
    {
        $this->exploitant = $exploitant;

        return $this;
    }

    /**
     * Get exploitant
     *
     * @return \CorporateBundle\Entity\Entreprise
     */
    public function getExploitant()
    {
        return $this->exploitant;
    }

    /**
     * Set nombreGrumeSaisie
     *
     * @param integer $nombreGrumeSaisie
     * @return Recovery
     */
    public function setNombreGrumeSaisie($nombreGrumeSaisie)
    {
        $this->nombreGrumeSaisie = $nombreGrumeSaisie;

        return $this;
    }

    /**
     * Get nombreGrumeSaisie
     *
     * @return integer 
     */
    public function getNombreGrumeSaisie()
    {
        return $this->nombreGrumeSaisie;
    }

    /**
     * Set site
     *
     * @param string $site
     * @return Recovery
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set dateApprobation
     *
     * @param \DateTime $dateApprobation
     * @return Recovery
     */
    public function setDateApprobation($dateApprobation)
    {
        $this->dateApprobation = $dateApprobation;

        return $this;
    }

    /**
     * Get dateApprobation
     *
     * @return \DateTime 
     */
    public function getDateApprobation()
    {
        return $this->dateApprobation;
    }

    /**
     * Set dateEnregistree
     *
     * @param \DateTime $dateEnregistree
     * @return Recovery
     */
    public function setDateEnregistree($dateEnregistree)
    {
        $this->dateEnregistree = $dateEnregistree;

        return $this;
    }

    /**
     * Get dateEnregistree
     *
     * @return \DateTime 
     */
    public function getDateEnregistree()
    {
        return $this->dateEnregistree;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "recovery";
    }

    /**
     * @return boolean
     */
    public function isLocked()
    {
        return $this->verified;
    }

    /**
     * @param boolean $lock
     */
    public function setLocked($lock)
    {
        $this->verified = $lock;
    }

    /**
     * @return array
     */
    public function getIdentifier()
    {
        return array(
            "id" => $this->getId(),
        );
    }

    public static function getSteps(){
        return [
            self::STEP_ENCODING => "Encodage",
            self::STEP_VERIFYING => "Contrôle",
        ];
    }

    public function configureCaptureFields(){
         return [
            "step",
            "verified",
        ];
    }

    /**
     * Set step
     *
     * @param integer $step
     * @return Recovery
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return integer 
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set verified
     *
     * @param boolean $verified
     * @return Recovery
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;

        return $this;
    }

    /**
     * Get verified
     *
     * @return boolean 
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Recovery
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Recovery
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set controller
     *
     * @param \Application\Sonata\UserBundle\Entity\User $controller
     * @return Recovery
     */
    public function setController(\Application\Sonata\UserBundle\Entity\User $controller = null)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * Add grume
     *
     * @param \DemandeBundle\Entity\RecoveryGrume $grume
     * @return Recovery
     */
    public function addGrume(\DemandeBundle\Entity\RecoveryGrume $grume)
    {
        $this->grume[] = $grume;
        $grume->setRecovery($this);
        return $this;
    }

    /**
     * Remove grume
     *
     * @param \DemandeBundle\Entity\RecoveryGrume $grume
     */
    public function removeGrume(\DemandeBundle\Entity\RecoveryGrume $grume)
    {
        $this->grume->removeElement($grume);
        $grume->setRecovery(null);
    }

    /**
     * Get grume
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getGrume()
    {
        return $this->grume;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection|array $grume
     */
    public function setGrume($grume){
        if(gettype($grume) == 'array'){
            $grume = new \Doctrine\Common\Collections\ArrayCollection($grume);
        }
        /** @var RecoveryGrume $i */
        foreach ($grume as $i) {
            $i->setRecovery($this);
        }

        $this->grume = $grume;
    }

    public function getGrumeCount(){
        return count($this->grume);
    }

    public function getNormalGrumeCount(){
        $c = $this->getGrumeCount() - $this->getNombreGrumeSaisie();
        return ($c > 0)? $c:0;
    }

    public function getCubage(){
        $c = 0.0;
        /** @var RecoveryGrume $g */
        foreach ($this->grume as $g) {
            $c += $g->getCubage();
        }

        return $c;
    }

    public function getCubageSA(){
        $c = 0.0;
        /** @var RecoveryGrume $g */
        foreach ($this->grume as $g) {
            $c += $g->getCubageSA();
        }

        return $c;
    }

    /**
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->getId();
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getInvoicePrefix()
    {
        return "REC";
    }

    /**
     * @return \CorporateBundle\Entity\Entreprise
     */
    public function getTarget()
    {
        return $this->getExploitant();
    }


}
