<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * RecoveryGrume
 *
 * @ORM\Table(name="recovery_grume")
 * @ORM\Entity(repositoryClass="DemandeBundle\Repository\RecoveryGrumeRepository")
 */
class RecoveryGrume
{    
    const STATUS_NON_SAISIE = 0;
    const STATUS_SAISIE = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\GreaterThan(0)
     * @ORM\Column(name="num_abattage", type="string", length=20)
     */
    private $numAbattage;

    /**
    * @ORM\ManyToOne(targetEntity="ExploitationBundle\Entity\Essence")
    * 
    */
    private $essence;

    /**
     * @var string
     * @Assert\GreaterThan(0)
     * @ORM\Column(name="longueur", type="decimal", precision=2, scale=1)
     */
    private $longueur;

    /**
     * @var string
     *
     * @ORM\Column(name="num_permis", type="string", length=255)
     */
    private $numPermis;

    /**
     * @var string
     *
     * @ORM\Column(name="diam_aa", type="float", precision=1, scale=3)
     */
    private $diamAA;

    /**
     * @var string
     *
     * @ORM\Column(name="diam_sa", type="float", precision=1, scale=3, nullable=true)
     */
    private $diamSA;

    /**
     * @var integer
     *
     * @ORM\Column(name="etat", type="integer", nullable=true)
     */
    private $etat;

     /**
     *
     * @ORM\ManyToOne(targetEntity="DemandeBundle\Entity\Recovery",inversedBy="grume")
     * @ORM\JoinColumn(nullable=false)
     */ 
    private $recovery;

    private $cubage;

    private $cubagesa;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numAbattage
     *
     * @param string $numAbattage
     * @return RecoveryGrume
     */
    public function setNumAbattage($numAbattage)
    {
        $this->numAbattage = $numAbattage;

        return $this;
    }

    /**
     * Get numAbattage
     *
     * @return string 
     */
    public function getNumAbattage()
    {
        return $this->numAbattage;
    }

    /**
     * Set essence
     *
     * @param string $essence
     * @return RecoveryGrume
     */
    public function setEssence($essence)
    {
        $this->essence = $essence;

        return $this;
    }

    /**
     * Get essence
     *
     * @return string 
     */
    public function getEssence()
    {
        return $this->essence;
    }

    /**
     * Set longueur
     *
     * @param string $longueur
     * @return RecoveryGrume
     */
    public function setLongueur($longueur)
    {
        $this->longueur = $longueur;

        return $this;
    }

    /**
     * Get longueur
     *
     * @return string 
     */
    public function getLongueur()
    {
        return $this->longueur;
    }

    /**
     * Set numPermis
     *
     * @param string $numPermis
     * @return RecoveryGrume
     */
    public function setNumPermis($numPermis)
    {
        $this->numPermis = $numPermis;

        return $this;
    }

    /**
     * Get numPermis
     *
     * @return string 
     */
    public function getNumPermis()
    {
        return $this->numPermis;
    }

    /**
     * Set diamAa
     *
     * @param string $diamAA
     * @return RecoveryGrume
     */
    public function setDiamAA($diamAA)
    {
        $this->diamAA = $diamAA;

        return $this;
    }

    /**
     * Get diamAA
     *
     * @return string 
     */
    public function getDiamAA()
    {
        return $this->diamAA;
    }

    /**
     * Set diamSA
     *
     * @param string $diamSA
     * @return RecoveryGrume
     */
    public function setDiamSA($diamSA)
    {
        $this->diamSA = $diamSA;

        return $this;
    }

    /**
     * Get diamSA
     *
     * @return string 
     */
    public function getDiamSA()
    {
        return $this->diamSA;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->essence = new \Doctrine\Common\Collections\ArrayCollection();
        $this->etat = self::STATUS_NON_SAISIE;
    }

    /**
     * Add essence
     *
     * @param \ExploitationBundle\Entity\Essence $essence
     * @return RecoveryGrume
     */
    public function addEssence(\ExploitationBundle\Entity\Essence $essence)
    {
        $this->essence[] = $essence;

        return $this;
    }

    /**
     * Remove essence
     *
     * @param \ExploitationBundle\Entity\Essence $essence
     */
    public function removeEssence(\ExploitationBundle\Entity\Essence $essence)
    {
        $this->essence->removeElement($essence);
    }

    /**
     * Set recovery
     *
     * @param \DemandeBundle\Entity\Recovery $recovery
     * @return RecoveryGrume
     */
    public function setRecovery(\DemandeBundle\Entity\Recovery $recovery)
    {
        $this->recovery = $recovery;

        return $this;
    }

    /**
     * Get recovery
     *
     * @return \DemandeBundle\Entity\Recovery 
     */
    public function getRecovery()
    {
        return $this->recovery;
    }

      /**
     * Set cubage
     *
     * @param string $cubage
     * 
     */
    public function setCubage($cubage){}

    /**
     * Get cubage
     *
     * @return double
     */

    public function getCubage(){
        return $this->calcul($this->diamAA);
    }

    /**
     * @return double
     */
     public function getCubageSA(){
        return $this->calcul($this->diamSA);
    }

    /**
     * @param $d
     * @return double
     */
    private function calcul($d){
        $v = $this->longueur * $d * $d * (pi()/4);
        return number_format($v,3,'.',',');
    }

    public static function getAvailableStatus(){
        return array(            
            self::STATUS_NON_SAISIE => "Non-saisie",
            self::STATUS_SAISIE => "Saisie",
        );
    }

    /**
     * Set etat
     *
     * @param integer $etat
     * @return RecoveryGrume
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }
}
