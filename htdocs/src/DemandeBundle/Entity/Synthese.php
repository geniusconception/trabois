<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvoiceBundle\Model\InvoiceTemplate;
use TrajetBundle\Model\Checkpoint;
use TrajetBundle\Model\Snapshot;

/**
 * @ORM\Entity
 * @ORM\Table(name="synthese")
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="string")
 * @ORM\DiscriminatorMap( {"grume" = "SyntheseGrume"} )
 */
abstract class Synthese implements Snapshot,InvoiceTemplate
{
    const STEP_ENCODING = 0;
    const STEP_VERIFYING = 1;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
    * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
    * 
    */
    private $exploitant;

    /**
     * @var string
     *
     * @ORM\Column(name="permis", type="string", length=100, nullable=true)
     */
    private $permis;

    /**
    * @ORM\ManyToMany(targetEntity="ExploitationBundle\Entity\Essence")
    * 
    */
    private $essence;

    /**
     * @var string
     *
     * @ORM\Column(name="site", type="string", length=100, nullable=true)
     */
    private $site;

    /**
     * @var string
     *
     * @ORM\Column(name="volume", type="decimal", precision=10, scale=3)
     */
    private $volume;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $controller;

    /**
     * @var int
     *
     * @ORM\Column(name="step", type="integer")
     */
    private $step;

    /**
     * @var bool
     *
     * @ORM\Column(name="verified", type="boolean", options={"default":false})
     */
    private $verified;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=100,unique=true)
     */
    private $reference;

    /**
     * 
     * @ORM\OneToMany(targetEntity="DemandeBundle\Entity\TigeEssence",mappedBy="synthese",orphanRemoval=true,cascade={"persist","remove"})
     */ 
    private $tige;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_redigee", type="datetime")
     */
    private $dateRedigee;

     /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_enregistree", type="datetime")
     */
    private $dateEnregistree;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \CorporateBundle\Entity\Entreprise
     */
    public function getExploitant()
    {
        return $this->exploitant;
    }

    /**
     * @param mixed $exploitant
     */
    public function setExploitant(\CorporateBundle\Entity\Entreprise $exploitant)
    {
        $this->exploitant = $exploitant;
    }

    /**
     * Set permis
     *
     * @param string $permis
     * @return Synthese
     */
    public function setPermis($permis)
    {
        $this->permis = $permis;

        return $this;
    }

    /**
     * Get permis
     *
     * @return string 
     */
    public function getPermis()
    {
        return $this->permis;
    }

    /**
     * Set essence
     *
     * @param string $essence
     * @return Synthese
     */
    public function setEssence($essence)
    {
        $this->essence = $essence;

        return $this;
    }

    /**
     * Get essence
     *
     * @return string 
     */
    public function getEssence()
    {
        return $this->essence;
    }

    /**
     * Set site
     *
     * @param string $site
     * @return Synthese
     */
    public function setSite($site)
    {
        $this->site = $site;

        return $this;
    }

    /**
     * Get site
     *
     * @return string 
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * Set volume
     *
     * @param string $volume
     * @return Synthese
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return string 
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set step
     *
     * @param integer $step
     * @return Synthese
     */
    public function setStep($step)
    {
        $this->step = $step;

        return $this;
    }

    /**
     * Get step
     *
     * @return integer 
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * Set verified
     *
     * @param boolean $verified
     * @return Synthese
     */
    public function setVerified($verified)
    {
        $this->verified = $verified;

        return $this;
    }

    /**
     * Get verified
     *
     * @return boolean 
     */
    public function getVerified()
    {
        return $this->verified;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Synthese
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->essence = new \Doctrine\Common\Collections\ArrayCollection();
        $this->verified = false;
        $this->step = self::STEP_ENCODING;
        $this->dateEnregistree = new \DateTime();
        $this->controller = null;
        $this->tige = new \Doctrine\Common\Collections\ArrayCollection();
    }

    public function __toString()
    {
        return ($this->getId())? sprintf("De %s [REF: %s]",$this->getExploitant(),$this->getReference()):"n/a";
    }

    /**
     * Add essence
     *
     * @param \ExploitationBundle\Entity\Essence $essence
     * @return Synthese
     */
    public function addEssence(\ExploitationBundle\Entity\Essence $essence)
    {
        $this->essence[] = $essence;

        return $this;
    }

    /**
     * Remove essence
     *
     * @param \ExploitationBundle\Entity\Essence $essence
     */
    public function removeEssence(\ExploitationBundle\Entity\Essence $essence)
    {
        $this->essence->removeElement($essence);
    }

    /**
     * Set dateRedigee
     *
     * @param \DateTime $dateRedigee
     * @return Synthese
     */
    public function setDateRedigee($dateRedigee)
    {
        $this->dateRedigee = $dateRedigee;

        return $this;
    }

    /**
     * Get dateRedigee
     *
     * @return \DateTime 
     */
    public function getDateRedigee()
    {
        return $this->dateRedigee;
    }

    /**
     * Set dateEnregistree
     *
     * @param \DateTime $dateEnregistree
     * @return Synthese
     */
    public function setDateEnregistree($dateEnregistree)
    {
        $this->dateEnregistree = $dateEnregistree;

        return $this;
    }

    /**
     * Get dateEnregistree
     *
     * @return \DateTime 
     */
    public function getDateEnregistree()
    {
        return $this->dateEnregistree;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Synthese
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "synthesis";
    }

    /**
     * @return boolean
     */
    public function isLocked()
    {
        return $this->verified;
    }

    /**
     * @param boolean $lock
     */
    public function setLocked($lock)
    {
        $this->verified = $lock;
    }

    /**
     * @return array
     */
    public function getIdentifier()
    {
        return array(
            "id" => $this->getId(),
        );
    }

    public static function getSteps(){
        return [
            self::STEP_ENCODING => "Encodage",
            self::STEP_VERIFYING => "Contrôle",
        ];
    }


    /**
     * Set controller
     *
     * @param \Application\Sonata\UserBundle\Entity\User $controller
     * @return Synthese
     */
    public function setController(\Application\Sonata\UserBundle\Entity\User $controller = null)
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * Get controller
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getController()
    {
        return $this->controller;
    }

    /**
     * @return array
     */
    public function configureCaptureFields()
    {
        return [
            "step",
            "verified",
        ];
    }

    /**
     * Add tige
     *
     * @param \DemandeBundle\Entity\TigeEssence $tige
     * @return Synthese
     */
    public function addTige(\DemandeBundle\Entity\TigeEssence $tige)
    {
        $this->tige[] = $tige;
        $tige->setSynthese($this);
        return $this;
    }

    /**
     * Remove tige
     *
     * @param \DemandeBundle\Entity\TigeEssence $tige
     */
    public function removeTige(\DemandeBundle\Entity\TigeEssence $tige)
    {
        $this->tige->removeElement($tige);
        $tige->setSynthese(null);
    }

    /**
     * Get tige
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTige()
    {
        return $this->tige;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection|array $tige
     */
    public function setTige($tige){
        if(gettype($tige) == 'array'){
            $tige = new \Doctrine\Common\Collections\ArrayCollection($tige);
        }
        /** @var Tige $i */
        foreach ($tige as $i) {
            $i->setSynthese($this);
        }

        $this->tige = $tige;
    }
}
