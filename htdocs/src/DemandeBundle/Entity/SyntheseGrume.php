<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvoiceBundle\Model\InvoiceTemplate;

/**
 * SyntheseGrume
 *
 * @ORM\Entity()
 */
class SyntheseGrume extends Synthese implements InvoiceTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="GrumeDeclare", type="integer")
     */
    private $grumeDeclare;

    /**
     * @var int
     *
     * @ORM\Column(name="GrumeDepasse", type="integer")
     */
    private $grumeDepasse;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set grumeDeclare
     *
     * @param integer $grumeDeclare
     * @return SyntheseGrume
     */
    public function setGrumeDeclare($grumeDeclare)
    {
        $this->grumeDeclare = $grumeDeclare;

        return $this;
    }

    /**
     * Get grumeDeclare
     *
     * @return integer 
     */
    public function getGrumeDeclare()
    {
        return $this->grumeDeclare;
    }

    /**
     * Set grumeDepasse
     *
     * @param integer $grumeDepasse
     * @return SyntheseGrume
     */
    public function setGrumeDepasse($grumeDepasse)
    {
        $this->grumeDepasse = $grumeDepasse;

        return $this;
    }

    /**
     * Get grumeDepasse
     *
     * @return integer 
     */
    public function getGrumeDepasse()
    {
        return $this->grumeDepasse;
    }

    public function getTotalBois()
    {
       return $this->getGrumeDeclare() - $this->getGrumeDepasse();
        
    }

    /**
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->getId();
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return null;
    }

    /**
     * @return string
     */
    public function getInvoicePrefix()
    {
        return "GRM";
    }

    /**
     * @return \CorporateBundle\Entity\Entreprise
     */
    public function getTarget()
    {
        return $this->getExploitant();
    }


}
