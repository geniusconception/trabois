<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvoiceBundle\Entity\Invoice;
use TrajetBundle\Model\Checkpoint;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Tagger
 *
 * @ORM\Table(name="tagger")
 * @ORM\Entity(repositoryClass="DemandeBundle\Repository\TaggerRepository")
 */
class Tagger implements Checkpoint
{
    const TYPE = "presystem";
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     * @Assert\GreaterThan(-1)
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;
    /**
     * @var int
     * @Assert\GreaterThan(-1)
     * @ORM\Column(name="dep", type="integer")
     */
    private $dep;
    /**
     * @var \InvoiceBundle\Entity\Invoice
     * @Assert\NotNull()
     * @ORM\OneToOne(targetEntity="\InvoiceBundle\Entity\Invoice", cascade={"persist"})
     */
    private $invoice;
    /**
     * @var boolean
     *
     * @ORM\Column(name="locked",type="boolean")
     */
    private $locked;
    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     */
    private $user;

    function __construct()
    {
        $this->locked = false;
        $this->qte = 0;
        $this->dep = 0;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     * @return Tagger
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return integer 
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set invoice
     *
     * @param \InvoiceBundle\Entity\Invoice $invoice
     * @return Tagger
     */
    public function setInvoice(\InvoiceBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \InvoiceBundle\Entity\Invoice 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'tagger';
    }

    /**
     * @return boolean
     */
    public function isLocked()
    {
        return $this->locked;
    }

    /**
     * @param boolean $lock
     */
    public function setLocked($lock)
    {
        $this->locked = $lock;
    }

    /**
     * @return array
     */
    public function getIdentifier()
    {
        return array("id" => $this->id);
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Tagger
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context){
        if($this->invoice){
            if($this->invoice->getStatus() !== Invoice::STATUS_PAID)
                $context->buildViolation("La facture séléctionnée doit être aquitté")->atPath('invoice')->addViolation();
            if($this->invoice->isLocked())
                $context->buildViolation("La facture séléctionnée est déjà utilisée")->atPath('invoice')->addViolation();
        }
        if($this->qte + $this->dep <= 0){
            $context->buildViolation("La qté demandée est invalide")->atPath('qte')->addViolation();
        }
    }

    /**
     * @return int
     */
    public function getDep()
    {
        return $this->dep;
    }

    /**
     * @param int $dep
     */
    public function setDep($dep)
    {
        $this->dep = $dep;
    }

    function __toString()
    {
        return ($this->getId())? sprintf("%s (x %d etiquettes)",$this->invoice->getRef(),$this->qte):"n/a";
    }
}
