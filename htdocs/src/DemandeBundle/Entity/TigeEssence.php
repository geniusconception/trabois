<?php

namespace DemandeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * TigeEssence
 *
 * @ORM\Table(name="tige_essence")
 * @ORM\Entity(repositoryClass="DemandeBundle\Repository\TigeEssenceRepository")
 */
class TigeEssence
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="ExploitationBundle\Entity\Essence")
     */ 
    private $essence;

    /**
     * @var int
     * @Assert\GreaterThan(0)
     * @ORM\Column(name="nombre_arbre", type="integer")
     */
    private $nombreArbre;

    /**
     * @var string
     *
     * @ORM\Column(name="cubage", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $cubage;

    /**
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="DemandeBundle\Entity\Synthese",inversedBy="tige")
     * @ORM\JoinColumn(nullable=false)
     */
    private $synthese;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set essence
     *
     * @param string $essence
     * @return TigeEssence
     */
    public function setEssence($essence)
    {
        $this->essence = $essence;

        return $this;
    }

    /**
     * Get essence
     *
     * @return string 
     */
    public function getEssence()
    {
        return $this->essence;
    }

    /**
     * Set nombreArbre
     *
     * @param integer $nombreArbre
     * @return TigeEssence
     */
    public function setNombreArbre($nombreArbre)
    {
        $this->nombreArbre = $nombreArbre;

        return $this;
    }

    /**
     * Get nombreArbre
     *
     * @return integer 
     */
    public function getNombreArbre()
    {
        return $this->nombreArbre;
    }

    /**
     * Set cubage
     *
     * @param string $cubage
     * @return TigeEssence
     */
    public function setCubage($cubage)
    {
        $this->cubage = $cubage;

        return $this;
    }

    /**
     * Get cubage
     *
     * @return string 
     */
    public function getCubage()
    {
        return $this->cubage;
    }

    /**
     * Set synthese
     *
     * @param \DemandeBundle\Entity\Synthese $synthese
     * @return TigeEssence
     */
    public function setSynthese(\DemandeBundle\Entity\Synthese $synthese)
    {
        $this->synthese = $synthese;

        return $this;
    }

    /**
     * Get synthese
     *
     * @return \DemandeBundle\Entity\Synthese 
     */
    public function getSynthese()
    {
        return $this->synthese;
    }
}
