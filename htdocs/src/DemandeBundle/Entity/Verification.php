<?php

namespace DemandeBundle\Entity;

use DemandeBundle\Model\DemandeInterface;
use Doctrine\ORM\Mapping as ORM;
use InvoiceBundle\Model\InvoiceTemplate;
use TrajetBundle\Model\Checkpoint;

/**
 * Verification
 *
 * @ORM\Table(name="verification")
 * @ORM\Entity(repositoryClass="DemandeBundle\Repository\VerificationRepository")
 */
class Verification implements DemandeInterface, Checkpoint, InvoiceTemplate
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * 
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var Demande
     *
     * @ORM\OneToOne(targetEntity="DemandeBundle\Entity\Demande")
     * @ORM\JoinColumn(nullable=true)
     */
    private $demande;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_enregistree", type="datetime",nullable=true)
     */
    private $dateEnregistree;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked;

    /**
     * @param Demande $d
     */
    public function __construct(Demande $d = null)
    {
        $this->locked = false;
        $this->demande = $d;
    }

    /**
    * Get id
    *
    * @return integer 
    */
    public function getId()
    {
        return $this->id;
    }
    

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Verification
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set demande
     *
     * @param \DemandeBundle\Entity\Demande $demande
     * @return Verification
     */
    public function setDemande(\DemandeBundle\Entity\Demande $demande = null)
    {
        $this->demande = $demande;

        return $this;
    }

    /**
     * Get demande
     *
     * @return \DemandeBundle\Entity\Demande 
     */
    public function getDemande()
    {
        return $this->demande;
    }
     

    public function setDateRedigee($dateRedigee)
    {
        $this->demande->setDateRedigee($dateRedigee);//Du faite que la classe verification n'a pas un attribut DateRedigee là maintenant on va pointer la classe demande pour qu'il puisse nous le donner
        return $this;
    }

     
    public function getDateRedigee()
    {
        return $this->demande->getDateRedigee();
    }

   
    public function setDateEnregistree($dateEnregistree)
    {
        $this->dateEnregistree = $dateEnregistree;
        return $this;
    }


    public function getDateEnregistree()
    {
        return $this->dateEnregistree;
    }

     public function setSource(\CorporateBundle\Entity\Entreprise $source = null)
    {
       $this->demande->setSource($source);
       return $this;
    }
   
    public function getSource()
    {
        return $this->demande->getSource();
    }


    function __toString()
    {

        return ($this->getDemande())? sprintf("[vérification] %s",$this->demande->__toString()):"n/a";
    }


    public function getName()
    {
       return "verification";
    }

    public function isLocked()
    {
        return $this->locked;
    }

    public function setLocked($lock)
    {
        $this->locked = $lock;
    }

    public function getIdentifier()
    {
        return array("id" => $this->getId());
    }

    /**
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->getId();
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return array();
    }

    /**
     * @return string
     */
    public function getInvoicePrefix()
    {
        return "DMD";
    }

    /**
     * @return \CorporateBundle\Entity\Entreprise
     */
    public function getTarget()
    {
        return $this->getSource();
    }

    public function setReference($reference)
    {
        $this->demande->setReference($reference);
    }

    public function getReference()
    {
        return $this->demande->getReference();
    }

    public function getListe()
    {
        return $this->demande->getListe();
    }

    public function setListe(\DemandeBundle\Entity\Colisage $liste)
    {
        $this->demande->setListe($liste);
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return "";
    }


}