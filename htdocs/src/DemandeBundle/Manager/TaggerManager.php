<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 03/07/2017
 * Time: 16:06
 */

namespace DemandeBundle\Manager;


use AppBundle\Model\Compteur;
use DemandeBundle\Entity\Tagger;
use Doctrine\ORM\EntityManagerInterface;
use TagBundle\Entity\Tag;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AppBundle\Security\AclManager;

class TaggerManager {

    const PREFIX = "PST";
    /**
     * @var Compteur
     */
    private $compteur;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
    /**
     * @var AclManager
     */
    private $acl;

    function __construct(EntityManagerInterface $em,Compteur $compteur,TokenStorageInterface $tokenStorageInterface,AclManager $acl)
    {
        $this->compteur = $compteur;
        $this->em = $em;
        $this->tokenStorage = $tokenStorageInterface;
        $this->acl = $acl;
    }

    public function generateTags(Tagger $tagger){
        /** @var \CorporateBundle\Entity\Entreprise $exp */
        $exp = $tagger->getInvoice()->getTarget();
        $user = $this->tokenStorage->getToken()->getUser();
        $tags = [];
        $n = $tagger->getQte() + $tagger->getDep();

        for($i=0; $i<$n; $i++){
            $type = ($i<$tagger->getQte())? Tag::TYPE_PRESYSTEM: Tag::TYPE_SAISIE;
            $tag = new Tag();
            $tag->setType($type);
            $tag->setExploitant($exp);
            $tag->setUser($user);
            $tag->setUsed(false);

            $d = new \DateTime();
            $id = $this->compteur->nextValue(self::PREFIX);
            $code = sprintf("%s%d%d%d%s",self::PREFIX,$d->format("Y"),$d->format("m"),$d->format("d"),$id);
            $tag->setCode($code);

            $this->em->persist($tag);
            $tags[] = $tag;
        }

        $tagger->setLocked(true);
        $this->em->persist($tagger);
        $this->em->flush();

        foreach ($tags as $tag) {
            $this->acl->saveAcl($tag,$user);
        }

        return $tagger;
    }
} 