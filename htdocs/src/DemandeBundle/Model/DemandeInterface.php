<?php
namespace DemandeBundle\Model;	

    interface DemandeInterface
	{
		public function setDateRedigee($dateRedigee);
		public function getDateRedigee();
		public function setDateEnregistree($dateEnregistree);
		public function getDateEnregistree();
		public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null);
		public function getUser();
		public function setSource(\CorporateBundle\Entity\Entreprise $source = null);
		public function getSource();
        public function setReference($reference);
        public function getReference();

        /**
         * @return \DemandeBundle\Entity\Colisage
         */
        public function getListe();
        public function setListe(\DemandeBundle\Entity\Colisage $liste);
 	}
