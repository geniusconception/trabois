<?php

namespace DemandeBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * RecoveryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RecoveryRepository extends EntityRepository
{
}
