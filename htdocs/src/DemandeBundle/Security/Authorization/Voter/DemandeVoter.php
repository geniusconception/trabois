<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 29/05/2017
 * Time: 14:59
 */

namespace DemandeBundle\Security\Authorization\Voter;


use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class DemandeVoter extends AclVoter{

    public function supportsClass($class)
    {
        return $class == 'DemandeBundle\Entity\Demande';
    }

    public function supportsAttribute($attribute)
    {
        return in_array($attribute,["EDIT","DELETE","SUBMIT"]);
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }

        foreach ($attributes as $attribute) {
            if ($this->supportsAttribute($attribute)) {
                /** @var \DemandeBundle\Entity\Demande  $object */
                if($object->isLocked()){
                    return self::ACCESS_DENIED;
                }
            }
        }

        return self::ACCESS_ABSTAIN;
    }


} 