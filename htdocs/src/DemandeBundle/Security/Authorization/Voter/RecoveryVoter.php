<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 04/07/2017
 * Time: 20:57
 */

namespace DemandeBundle\Security\Authorization\Voter;

use Application\Sonata\UserBundle\Entity\User;
use DemandeBundle\Entity\Recovery;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Role\Role;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Permission\PermissionMapInterface;
use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Role\RoleHierarchyInterface;

class RecoveryVoter extends AclVoter{

    const ENCODER = "ROLE_ENCODER";
    const CONTROLLER = "ROLE_CONTROLLER";
    /**
     * @var RoleHierarchyInterface
     */
    protected $rh;

    public function __construct(RoleHierarchyInterface $rh,AclProviderInterface $aclProvider, ObjectIdentityRetrievalStrategyInterface $oidRetrievalStrategy, SecurityIdentityRetrievalStrategyInterface $sidRetrievalStrategy, PermissionMapInterface $permissionMap, LoggerInterface $logger = null, $allowIfObjectIdentityUnavailable = true)
    {
        parent::__construct($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap, $logger, $allowIfObjectIdentityUnavailable);
        $this->rh = $rh;
    }

    public function supportsClass($class)
    {
        $Recovery = '\DemandeBundle\Entity\Recovery';
        $RecoveryAdmin = '\DemandeBundle\Admin\RecoveryAdmin';
        return ($class instanceof $Recovery) || $class instanceof $RecoveryAdmin;
    }

    public function supportsAttribute($attribute)
    {
        return in_array($attribute,array('CREATE','VIEW','EDIT','DELETE','SUBMIT','VERIFY'));
    }

    public function vote(TokenInterface $token, $object, array $attributes){
        if (!$this->supportsClass($object)) {
            return self::ACCESS_ABSTAIN;
        }

        $Recovery = '\DemandeBundle\Entity\Recovery';
        $user = $token->getUser();

        foreach ($attributes as $attribute) {
            if ($this->supportsAttribute($attribute)) {
                if($this->checkRole($user,self::ENCODER)){
                    if($attribute === "CREATE")
                        return self::ACCESS_GRANTED;
                    if($object instanceof $Recovery){
                        if($attribute === "VERIFY")
                            return self::ACCESS_DENIED;
                        if($attribute === "VIEW")
                            return self::ACCESS_GRANTED;
                        /** @var \DemandeBundle\Entity\Recovery $object */
                        if($object->getStep() === Recovery::STEP_VERIFYING)
                            return self::ACCESS_DENIED;
                        /** @var \DemandeBundle\Entity\Recovery $object */
                        if($attribute === "SUBMIT" && $object->getStep() === Recovery::STEP_ENCODING && $object->getUser()->getId() == $user->getId())
                            return self::ACCESS_GRANTED;
                        if($attribute === "EDIT" && $object->getUser()->getId() != $user->getId())
                            return self::ACCESS_DENIED;
                    }
                }elseif($this->checkRole($user,self::CONTROLLER)){
                    if($attribute === "CREATE")
                        return self::ACCESS_DENIED;
                    if($object instanceof $Recovery ){
                        /** @var \DemandeBundle\Entity\Recovery $object */
                        if($object->getStep() === Recovery::STEP_ENCODING)
                            return self::ACCESS_DENIED;
                        if($object->isLocked() && $attribute !== "VIEW")
                            return self::ACCESS_DENIED;
                        if($object->getStep() == Recovery::STEP_VERIFYING && $attribute === "VERIFY")
                            return self::ACCESS_GRANTED;
                    }
                }
            }
        }

        return self::ACCESS_ABSTAIN;
    }

    protected function checkRole(User $user,$role){
        $role = new Role($role);
        foreach ($user->getRoles() as $uRole) {
            if(in_array($role,$this->rh->getReachableRoles(array(new Role($uRole))))){
                return true;
            }
        }
        return false;
    }
} 