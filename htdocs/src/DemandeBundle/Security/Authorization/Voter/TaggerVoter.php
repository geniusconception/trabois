<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 28/06/2017
 * Time: 22:53
 */

namespace DemandeBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class TaggerVoter extends AclVoter{

    public function supportsClass($class)
    {
        return in_array($class,array('DemandeBundle\Entity\Tagger'));
    }

    public function supportsAttribute($attribute)
    {
        return $attribute !== "VIEW" && $attribute !== "LIST";
    }

    public function vote(TokenInterface $token, $object, array $attributes){
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }
        foreach ($attributes as $attribute) {
            if ($this->supportsAttribute($attribute)) {
                /** @var \DemandeBundle\Entity\Tagger $object */
                if ($object->isLocked()) {
                    return self::ACCESS_DENIED;
                }
            }
        }
        return self::ACCESS_ABSTAIN;
    }
} 