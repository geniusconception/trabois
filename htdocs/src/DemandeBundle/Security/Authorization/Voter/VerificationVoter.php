<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 29/05/2017
 * Time: 16:25
 */

namespace DemandeBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class VerificationVoter extends AclVoter{

    public function supportsClass($class)
    {
        return $class == 'DemandeBundle\Entity\Verification';
    }

    public function supportsAttribute($attribute)
    {
        return in_array($attribute,["EDIT","DELETE","VERIFY"]);
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }

        foreach ($attributes as $attribute) {
            /** @var \DemandeBundle\Entity\Verification  $object */
            if ($this->supportsAttribute($attribute) && $object->isLocked()) {
                return self::ACCESS_DENIED;
            }
        }

        return self::ACCESS_ABSTAIN;
    }

} 