<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 04/07/2017
 * Time: 12:32
 */

namespace DemandeBundle\Service;


use AppBundle\Model\Configurator;
use InvoiceBundle\Entity\Invoice;
use InvoiceBundle\Entity\InvoiceItem;
use InvoiceBundle\Model\InvoiceCreator;
use InvoiceBundle\Model\InvoiceTemplate;

class GrumeInvoiceCreator implements InvoiceCreator{

    /**
     * @var Configurator
     */
    private $config;

    function __construct(Configurator $config)
    {
        $this->config = $config;
    }

    /**
     * @param InvoiceTemplate $template
     * @return Invoice
     */
    public function create(InvoiceTemplate $template)
    {
        /** @var \DemandeBundle\Entity\SyntheseGrume $g */
        $g = $template;
        $invoice = new Invoice();

        $invoice->setTransmitter($this->config->get('master'));
        $invoice->setTarget($g->getTarget());
        $invoice->setCurrency($this->config->get('currency'));
        $invoice->setNote(sprintf("Référence de l'enregistrement: %s",$g->getReference()));


        $norm = new InvoiceItem("Etiquettes pré-système");
        $norm->setPu($this->config->get('presystem_amount'));
        $qte = ($g->getTotalBois() > 0)? $g->getTotalBois(): 0;
        $norm->setQte($qte);
        $norm->setSku('étiquettes');

        $dep = new InvoiceItem("Etiquettes pré-système (dépassement)");
        $dep->setPu($this->config->get('presystem_penal_amount'));
        $dep->setQte($g->getGrumeDepasse());
        $dep->setSku('étiquettes');

        $invoice->addItem($norm);
        $invoice->addItem($dep);

        return $invoice;
    }

} 