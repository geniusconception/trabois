<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 21/06/2017
 * Time: 01:22
 */

namespace DemandeBundle\Service;


use AppBundle\Model\Configurator;
use InvoiceBundle\Entity\Invoice;
use InvoiceBundle\Entity\InvoiceItem;
use InvoiceBundle\Model\InvoiceCreator;
use InvoiceBundle\Model\InvoiceTemplate;

class VerificationInvoiceCreator implements InvoiceCreator{
    /**
     * @var Configurator
     */
    private $config;

    function __construct(Configurator $config)
    {
        $this->config = $config;
    }

    /**
     * @param InvoiceTemplate $template
     * @return Invoice
     */
    public function create(InvoiceTemplate $template)
    {
        /** @var \DemandeBundle\Entity\Verification $v */
        $v = $template;
        $invoice = new Invoice();

        $invoice->setTransmitter($this->config->get('master'));
        $invoice->setTarget($v->getTarget());
        $invoice->setCurrency($this->config->get('currency'));

        $c = $v->getListe();
        $invoice->setNote((string)$c);

        $item = new InvoiceItem("Etiquettes pré-système");
        $item->setPu($this->config->get('presystem_amount'));
        $invoice->addItem($item);

        return $invoice;
    }


} 