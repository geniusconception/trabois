<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 03/08/2017
 * Time: 17:14
 */

namespace DemandeBundle\Spread;

use DemandeBundle\Controller\RecoveryController;
use Doctrine\ORM\EntityManagerInterface;
use Spy\Timeline\Model\ActionInterface;
use Spy\Timeline\Spread\Entry\EntryCollection;
use Spy\Timeline\Spread\SpreadInterface;
use Spy\Timeline\Spread\Entry\EntryUnaware;

class RecoverySpread implements SpreadInterface{
    /**
     * @var EntityManagerInterface
     */
    protected  $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    /**
     * You spread class is support the action ?
     *
     * @param ActionInterface $action
     *
     * @return boolean
     */
    public function supports(ActionInterface $action)
    {
        return in_array($action->getVerb(),[RecoveryController::VERB_SUBMIT,RecoveryController::VERB_VERIFY]);
    }

    /**
     * @param  ActionInterface $action action we look for spreads
     * @param  EntryCollection $coll Spreads defined on an EntryCollection
     * @return void
     */
    public function process(ActionInterface $action, EntryCollection $coll)
    {
        $role = ($action->getVerb() == RecoveryController::VERB_SUBMIT)? "ROLE_CONTROLLER":"ROLE_FINANCIER";
        $users = $this->getUsers($role);
        /** @var \FOS\UserBundle\Model\User $user */
        foreach ($users as $user) {
            $coll->add(new EntryUnaware(get_class($user), $user->getId()));
        }
    }

    private function getUsers($role){
        $qb = $this->em->createQueryBuilder();
        $qb->select('u')
            ->from("ApplicationSonataUserBundle:User", 'u')
            ->leftJoin('u.groups', 'g')
            ->where($qb->expr()->orX(
                $qb->expr()->like('u.roles', ':roles'),
                $qb->expr()->like('g.roles', ':roles')
            ))
            ->setParameter('roles', '%'.$role.'%');

        return $qb->getQuery()->getResult();
    }

} 