<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 25/06/2017
 * Time: 22:26
 */

namespace DemandeBundle\Twig;


use DemandeBundle\Entity\Colisage;
use DemandeBundle\Entity\ColisageBotte;
use DemandeBundle\Entity\ColisageGrume;
use DemandeBundle\Entity\ColisagePaquet;

class DemandeExtension extends \Twig_Extension{

    /**
     * @var \Twig_Environment
     */
    private $twig;

    function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }


    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('render_colisage',array($this,'renderColisage'),array('is_safe' => array('html'))),
        );
    }

    public function renderColisage(Colisage $list){
        $args = array("colisage" => $list);
        if($list instanceof ColisageBotte)
            return $this->twig->render("DemandeBundle:Colisage:botte.html.twig",$args);
        if($list instanceof ColisageGrume)
            return $this->twig->render("DemandeBundle:Colisage:grume.html.twig",$args);
        if($list instanceof ColisagePaquet)
            return $this->twig->render("DemandeBundle:Colisage:paquet.html.twig",$args);
        return null;
    }
} 