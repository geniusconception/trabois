<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 13/07/2017
 * Time: 19:36
 */

namespace DossierBundle\Activity;

use AppBundle\Manager\AffectionManager;
use DossierBundle\Entity\Dossier;
use DossierBundle\Event\DossierEvent;
use DossierBundle\Event\DossierEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Spy\Timeline\Driver\ActionManagerInterface;
use Spy\Timeline\Model\ComponentInterface;

class DossierActivity implements EventSubscriberInterface{

    const VERB_SUBMIT="dossier.submit";
    const VERB_RETURN="dossier.return";
    const VERB_VALIDATE="dossier.validate";

    const COMPLEMENT_DOSSIER = "dossier";
    const COMPLEMENT_STEP = "step";
    const COMPLEMENT_EXP = "exp";
    /**
     * @var AffectionManager
     */
    private $affm;
    /**
     * @var ActionManagerInterface
     */
    protected $actionManager;

    function __construct(AffectionManager $affm,ActionManagerInterface $am)
    {
        $this->affm = $affm;
        $this->actionManager = $am;
    }


    public static function getSubscribedEvents()
    {
        return array(
            DossierEvents::DOSSIER_SUBMITTED => 'onDossierSubmitted',
            DossierEvents::DOSSIER_RETURNED => 'onDossierReturned',
        );
    }

    public function onDossierSubmitted(DossierEvent $event){
        $d = $event->getDossier();
        if($d->isLocked()){
            $this->action(self::VERB_VALIDATE,$d);
        }else{
            $this->action(self::VERB_SUBMIT,$d);
        }

    }

    public function onDossierReturned(DossierEvent $event){
        $this->action(self::VERB_RETURN,$event->getDossier());
    }

    /**
     * @return ComponentInterface
     */
    protected function getSubject()
    {
        return $this->actionManager->findOrCreateComponent($this->affm->getAffectation()->getUser());
    }

    protected function action($verb,Dossier $d){
        $target = $this->actionManager->findOrCreateComponent($d);
        $action = $this->actionManager->create($this->getSubject(),$verb,[
            self::COMPLEMENT_DOSSIER => $target,
            self::COMPLEMENT_STEP => $d->getStep(),
            self::COMPLEMENT_EXP => $d->getExploitant()->getId()
        ]);

        $this->actionManager->updateAction($action);
    }
}