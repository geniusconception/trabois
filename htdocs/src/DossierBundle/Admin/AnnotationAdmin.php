<?php

namespace DossierBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class AnnotationAdmin extends AbstractAdmin
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('contenu','textarea',array('label' => "Contenu message"))
            ->add('user','text',array('label' => "Ecrit par"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('contenu','textarea',array('label' => "Contenu (255 caractères)"))
            ->add('fichier','file',array('label' => "pièce-jointe",'required' => false))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('contenu',null,array('label' => "Contenu message"))
            ->add('user','text',array('label' => "par"))
            ->add('fichier',null,array('label' => 'pièce-jointe'))
            ->add('date',null,array('label' => "date"))
        ;
    }

 
    /**
     * @param mixed $tokenStorage
    */
    protected function configureRoutes(RouteCollection $collection){
        if($this->isChild())
            $collection->remove("list");
        else
            $collection->clear();
    }

    public function getParentAssociationMapping()
    {
        return "dossier";
    }

    /**
     * @param mixed $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist($object)
    {
        /** @var \DossierBundle\Entity\Annotation $object */
        $object->setUser($this->tokenStorage->getToken()->getUser());
    }
}
