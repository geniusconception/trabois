<?php

namespace DossierBundle\Admin;

use CorporateBundle\Model\ExploitantFiltred;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Admin\FieldDescriptionCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AppBundle\Manager\AffectionManager;
use Sonata\AdminBundle\Route\RouteCollection;
use TrajetBundle\Model\CheckpointManager;
use AppBundle\Admin\ReversedOrderAdmin;
use Symfony\Component\Form\FormBuilderInterface;
use DossierBundle\Extension\AbstractDossierExtension;
use DossierBundle\Entity\Dossier;

class DossierAdmin extends ReversedOrderAdmin implements ExploitantFiltred
{
    
     /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
   
    /**
     * @var \AppBundle\Model\Compteur
     */
    private $compteur;

    /**
     * @var array
     */
    protected $types = [];
     
    /**
     * @var AffectionManager
     */
    private $affm;
    /**
     * @var CheckpointManager
     */
    private $cm;
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('reference',null,array('label' => "Référence document"))
            ->add('type',null,array(
                'label' => "Type document"
                ),
                'choice',array(
                    'choices' => $this->types,
                )
            )
            ->add('step',null,array(
                    'label' => "Etape"
                ),
                'choice',array(
                    'choices' => self::getSteps(),
                )
            )
            ->add('exploitant',null,array('label' => "Exploitant"))
            ->add('lock',null,array('label' =>"Dossiers classés"))
         ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('reference','text',array(
                'label' => "Référence",
                'route' => ['name' => 'show']
            ))
            ->add('title',null,array('label' => "Nom"))
            ->add('exploitant',null,array('label' => "Exploitant"))
            ->add('date',null,array('label' => "créé le"))
            ->add('lock','boolean',array('label' => 'classé ?'))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'follow' => array(
                        'template' => 'DossierBundle:CRUD:list__action_follow.html.twig'
                    ),
                    'submit' => array(
                        'template' => 'DossierBundle:CRUD:list__action_submit.html.twig'
                    ),
                    'return' => array(
                        'template' => 'DossierBundle:CRUD:list__action_return.html.twig'
                    )
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $q = $this->modelManager
            ->createQuery("UploadBundle:Files","f")
            ->where("f.exploitant = :exp AND f.authenticated = :auth")
            ->setParameter("exp",$this->getExploitant())
            ->setParameter("auth",true)
        ;
        /** @var \DossierBundle\Entity\Dossier $subject */
        $subject = $this->getSubject();
        $formMapper
            ->add('title','text',array('label' => "Nom du dossier"))
            ;
        if(!$subject->getType())
            $formMapper->add('type','choice',array('label' => "Type dossier",'choices' => $this->types));
        else
            $formMapper->add('type','hidden');
        $formMapper
            ->add('annexes','sonata_type_model',array(
                'label' => "Annexes",
                'multiple' => true,
                'query' => $q
            ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('reference',null,array('label' => "Référence"))
            ->add('title',null,array('label' => "Nom"))
            ->add('exploitant',null,array('label' => "Exploitant"))
            ->add('date',null,array('label' => "créé le"))
        ;
    }

    /**
     * @param \AppBundle\Model\Compteur $compteur
     */
    public function setCompteur($compteur)
    {
        $this->compteur = $compteur;
    }


    /**
     * @param mixed $tokenStorage
    */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }
 
    public function prePersist($object)
    {
        $object->setStep("EXPLOITANT");
        $exp = $this->getExploitant();
        /** @var \DossierBundle\Entity\Dossier $object */
        $object->setExploitant($exp);
        /** @var \DateTime $d */
        $d = $object->getDate();
        $prefix = $object->getType();
        $ref = $prefix;
        $ref .= $d->format('m').$d->format('Y');
        $ref .= "-".$this->compteur->nextValue($prefix);
        $object->setReference($ref);
    }

    /**
     * @param AffectionManager $affm
     */
    public function setAffm(AffectionManager $affm)
    {
        $this->affm = $affm;
    }

    /**
     * @param array $types
     */
    public function setTypes($types)
    {
        $this->types = $types;
    }

    public function getTemplate($name)
    {
        foreach ($this->getExtensions() as $extension) {
            if($extension instanceof AbstractDossierExtension){
                /** @var Dossier $dossier */
                $dossier = $this->getSubject();
                if($dossier && $template  = $extension->getTemplate($name,$dossier->getType())){
                    return $template;
                }
            }
        }
        if($name == 'show')
            return "DossierBundle:CRUD:dossier.html.twig";
        return parent::getTemplate($name);
    }

    protected function configureRoutes(RouteCollection $collection){
        $collection->add('submit',$this->getRouterIdParameter().'/submit');
        $collection->add('return',$this->getRouterIdParameter().'/return');
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        if(isset($list['create']))
            unset($list['create']);

        if(in_array($action,['show'])){
            $list['annotate'] = [
                'template' => 'DossierBundle:CRUD:action_annotate.html.twig'
            ];
            $list['submit'] = [
                'template' => 'DossierBundle:CRUD:action_submit.html.twig'
            ];
            $list['return'] = [
                'template' => 'DossierBundle:CRUD:action_return.html.twig'
            ];
        }

        return $list;
    }

    /**
     * @param CheckpointManager $cm
     */
    public function setCheckpointManager(CheckpointManager $cm)
    {
        $this->cm = $cm;
    }

    public function postPersist($object)
    {
        /** @var \DossierBundle\Entity\Dossier $object */
        $this->cm->addStep($object,$object->getReference(),$object->getType());
    }

    public static  function getSteps(){
        return array(
            "EXPLOITANT" => "Exploitant",
            "DIAF" => "Analyse - DIAF",
            "DGF" => "Analyse - DGF",
            "SG" => "Sécretariat Général",
            "MINISTRY" => "validation - Ministère",
        );
    }

    /**
     * @return string
     */
    public function getExploitantField()
    {
        return "exploitant";
    }

    /**
     * @return \CorporateBundle\Entity\Entreprise|null
     */
    public function getExploitant(){
        return ($this->affm->isExploitant())? $this->affm->getAffectation()->getExploitant(): null;
    }

    public function defineFormBuilder(FormBuilderInterface $formBuilder)
    {
        $mapper = new FormMapper($this->getFormContractor(), $formBuilder, $this);

        $this->configureFormFields($mapper);

        foreach ($this->getExtensions() as $extension) {
            $extension->configureFormFields($mapper);
            if($extension instanceof AbstractDossierExtension){
                /** @var Dossier $dossier */
                $dossier = $this->getSubject();
                $extension->configureDossierDataFormFields($mapper,$dossier->getType(),$this);
            }
        }

        $this->attachInlineValidator();
    }

}
