<?php

namespace DossierBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Sonata\AdminBundle\Exception\ModelManagerException;

class DossierController extends CRUDController
{
    public function createAction()
    {
        $request = $this->getRequest();
        // the key used to lookup the template
        $templateKey = 'edit';

        $this->admin->checkAccess('create');

        $class = new \ReflectionClass($this->admin->hasActiveSubClass() ? $this->admin->getActiveSubClass() : $this->admin->getClass());

        if ($class->isAbstract()) {
            return $this->render(
                'SonataAdminBundle:CRUD:select_subclass.html.twig',
                array(
                    'base_template' => $this->getBaseTemplate(),
                    'admin' => $this->admin,
                    'action' => 'create',
                ),
                null,
                $request
            );
        }
        /** @var \DossierBundle\Entity\Dossier $object */
        $object = $this->admin->getNewInstance();
        $type = $this->getDossierType($request);
        $dlist = $this->getParameter("dossier_list");
        if(isset($dlist[$type])){
            $object->setType($type);
            $object->setTitle($dlist[$type]);
        }

        $preResponse = $this->preCreate($request, $object);
        if ($preResponse !== null) {
            return $preResponse;
        }

        $this->admin->setSubject($object);

        /** @var $form \Symfony\Component\Form\Form */
        $form = $this->admin->getForm();
        $form->setData($object);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            //TODO: remove this check for 4.0
            if (method_exists($this->admin, 'preValidate')) {
                $this->admin->preValidate($object);
            }
            $isFormValid = $form->isValid();

            // persist if the form was valid and if in preview mode the preview was approved
            if ($isFormValid && (!$this->isInPreviewMode($request) || $this->isPreviewApproved($request))) {
                $this->admin->checkAccess('create', $object);

                try {
                    $object = $this->admin->create($object);

                    if ($this->isXmlHttpRequest()) {
                        return $this->renderJson(array(
                            'result' => 'ok',
                            'objectId' => $this->admin->getNormalizedIdentifier($object),
                        ), 200, array());
                    }

                    $this->addFlash(
                        'sonata_flash_success',
                        $this->admin->trans(
                            'flash_create_success',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );

                    // redirect to edit mode
                    return $this->redirectTo($object);
                } catch (ModelManagerException $e) {
                    $this->handleModelManagerException($e);

                    $isFormValid = false;
                }
            }

            // show an error message if the form failed validation
            if (!$isFormValid) {
                if (!$this->isXmlHttpRequest()) {
                    $this->addFlash(
                        'sonata_flash_error',
                        $this->admin->trans(
                            'flash_create_error',
                            array('%name%' => $this->escapeHtml($this->admin->toString($object))),
                            'SonataAdminBundle'
                        )
                    );
                }
            } elseif ($this->isPreviewRequested()) {
                // pick the preview template if the form was valid and preview was requested
                $templateKey = 'preview';
                $this->admin->getShow();
            }
        }

        $view = $form->createView();

        // set the theme for the current Admin Form
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render($this->admin->getTemplate($templateKey), array(
            'action' => 'create',
            'form' => $view,
            'object' => $object,
        ), null);
    }

    public function submitAction(Request $request, $id){
        /** @var \DossierBundle\Entity\Dossier $d */
        $id = $request->get($this->admin->getIdParameter());
        $d = $this->admin->getObject($id);

        if (!$d) {
            throw new NotFoundHttpException(sprintf('unable to find the dossier with id : %s', $id));
        }

        if($d->isLocked()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autorisé à soumettre cet enregistrement");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        if(!$this->admin->isGranted('SUBMIT',$d))
            throw $this->createAccessDeniedException("vous n'êtes pas authoriser à effectuer cette opération");

        $url = $this->admin->generateObjectUrl('submit',$d);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** @var \DossierBundle\Manager\DossierManager $manager */
            $manager = $this->get("dossier.manager.dossier");

            if($d = $manager->submit($d))
                $this->addFlash('sonata_flash_success', 'Le dossier a été soumis avec succès');
            else
                $this->addFlash('flash_create_error', "Le dossier n'a pas pu être soumis");

            return $this->redirect($this->admin->generateUrl('show',[$this->admin->getIdParameter() => $id]));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $d,
            "message" => sprintf('Êtes-vous sûr de vouloir soumettre le dossier "%s" ?',$d->__toString()),
        ));
    }

    public function returnAction(Request $request, $id){
        /** @var \DossierBundle\Entity\Dossier $d */
        $id = $request->get($this->admin->getIdParameter());
        $d = $this->admin->getObject($id);

        if (!$d) {
            throw new NotFoundHttpException(sprintf('unable to find the dossier with id : %s', $id));
        }

        if($d->isLocked()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autorisé à retourner cet enregistrement");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        if(!$this->admin->isGranted('RETURN',$d))
            throw $this->createAccessDeniedException("vous n'êtes pas authoriser à effectuer cette opération");

        $url = $this->admin->generateObjectUrl('return',$d);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** @var \DossierBundle\Manager\DossierManager $manager */
            $manager = $this->get("dossier.manager.dossier");

            if($d = $manager->doReturn($d))
                $this->addFlash('sonata_flash_success', 'Le dossier a été retourné avec succès');
            else
                $this->addFlash('flash_create_error', "Le dossier n'a pas pu être retourné");

            return $this->redirect($this->admin->generateUrl('show',[$this->admin->getIdParameter() => $id]));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $d,
            "message" => sprintf('Êtes-vous sûr de vouloir retourné le dossier "%s" ?',$d->__toString()),
        ));
    }

    private function getDossierType(Request $request){
        if($request->isMethod(Request::METHOD_POST)){
            $uid = $request->query->get("uniqid");
            $uid = $request->request->get($uid);
            return $uid['type'];
        }
        else
            return $request->query->get("type");
    }
}
