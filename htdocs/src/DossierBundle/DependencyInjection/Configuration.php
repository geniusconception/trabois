<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 12/07/2017
 * Time: 15:18
 */

namespace DossierBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface{

    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('dossier');

        $rootNode
            ->children()
                ->arrayNode('list')
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')
                        ->isRequired()->cannotBeEmpty()
                    ->end()
                ->end()
                ->arrayNode('groups')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('icon')->defaultValue("suitcase")->cannotBeEmpty()->end()
                            ->scalarNode('title')->isRequired()->cannotBeEmpty()->end()
                            ->arrayNode('dossiers')->prototype('scalar')->end()->end()
                        ->end()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
} 