<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 12/07/2017
 * Time: 15:26
 */

namespace DossierBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;

class DossierExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container){
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter("dossier_list",$config['list']);
        $container->setParameter("dossier_grouped_list",$config['groups']);
    }

} 