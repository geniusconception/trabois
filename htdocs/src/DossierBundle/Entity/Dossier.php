<?php

namespace DossierBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TrajetBundle\Model\Snapshot;

/**
 * Dossier
 *
 * @ORM\Table(name="dossier",indexes={@ORM\Index(name="type_idx", columns={"type"})})
 * @ORM\Entity(repositoryClass="DossierBundle\Repository\DossierRepository")
 */
class Dossier implements Snapshot
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255,unique=true)
     */
    private $reference;
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=9)
     */
    private $type;

    /**
    * @ORM\ManyToMany(targetEntity="UploadBundle\Entity\Files")
    */
    private $annexes;
 
    /**
     *
     * @ORM\OneToMany(targetEntity="DossierBundle\Entity\Annotation",mappedBy="dossier",cascade={"all"},orphanRemoval=true)
     * @ORM\JoinColumn(nullable=true)
     */
    private $annotations;

    /**
     * @var \CorporateBundle\Entity\Entreprise
     * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
     * @ORM\JoinColumn(nullable=false)
     */
    private $exploitant;

    /**
     * @var string
     *
     * @ORM\Column(name="step", type="string", length=15)
     */
    private $step;
    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    private $lock;
    /**
     * @var array
     * @ORM\Column(name="data",type="array",nullable=true)
     */
    private $data;
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Dossier
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Dossier
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Dossier
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set annexes
     *
     * @param string $annexes
     * @return Dossier
     */
    public function setAnnexes($annexes)
    {
        $this->annexes = $annexes;

        return $this;
    }

    /**
     * Get annexes
     *
     * @return string 
     */
    public function getAnnexes()
    {
        return $this->annexes;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->annexes = new \Doctrine\Common\Collections\ArrayCollection();
        $this->annotations = new \Doctrine\Common\Collections\ArrayCollection();
        $this->date = new \Datetime();
        $this->type = null;
        $this->lock = false;
        $this->data = array();
    }

    /**
     * Add annexes
     *
     * @param \UploadBundle\Entity\Files $annexes
     * @return Dossier
     */
    public function addAnnex(\UploadBundle\Entity\Files $annexes)
    {
        $this->annexes[] = $annexes;

        return $this;
    }

    /**
     * Remove annexes
     *
     * @param \UploadBundle\Entity\Files $annexes
     */
    public function removeAnnex(\UploadBundle\Entity\Files $annexes)
    {
        $this->annexes->removeElement($annexes);
    }

    /**
     * Add annotations
     *
     * @param \DossierBundle\Entity\Annotation $annotations
     * @return Dossier
     */
    public function addAnnotation(\DossierBundle\Entity\Annotation $annotations)
    {
        $this->annotations[] = $annotations;
        $annotations->setDossier($this);
        return $this;
    }

    /**
     * Remove annotations
     *
     * @param \DossierBundle\Entity\Annotation $annotations
     */
    public function removeAnnotation(\DossierBundle\Entity\Annotation $annotations)
    {
        $this->annotations->removeElement($annotations);
        $annotations->setDossier(null);
    }

    /**
     * Get annotations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAnnotations()
    {
        return $this->annotations;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Dossier
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set exploitant
     *
     * @param \CorporateBundle\Entity\Entreprise $exploitant
     * @return Dossier
     */
    public function setExploitant(\CorporateBundle\Entity\Entreprise $exploitant)
    {
        $this->exploitant = $exploitant;

        return $this;
    }

    /**
     * Get exploitant
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getExploitant()
    {
        return $this->exploitant;
    }

    function __toString()
    {
        return ($this->getId())? sprintf("Dossier %s",$this->reference): "n-a";
    }

    /**
     * @return string
     */
    public function getStep()
    {
        return $this->step;
    }

    /**
     * @param string $step
     */
    public function setStep($step)
    {
        $this->step = $step;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return "dossier";
    }

    /**
     * @return boolean
     */
    public function isLocked()
    {
        return $this->lock;
    }

    /**
     * @param boolean $lock
     */
    public function setLocked($lock)
    {
        $this->lock = $lock;
    }

    /**
     * @return array
     */
    public function getIdentifier()
    {
        return array("id" => $this->getId());
    }

    /**
     * @return array
     */
    public function configureCaptureFields()
    {
        return array("step");
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }
}
