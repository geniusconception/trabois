<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 10/07/2017
 * Time: 15:28
 */

namespace DossierBundle\Event;

use DossierBundle\Entity\Dossier;
use Symfony\Component\EventDispatcher\Event;

class DossierEvent extends Event{
    /**
     * @var Dossier
     */
    protected $dossier;

    function __construct(Dossier $dossier)
    {
        $this->dossier = $dossier;
    }

    /**
     * @return Dossier
     */
    public function getDossier()
    {
        return $this->dossier;
    }
} 