<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 10/07/2017
 * Time: 15:27
 */

namespace DossierBundle\Event;


class DossierEvents {

    const DOSSIER_SUBMITTED = "dossier.event.submitted";

    const DOSSIER_RETURNED = "dossier.event.returned";
}