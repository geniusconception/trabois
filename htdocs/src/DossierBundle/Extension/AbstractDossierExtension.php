<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 30/07/2017
 * Time: 17:40
 */

namespace DossierBundle\Extension;

use DossierBundle\Admin\DossierAdmin;
use Sonata\AdminBundle\Admin\AbstractAdminExtension;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

abstract class AbstractDossierExtension extends AbstractAdminExtension{

    /**
     * @param FormMapper $mapper
     * @param $dossierType
     * @param DossierAdmin $admin
     */
    public function configureDossierDataFormFields(FormMapper $mapper,$dossierType,DossierAdmin $admin){}

    /**
     * @param string $name
     * @param string|null $type
     * @return string|null
     */
    public function getTemplate($name,$type = null){
        return null;
    }
}