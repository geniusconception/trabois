<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 10/07/2017
 * Time: 14:49
 */

namespace DossierBundle\Manager;
use Doctrine\ORM\EntityManagerInterface;
use DossierBundle\Entity\Dossier;
use DossierBundle\Event\DossierEvent;
use DossierBundle\Event\DossierEvents;
use TrajetBundle\Model\CheckpointManager;

class DossierManager {

    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var CheckpointManager
     */
    private $cm;

    private $dispatcher;

    public function __construct (EntityManagerInterface $em, CheckpointManager $cm, $disp)
    {
        $this->em = $em;
        $this->cm = $cm;
        $this->dispatcher = $disp;
    }

    /**
     * @param Dossier $d
     * @return bool|Dossier
     */
    public function submit(Dossier $d){
        if($d->isLocked())
            return false;

        $circuit = $d->getType();
        $step = $d->getStep();

        if($this->cm->circuitExist($circuit)){
            $step = $this->cm->guessNextStep($circuit,$step);
            if($step){
                $d->setStep($step);
                $this->em->persist($d);
                $this->em->flush();
                /** @var Dossier $d */
                $d = $this->cm->addStep($d,$d->getReference(),$circuit);
            }else{
                /** @var Dossier $d */
                $d = $this->cm->lock($d);
            }

            $this->dispatcher->dispatch(DossierEvents::DOSSIER_SUBMITTED,new DossierEvent($d));
            return $d;
        }
        return false;
    }

    /**
     * @param Dossier $d
     * @return bool|Dossier
     */
    public function doReturn(Dossier $d){
        if($d->isLocked())
            return false;

        $circuit = $d->getType();
        $step = $d->getStep();

        if($this->cm->circuitExist($circuit)){
            $step = $this->cm->guessNextStep($circuit,$step,true);
            if($step){
                $d->setStep($step);
                $this->em->persist($d);
                $this->em->flush();
                $this->dispatcher->dispatch(DossierEvents::DOSSIER_RETURNED,new DossierEvent($d));

                return $d;
            }
        }
        return false;
    }
}