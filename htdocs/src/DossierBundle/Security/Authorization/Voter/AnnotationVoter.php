<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 10/07/2017
 * Time: 01:27
 */

namespace DossierBundle\Security\Authorization\Voter;

use AppBundle\Manager\AffectionManager;
use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Permission\PermissionMapInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class AnnotationVoter extends AclVoter{

    /**
     * @var AffectionManager
     */
    private $affec;

    public function __construct(AffectionManager $affec,AclProviderInterface $aclProvider, ObjectIdentityRetrievalStrategyInterface $oidRetrievalStrategy, SecurityIdentityRetrievalStrategyInterface $sidRetrievalStrategy, PermissionMapInterface $permissionMap, LoggerInterface $logger = null, $allowIfObjectIdentityUnavailable = true)
    {
        parent::__construct($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap, $logger, $allowIfObjectIdentityUnavailable);
        $this->affec = $affec;
    }

    public function supportsClass($class)
    {
        return $class == 'DossierBundle\Entity\Annotation';
    }

    public function supportsAttribute($attribute){
        return in_array($attribute,["CREATE"]);
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }

        foreach ($attributes as $attribute) {
            if ($this->supportsAttribute($attribute)){
                if($a = $this->affec->getAffectation()){
                    /** @var \DossierBundle\Entity\Annotation  $object */
                    return ($a->getRole() == $object->getDossier()->getStep())? self::ACCESS_GRANTED:self::ACCESS_DENIED;
                }else{
                    return self::ACCESS_DENIED;
                }
            }
        }

        return self::ACCESS_ABSTAIN;
    }
} 