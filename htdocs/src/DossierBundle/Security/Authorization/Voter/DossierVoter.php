<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 09/07/2017
 * Time: 23:31
 */

namespace DossierBundle\Security\Authorization\Voter;

use AppBundle\Manager\AffectionManager;
use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Permission\PermissionMapInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class DossierVoter extends AclVoter{

    /**
     * @var AffectionManager
     */
    private $affec;

    public function __construct(AffectionManager $affec,AclProviderInterface $aclProvider, ObjectIdentityRetrievalStrategyInterface $oidRetrievalStrategy, SecurityIdentityRetrievalStrategyInterface $sidRetrievalStrategy, PermissionMapInterface $permissionMap, LoggerInterface $logger = null, $allowIfObjectIdentityUnavailable = true)
    {
        parent::__construct($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap, $logger, $allowIfObjectIdentityUnavailable);
        $this->affec = $affec;
    }

    public function supportsClass($class)
    {
        return $class == 'DossierBundle\Entity\Dossier' || $class == 'DossierBundle\Admin\DossierAdmin';
    }

    public function supportsAttribute($attribute){
        return in_array($attribute,["EDIT","SUBMIT","ANNOTATE","NEW","CREATE","RETURN","DELETE"]);
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }

        $isExploitant = $this->affec->isExploitant();

        foreach ($attributes as $attribute) {
            $Admin = 'DossierBundle\Admin\DossierAdmin';
            if ($this->supportsAttribute($attribute)){
                if($object instanceof $Admin){
                    if($attribute == "CREATE")
                        return ($isExploitant)? self::ACCESS_GRANTED: self::ACCESS_DENIED;
                }else{
                    /** @var \DossierBundle\Entity\Dossier  $object */
                    if($object->isLocked())
                        return self::ACCESS_DENIED;
                    if(in_array($attribute,["EDIT","SUBMIT","RETURN","ANNOTATE"])){
                        if($a = $this->affec->getAffectation()){
                            if($attribute == "EDIT" && !$isExploitant)
                                return self::ACCESS_DENIED;
                            if($attribute == "RETURN" && $isExploitant)
                                return self::ACCESS_DENIED;
                            /** @var \DossierBundle\Entity\Dossier  $object */
                            if($a->getRole() == $object->getStep()){
                                if($isExploitant)
                                    return ($object->getExploitant()->getId() == $a->getExploitant()->getId())? self::ACCESS_GRANTED:self::ACCESS_DENIED;
                                else
                                    return self::ACCESS_GRANTED;
                            }else{
                                return self::ACCESS_DENIED;
                            }
                        }else{
                            return self::ACCESS_DENIED;
                        }
                    }
                    elseif($attribute == "DELETE"){
                        return self::ACCESS_DENIED;
                    }
                }
            }
        }

        return self::ACCESS_ABSTAIN;
    }


} 