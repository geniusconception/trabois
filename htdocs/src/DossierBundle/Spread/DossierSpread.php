<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 13/07/2017
 * Time: 21:08
 */

namespace DossierBundle\Spread;

use Doctrine\ORM\EntityManagerInterface;
use DossierBundle\Activity\DossierActivity;
use Spy\Timeline\Model\ActionInterface;
use Spy\Timeline\Spread\Entry\EntryCollection;
use Spy\Timeline\Spread\SpreadInterface;
use Spy\Timeline\Spread\Entry\EntryUnaware;

abstract class DossierSpread implements SpreadInterface{

    /**
     * @var EntityManagerInterface
     */
    protected  $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * You spread class is support the action ?
     *
     * @param ActionInterface $action
     *
     * @return boolean
     */
    public function supports(ActionInterface $action)
    {
        return in_array($action->getVerb(),[DossierActivity::VERB_SUBMIT,DossierActivity::VERB_RETURN,DossierActivity::VERB_VALIDATE]);
    }

    /**
     * @param  ActionInterface $action action we look for spreads
     * @param  EntryCollection $coll Spreads defined on an EntryCollection
     * @return void
     */
    public function process(ActionInterface $action, EntryCollection $coll)
    {
        $users = $this->getUsers($action);
        /** @var \FOS\UserBundle\Model\User $user */
        foreach ($users as $user) {
            $coll->add(new EntryUnaware(get_class($user), $user->getId()), 'dossier');
        }
    }

    /**
     * @param ActionInterface $action
     * @return mixed
     */
    abstract protected function getUsers(ActionInterface $action);

} 