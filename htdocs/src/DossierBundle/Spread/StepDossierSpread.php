<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 13/07/2017
 * Time: 21:37
 */

namespace DossierBundle\Spread;

use Doctrine\ORM\EntityManagerInterface;
use DossierBundle\Activity\DossierActivity;
use Spy\Timeline\Model\ActionInterface;
use AppBundle\Manager\AffectionManager;

class StepDossierSpread extends DossierSpread {

    /**
     * @var AffectionManager
     */
    private $affm;

    function __construct(EntityManagerInterface $em,AffectionManager $am)
    {
        parent::__construct($em);
        $this->affm = $am;
    }


    /**
     * @param ActionInterface $action
     * @return mixed
     */
    protected function getUsers(ActionInterface $action)
    {
        $role = $this->affm->getAffectation()->getRole();
        $step = $action->getComponent(DossierActivity::COMPLEMENT_STEP);
        $q = $this->em
            ->createQueryBuilder()
            ->select('a')
            ->from("AppBundle:Affectation","a")
            ->leftJoin("a.user",'u')
            ->addSelect('u')
            ->where("a.role = :role OR a.role = :step")
            ->setParameter("role",$role)
            ->setParameter("step",$step)
            ->getQuery()
        ;
        return array_map(function($a){
            /** @var \AppBundle\Entity\Affectation $a */
            return $a->getUser();
        },$q->getResult());
    }


} 