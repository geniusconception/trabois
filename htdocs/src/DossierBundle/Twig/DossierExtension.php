<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 13/07/2017
 * Time: 00:38
 */

namespace DossierBundle\Twig;


class DossierExtension extends \Twig_Extension{

    /**
     * @var \Twig_Environment
     */
    private $twig;
    /**
     * @var array
     */
    private $list;
    /**
     * @var array
     */
    private $groups;

    function __construct(\Twig_Environment $twig,$groups, $list)
    {
        $this->groups = $groups;
        $this->list = $list;
        $this->twig = $twig;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction("dossier_list",array($this,"renderList"), array('is_safe' => array('html'))),
        );
    }

    public function renderList(){
        return $this->twig->render("DossierBundle:Dossier:menu_list.html.twig",array(
            "dossier_grouped_list" => $this->groups,
            "dossier_list" => $this->list,
        ));
    }
} 