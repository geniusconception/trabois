<?php

namespace InventoryBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use InventoryBundle\Entity\Arbre;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Model\AvancedImportsInterface;

class ArbreAdmin extends AbstractAdmin implements AvancedImportsInterface
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('numero',null,array('label' => "Numéro prospection"))
            ->add('etat',null,array('label' => "Qualité"),
                'choice',array(
                    'choices' => Arbre::getTypes()
                ))
            ->add('bloc',null,array('label' => "Bloc"))
            ->add('parcelle',null,array('label' => "Parcelle"))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('numero','text',array('label' => "Numéro prospection"))
            ->add('diametre','number',array('label' => "Diamètre"))
            ->add('etat','choice',array(
                'label' => "Qualité",
                'choices' => Arbre::getTypes()
            ))
            ->add('longitude','numero',array('label' => "Longitude"))
            ->add('lattitude','numero',array('label' => "Lattitude"))
            ->add('bloc','text',array('label' => "Bloc"))
            ->add('parcelle','text',array('label' => "Parcelle"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('numero','text',array('label' => "N° prospection"))
            ->add('diametre','number',array('label' => "Diamètre"))
            ->add('essence',null,array('label' => 'Essence'))
            ->add('etat','choice',array('choices' => Arbre::getTypes(),'label' => "Qualité"))
            ->add('longitude','number',array('label' => "Longitude"))
            ->add('lattitude','number',array('label' => "Lattitude"))
            ->add('bloc','text',array('label' => "Bloc","required" => false))
            ->add('parcelle','text',array('label' => "Parcelle","required" => false))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('numero','text',array('label' => "Numéro prospection"))
            ->add('diametre','text',array('label' => "Diamètre"))
            ->add('etat','choice',array('choices' => Arbre::getTypes(),'label' => "Qualité"))
            ->add('longitude','number',array('label' => "Longitude"))
            ->add('lattitude','number',array('label' => "Lattitude"))
            ->add('bloc','text',array('label' => "Bloc"))
            ->add('parcelle','text',array('label' => "Parcelle"))
        ;
    }

    public function getParentAssociationMapping()
    {
        return 'inventory';
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        if(!$this->isChild())
            $collection->remove('list');
    }

    public function preHydrate($object, array $header, array $data)
    {
        $index = array_search('essence',$header);
        $ess = strtolower($data[$index]);
        $e = $this
            ->getModelManager()
            ->createQuery("ExploitationBundle:Essence","e")
            ->where("e.nomPilote = :ess")
            ->setParameter("ess",$ess)
            ->getQuery()
            ->getOneOrNullResult();
        ;
        $data[$index] = $e;
        return $data;
    }

    public function postHydrate($object, array $header)
    {
        // TODO: Implement postHydrate() method.
    }

    public function getHintTemplate()
    {
        return "InventoryBundle:Arbre:importation_hint.html.twig";
    }

    /**
     * @return array
     */
    public function configureImportFields()
    {
        return [
            "numero",
            "essence",
            "diametre",
            "etat",
            "longitude",
            "lattitude",
            "bloc",
            "parcelle",
        ];
    }


}
