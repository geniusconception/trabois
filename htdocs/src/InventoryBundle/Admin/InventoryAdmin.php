<?php

namespace InventoryBundle\Admin;

use CorporateBundle\Model\AdvancedExploitantFiltred;
use AppBundle\Manager\AffectionManager;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use AppBundle\Admin\ReversedOrderAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class InventoryAdmin extends ReversedOrderAdmin implements AdvancedExploitantFiltred{
    
     /**
     * @var \AppBundle\Model\Compteur
     */
    private $compteur;
    /**
     * @var AffectionManager
     */
    private $affm;

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('concession',null,array('label' => "Concession"))
            ->add('reference',null,array('label' => "Référence"))
            ->add('approved',null,array('label' => "Approuvé?"))
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('reference',null,array(
                "route" => array("name" => "show")
            ),array('label' => "Concession"))
            ->add('concession','text',array('label' => "Concession"))
            ->add('date',null,array('label' => "Date"))
            ->add('approved','boolean',array('label' => "Approuvé?"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'tree' => [
                        'template' => "InventoryBundle:Button:list__action_tree.html.twig"
                    ],
                    'map' => [
                        'template' => "InventoryBundle:CRUD:show_map.html.twig"
                    ],
                    'approve' => [
                        'template' => "InventoryBundle:CRUD:list__action_approve.html.twig"
                    ]
                    
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $q = $this->modelManager
            ->createQuery("CorporateBundle:Concession","c")
            ->where("c.entreprise = :exp")
            ->setParameter("exp",$this->getExploitant())
        ;
        $formMapper
            ->add('concession','sonata_type_model',array(
                'label' => 'Concession',
                'query' => $q,
            ))

            ->add('file','file',array('label' => "Map (fichier KML)", 'required'=>false))
            ->add('inventoryFile','file',array('label' => "Inventaire", 'required'=>false))
            ->add('arbre',"sonata_type_collection",array(
                    'type_options' => array(
                        "delete" => true,
                    ),
                    'by_reference' => false,
                    'label' => 'Arbre'
                ),array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                ))
          ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('concession','text',array('label' => "Concession"))
            ->add('name','text',array(
                'label' => "Fichier",
                'template' => "InventoryBundle:Button:download.html.twig"
            ))
            ->add('reference','text',array('label' => "Référence"))
            ->add('date',null,array('label' => "Date"))
            ->add('approved','boolean',array('label' => "Approuvé?"))
            ->add('arbre',null,[
                'label' => "Rélévé de comptage",
                'template' => "InventoryBundle:Button:action_arbre_csv.html.twig",
            ])
        ;
    }

    /**
     * @param \AppBundle\Model\Compteur $compteur
    */
    public function setCompteur($compteur)
    {
        $this->compteur = $compteur;
    }

    public function prePersist($object)
    {
        $d = new \Datetime('now');
        $prefix = "INV";
        $ref = $prefix;
        $ref .= $d->format('m').$d->format('Y');
        $ref .= "-".$this->compteur->nextValue($prefix);
        $object->setReference($ref);
    }

    /**
     * @param string $alias
     * @return array
     */
    public function getJoinedRules($alias = "o")
    {
        return [
            sprintf("%s.concession",$alias) => "c",
        ];
    }

    /**
     * @return string
     */
    public function getExploitantField()
    {
        return "c.entreprise";
    }

    /**
     * @return \CorporateBundle\Entity\Entreprise|null
     */
    private function getExploitant(){
        return ($this->affm->isExploitant())? $this->affm->getAffectation()->getExploitant(): null;
    }

    /**
     * @param AffectionManager $affm
     */
    public function setAffm(AffectionManager $affm)
    {
        $this->affm = $affm;
    }

    protected function configureRoutes(RouteCollection $collection){
        $collection->add('approve',$this->getRouterIdParameter().'/approve');
        $collection->add('map',$this->getRouterIdParameter().'/map');
        $collection->add('arbreCSV',$this->getRouterIdParameter().'/arbre_csv');
    }

    public function configureActionButtons($action, $object = null)
    {
        $list = parent::configureActionButtons($action, $object);

        if(in_array($action,['show'])){
            $list['approve'] = [
                'template' => 'InventoryBundle:Button:action_approve.html.twig'
            ];
            $list['map'] = [
                'template' => 'InventoryBundle:Button:action_map.html.twig'
            ];
        }

        return $list;
    }
}
