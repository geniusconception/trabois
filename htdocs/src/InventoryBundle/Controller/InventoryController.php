<?php

namespace InventoryBundle\Controller;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\Request;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class InventoryController extends CRUDController
{
    public function approveAction(Request $request)
    {
        $id = $request->get($this->admin->getIdParameter());
        /** @var \InventoryBundle\Entity\Inventory $i */
        $i = $this->admin->getObject($id);

        if (!$i) {
            throw new NotFoundHttpException(sprintf('unable to find the inventory with id : %s', $id));
        }

        if($i->isApproved()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autorisé à approuver cet enregistrement");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        if(!$this->admin->isGranted('APPROVE',$i))
            throw $this->createAccessDeniedException("vous n'êtes pas authoriser à effectuer cette opération");

        $url = $this->admin->generateObjectUrl('approve',$i);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** @var \InventoryBundle\Manager\InventoryManager $manager */
            $manager = $this->get("inventory.manager.inventory");

            if($i = $manager->approve($i))
                $this->addFlash('sonata_flash_success', 'L\'inventaire a été approuvé avec succès');
            else
                $this->addFlash('flash_create_error', "L'inventaire n'a pas pu être approuvé");

            return $this->redirect($this->admin->generateUrl('show',[$this->admin->getIdParameter() => $id]));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $i,
            "message" => sprintf('Êtes-vous sûr de vouloir approuver l\' inventaire "%s" ?',$i->__toString()),
        ));
    }

    public function arbreCSVAction(Request $request){
        $id = $request->get($this->admin->getIdParameter());
        /** @var \InventoryBundle\Entity\Inventory $i */
        $i = $this->admin->getObject($id);

        if (!$i) {
            throw new NotFoundHttpException(sprintf('unable to find the inventory with id : %s', $id));
        }

        if(!$this->admin->isGranted('VIEW',$i))
            throw $this->createAccessDeniedException("vous n'êtes pas authoriser à effectuer cette opération");

        /** @var \InventoryBundle\Manager\InventoryManager $manager */
        $manager = $this->get("inventory.manager.inventory");
        $path = $manager->generateArbreFile($i);

        $resp = new BinaryFileResponse($path);
        $resp->headers->set('Content-Type', "text/csv");
        $resp->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            sprintf("%s.csv",$i)
        );
        $resp->deleteFileAfterSend(true);

        return $resp;
    }

    public function mapAction(Request $request){
        $id = $request->get($this->admin->getIdParameter());
        /** @var \InventoryBundle\Entity\Inventory $i */
        $i = $this->admin->getObject($id);

        if (!$i) {
            throw new NotFoundHttpException(sprintf('unable to find the inventory with id : %s', $id));
        }

        if(!$this->admin->isGranted('VIEW',$i))
            throw $this->createAccessDeniedException("vous n'êtes pas authoriser à effectuer cette opération");

        $list_essence = $this->getDoctrine()->getManager()->getRepository('ExploitationBundle:Essence')->findAll();

        /** @var \InventoryBundle\Manager\InventoryManager $manager */
        $manager = $this->get("inventory.manager.inventory");

        $arbre = $manager->coordonneeArbre($i);
       
        $data = $arbre;

        $dataCenter = $data[0];        
        
        $centre = json_encode($dataCenter);       

        return $this->render('InventoryBundle:Map:map.html.twig', array(
            "data"=>$data,
            "id"=>$i,
            "centre"=>$centre,
            "essence"=>$list_essence));  
    }
}
