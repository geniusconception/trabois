<?php

namespace InventoryBundle\Entity;

use AppBundle\Utils\NumberUtils;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
/**
 * Arbre
 *
 * @UniqueEntity({"numero","inventory"}, message="Le numero de prospection doit être unique dans un inventaire")
 * @ORM\Table(name="arbre",uniqueConstraints={@ORM\UniqueConstraint(name="num_prospection",columns={"numero","inventory_id"})})
 * @ORM\Entity(repositoryClass="InventoryBundle\Repository\ArbreRepository")
 */
class Arbre
{
    const STATUS_BONNE = 1;//Bonne
    const STATUS_MOYENNE = 2;//Moyenne
    const STATUS_MAUVAISE = 3;//Mauvaise

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="numero", type="integer")
     */
    private $numero;

    /**
     * @var \ExploitationBundle\Entity\Essence
     * @ORM\ManyToOne(targetEntity="ExploitationBundle\Entity\Essence")
     */ 
    private $essence;

    /**
     * @var decimal
     *
     * @ORM\Column(name="diametre", type="decimal", precision=5, scale=2)
     */
    private $diametre;

    /**
     * @var int
     * @Assert\Choice({1, 2, 3}, message="la valeur de l'état doit être un valeur entre 1,2 et 3" )
     * @ORM\Column(name="etat", type="integer")
     */
    private $etat;

    /**
     * @var number
     * @Assert\Range(
     * min=-180,
     * minMessage = "la valeur minimale de la longitude est {{ limit }} degré",
     * max=180,
     * maxMessage = "la valeur maximale de la longitude est {{ limit }} degré"
     * )
     * @ORM\Column(name="longitude", type="string", length=255,nullable =true)
     */
    private $longitude;

    /**
     * @var number
     * @Assert\Range(
     * min=-90,
     * minMessage = "la valeur minimale de la lattitude est {{ limit }} degré",
     * max=90,
     * maxMessage = "la valeur maximale de la lattitude est {{ limit }} degré",
     * )
     * @ORM\Column(name="lattitude", type="string", length=255,nullable =true)
     */
    private $lattitude;

    /**
     * @var string
     *
     * @ORM\Column(name="bloc", type="string", length=255,nullable =true)
     */
    private $bloc;

    /**
     * @var string
     *
     * @ORM\Column(name="parcelle", type="string", length=255,nullable =true)
     */
    private $parcelle;

    /**
     * @var Inventory
     *
     * @ORM\ManyToOne(targetEntity="InventoryBundle\Entity\Inventory",inversedBy="arbre")
     * @ORM\JoinColumn(nullable=false)
     */
    private $inventory;

    function __toString()
    {
        return ($this->getId())? sprintf("Arbre No.%d",$this->getNumero()): "n/a";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set numero
     *
     * @param integer $numero
     * @return Arbre
     */
    public function setNumero($numero)
    {
        $this->numero = $numero;

        return $this;
    }

    /**
     * Get numero
     *
     * @return integer 
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Set essence
     *
     * @param integer $essence
     * @return Arbre
     */
    public function setEssence($essence)
    {
        $this->essence = $essence;

        return $this;
    }

    /**
     * Get essence
     *
     * @return \ExploitationBundle\Entity\Essence
     */
    public function getEssence()
    {
        return $this->essence;
    }

    /**
     * @return decimal
     */
    public function getDiametre()
    {
        return $this->diametre;
    }

    /**
     * @param decimal $diametre
     */
    public function setDiametre($diametre)
    {
        $this->diametre = NumberUtils::toNumber($diametre);
    }


    /**
     * Set etat
     *
     * @param integer $etat
     * @return Arbre
     */
    public function setEtat($etat)
    {
        $this->etat = $etat;

        return $this;
    }

    /**
     * Get etat
     *
     * @return integer 
     */
    public function getEtat()
    {
        return $this->etat;
    }

    /**
     * @return number
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param number $longitude
     */
    public function setLongitude($longitude)
    {
        /*if(is_string($longitude))
        {
            $this->longitude = NumberUtils::toNumber($longitude);
        }
        else
        {*/
            $this->longitude = $longitude;
        //}
    }

    /**
     * @return number
     */
    public function getLattitude()
    {
        return $this->lattitude;
    }

    /**
     * @param number $lattitude
     */
    public function setLattitude($lattitude)
    {
        /*if(is_string($lattitude))
        {
            $this->lattitude = NumberUtils::toNumber($lattitude);
        }
        else
        {*/
            $this->lattitude = $lattitude;
       // }
    }

    /**
     * Set bloc
     *
     * @param string $bloc
     * @return Arbre
     */
    public function setBloc($bloc)
    {
        $this->bloc = $bloc;

        return $this;
    }

    /**
     * Get bloc
     *
     * @return string 
     */
    public function getBloc()
    {
        return $this->bloc;
    }

    /**
     * Set parcelle
     *
     * @param string $parcelle
     * @return Arbre
     */
    public function setParcelle($parcelle)
    {
        $this->parcelle = $parcelle;

        return $this;
    }

    /**
     * Get parcelle
     *
     * @return string 
     */
    public function getParcelle()
    {
        return $this->parcelle;
    }

    /**
     * @return array
     */
    public static function getTypes(){
        return array(
            self::STATUS_BONNE => "Bonne",
            self::STATUS_MOYENNE => "Moyenne",
            self::STATUS_MAUVAISE => "Mauvaise",
        );
    }

     

    /**
     * Set inventory
     *
     * @param \InventoryBundle\Entity\Inventory $inventory
     * @return Arbre
     */
    public function setInventory(\InventoryBundle\Entity\Inventory $inventory)
    {
        $this->inventory = $inventory;

        return $this;
    }

    /**
     * Get inventory
     *
     * @return \InventoryBundle\Entity\Inventory 
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    public function toArray(){
        return [
            "numero" => $this->numero,
            "essence" => $this->getEssence()->getNomPilote(),
            "diametre" => $this->diametre,
            "etat" => self::getTypes()[$this->etat],
            "longitude" => $this->longitude,
            "lattitude" => $this->lattitude,
            "bloc" => $this->bloc,
            "parcelle" => $this->parcelle,
        ];
    }

    public static function getFields(){
        return [
            "numero" => "No. Prospection",
            "essence" => "Essence",
            "diametre" => "Diamètre",
            "etat" => "Etat",
            "longitude" => "Longitude",
            "lattitude" => "Lattitude",
            "bloc" => "Bloc",
            "parcelle" => "Parcelle",
        ];
    }

    public static function buildFromArray(array $data){
        $a = new self();

        if(isset($data['essence']))
            $a->setEssence($data['essence']);
        $a->setBloc($data['bloc']);
        $a->setNumero($data['numero']);
        $a->setParcelle($data['parcelle']);
        $a->setDiametre($data['diametre']);
        $a->setLongitude($data['longitude']);
        $a->setLattitude($data['lattitude']);
        $a->setEtat($data['etat']);
        return $a;
    }
}
