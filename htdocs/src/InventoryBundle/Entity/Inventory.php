<?php

namespace InventoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Naming\NamerInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Inventory
 *
 * @ORM\Table(name="inventory")
 * @ORM\Entity(repositoryClass="InventoryBundle\Repository\InventoryRepository")
 * @Vich\Uploadable
 */
class Inventory
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * 
     * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Concession")
     */ 
    private $concession;

    /**
     * 
     * @ORM\OneToMany(targetEntity="InventoryBundle\Entity\Arbre",mappedBy="inventory",orphanRemoval=true,cascade={"persist","remove"})
     */ 
    private $arbre;

     /**
     * @Assert\File(
     *  mimeTypes ={"application/vnd.google-earth.kml+xml", "application/xml"}
     * ,mimeTypesMessage  = "Veuillez soumettre un fichier KML valide."
     * )
     * 
     * @Vich\UploadableField(mapping="document_attach", fileNameProperty="name")
     * 
     * @var File
     */
    private $file;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
    /**
     * @Assert\File(
     *  mimeTypes ={"application/msword","application/vnd.openxmlformats-officedocument.wordprocessingml.document","application/pdf", "application/x-pdf"}
     * ,mimeTypesMessage  = "Veuillez soumettre un fichier PDF ou Word valide."
     * )
     *
     * @Vich\UploadableField(mapping="document_attach", fileNameProperty="inventory")
     *
     * @var File
     */
    private $inventoryFile;

    /**
     * @var string
     *
     * @ORM\Column(name="inventory", type="string", length=255, nullable=true)
     */
    private $inventory;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255,unique=true)
     */
    private $reference;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date",type="datetime")
     */
    private $date;
    /**
     * @var boolean
     *
     * @ORM\Column(name="approved",type="boolean")
     */
    private $approved;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    function __toString()
    {
        return ($this->getId())? sprintf("Inventaire %s",$this->getReference()): "n/a";
    }

    function fullString()
    {
        if(($this->getId())){
            $exp = $this->getConcession()->getEntreprise()->getNom();
            return sprintf("%s - Inventaire %s",$exp,$this->getReference());
        }
        return "n/a";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set concession
     *
     * @param \CorporateBundle\Entity\Concession $concession
     * @return Inventory
     */
    public function setConcession(\CorporateBundle\Entity\Concession $concession = null)
    {
        $this->concession = $concession;

        return $this;
    }

    /**
     * Get concession
     *
     * @return \CorporateBundle\Entity\Concession 
     */
    public function getConcession()
    {
        return $this->concession;
    }

     /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Inventory
     */
    public function setFile(File $image = null)
    {
        $this->file = $image;

        if($image){
            $this->updatedAt = new \DateTime('now');
        }
        
        return $this;
    }

    /**
     * @return File|null
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Inventory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Inventory
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->arbre = new \Doctrine\Common\Collections\ArrayCollection();
        $this->date = new \DateTime();
        $this->approved = false;
        $this->updatedAt = new \DateTime();
    }

    /**
     * Add arbre
     *
     * @param \InventoryBundle\Entity\Arbre $arbre
     * @return Inventory
     */
    public function addArbre(\InventoryBundle\Entity\Arbre $arbre)
    {
        $this->arbre[] = $arbre;
        $arbre->setInventory($this);
        return $this;
    }

    /**
     * Remove arbre
     *
     * @param \InventoryBundle\Entity\Arbre $arbre
     */
    public function removeArbre(\InventoryBundle\Entity\Arbre $arbre)
    {
        $this->arbre->removeElement($arbre);
        $arbre->setInventory(null);
    }

    /**
     * Get arbre
     *
     * @return \Doctrine\Common\Collections\Collection  
     */
    public function getArbre()
    {
        return $this->arbre;
    }
    

    /**
     * @param \Doctrine\Common\Collections\Collection|array $arbre
     */
    public function setArbre($arbre){
        if(gettype($arbre) == 'array'){
            $arbre = new \Doctrine\Common\Collections\ArrayCollection($arbre);
        }
        /** @var Arbre $i */
        foreach ($arbre as $i) {
            $i->setInventory($this);
        }

        $this->arbre = $arbre;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

    /**
     * @return boolean
     */
    public function isApproved()
    {
        return $this->approved;
    }

    /**
     * @param boolean $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    /**
     * @return string
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * @param string $inventory
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * @return File
     */
    public function getInventoryFile()
    {
        return $this->inventoryFile;
    }

    /**
     * @param File $inventoryFile
     */
    public function setInventoryFile($inventoryFile)
    {
        $this->inventoryFile = $inventoryFile;
        if($inventoryFile){
            $this->updatedAt = new \DateTime('now');
        }
    }

}
