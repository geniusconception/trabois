<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 21/07/2017
 * Time: 01:32
 */

namespace InventoryBundle\Event;


use InventoryBundle\Entity\Inventory;
use Symfony\Component\EventDispatcher\Event;

class InventoryEvent extends Event{

    /**
     * @var Inventory
     */
    private $inventory;

    function __construct(Inventory $inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * @return Inventory
     */
    public function getInventory()
    {
        return $this->inventory;
    }

} 