<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 22/07/2017
 * Time: 17:15
 */

namespace InventoryBundle\Listener;


use InventoryBundle\Event\InventoryEvent;
use InventoryBundle\Event\InventoryEvents;
use InventoryBundle\Manager\InventoryManager;
use Sonata\AdminBundle\Admin\Pool;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\File\File as BaseFile;

class ApprovedInventoryListener implements EventSubscriberInterface{
    /**
     * @var Pool
     */
    private $adminPool;
    /**
     * @var InventoryManager
     */
    private $manager;

    function __construct(InventoryManager $manager,Pool $adminPool)
    {
        $this->adminPool = $adminPool;
        $this->manager = $manager;
    }


    public static function getSubscribedEvents()
    {
        return [
            InventoryEvents::INVENTORY_APPROVED => "onApproved",
        ];
    }

    public function onApproved(InventoryEvent $event){
        $i = $event->getInventory();
        /** @var \Sonata\AdminBundle\Admin\AbstractAdmin $admin */
        $admin = $this->adminPool->getAdminByClass('UploadBundle\Entity\Files');
        /** @var \UploadBundle\Entity\Files $trees */
        /*$trees = $admin->getNewInstance();
        $trees->setTitre(sprintf("%s - Rélévé de comptage",$i));
        $file = new BaseFile($this->manager->generateArbreFile($i));
        $trees->setImageFile($file);
        $admin->create($trees);*/
        /** @var \UploadBundle\Entity\Files $inventory */
        $inventory = $admin->getNewInstance();
        $inventory->setTitre(sprintf("%s - document",$i));
        $inventory->setName($i->getInventory());
        $admin->create($inventory);
    }

} 