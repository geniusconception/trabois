<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 21/07/2017
 * Time: 01:37
 */

namespace InventoryBundle\Manager;


use Doctrine\ORM\EntityManagerInterface;
use InventoryBundle\Entity\Arbre;
use InventoryBundle\Entity\Inventory;
use InventoryBundle\Event\InventoryEvent;
use InventoryBundle\Event\InventoryEvents;

class InventoryManager {

    /**
     * @var EntityManagerInterface
     */
    private $em;

    private $dispatcher;

    function __construct($dispatcher, EntityManagerInterface $em)
    {
        $this->dispatcher = $dispatcher;
        $this->em = $em;
    }

    public function approve(Inventory $inventory){
        if($inventory->isApproved())
            return false;
        $inventory->setApproved(true);
        $this->em->persist($inventory);
        $this->em->flush();
        $this->dispatcher->dispatch(InventoryEvents::INVENTORY_APPROVED,new InventoryEvent($inventory));
        return $inventory;
    }

    public function get($reference){
        return $this->em->getRepository("InventoryBundle:Inventory")->findOneBy(['reference' => $reference]);
    }

    public function generateArbreFile(Inventory $i){
        $path = tempnam(sys_get_temp_dir(),'INV');

        $f = new \SplFileObject($path,'w');

        $f->fputcsv(array_values(Arbre::getFields()),";");
        /** @var Arbre $a */
        foreach ($i->getArbre() as $a) {
            $f->fputcsv(array_values($a->toArray()),";");
        }

        return $path;
    }

    public function coordonneeArbre(Inventory $i){
        $arbre = [];

         /** @var Arbre $a */
        foreach ($i->getArbre() as $key => $a) {
             $arbre[$key] = ['lat' => $a->getLattitude(),'lng'=> $a->getLongitude()];
        }

        return $arbre;
    }
     
} 