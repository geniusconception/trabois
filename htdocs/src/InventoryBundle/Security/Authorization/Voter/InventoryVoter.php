<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 21/07/2017
 * Time: 13:53
 */

namespace InventoryBundle\Security\Authorization\Voter;

use AppBundle\Entity\Affectation;
use AppBundle\Manager\AffectionManager;
use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Permission\PermissionMapInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class InventoryVoter extends AclVoter{

    /**
     * @var AffectionManager
     */
    private $affec;

    public function __construct(AffectionManager $affec,AclProviderInterface $aclProvider, ObjectIdentityRetrievalStrategyInterface $oidRetrievalStrategy, SecurityIdentityRetrievalStrategyInterface $sidRetrievalStrategy, PermissionMapInterface $permissionMap, LoggerInterface $logger = null, $allowIfObjectIdentityUnavailable = true)
    {
        parent::__construct($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap, $logger, $allowIfObjectIdentityUnavailable);
        $this->affec = $affec;
    }

    public function supportsClass($class){
        return $class == 'InventoryBundle\Entity\Inventory';
    }

    public function supportsAttribute($attribute){
        return in_array($attribute,["APPROVE","EDIT","DELETE"]);
    }

    public function vote(TokenInterface $token, $object, array $attributes){
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }
        $isExploitant = $this->affec->isExploitant();

        foreach ($attributes as $attribute){
            if ($this->supportsAttribute($attribute)){
                if(!$isExploitant)
                    return self::ACCESS_DENIED;
                $a = $this->affec->getAffectation();
                /** @var \InventoryBundle\Entity\Inventory $object */
                if($object->isApproved())
                    return self::ACCESS_DENIED;
                if($object->getConcession()->getEntreprise()->getId() !== $a->getExploitant()->getId())
                    return self::ACCESS_DENIED;
                if($attribute == 'APPROVE')
                    return ($a->getPosition() === Affectation::POSITION_DIRECTOR)? self::ACCESS_GRANTED:self::ACCESS_DENIED;
                else
                    return ($a->getPosition() !== Affectation::POSITION_DIRECTOR)? self::ACCESS_GRANTED:self::ACCESS_DENIED;
            }
        }

        return self::ACCESS_ABSTAIN;
    }
} 