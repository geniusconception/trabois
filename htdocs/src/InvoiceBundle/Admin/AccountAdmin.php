<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 25/05/2017
 * Time: 17:49
 */

namespace InvoiceBundle\Admin;

use InvoiceBundle\Entity\Account;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class AccountAdmin extends AbstractAdmin{

    public function configureShowFields(ShowMapper $showMapper){
        $showMapper->with("Général",array("class" => "col-lg-6"));
        $showMapper
            ->add('type','choice',array('choices' => Account::getTypes()))
            ->add('accountLabel','text',array('label' => "Nom du compte"))
            ->add('currency','choice',array('choices' => array(
                "USD" => "Dollar US",
                "CDF" => "Francs Congolais"
            ),'label' => "Devise"))
            ->add('country','text',array('label' => 'Pays'))
            #->add('balance',null,array('label' => "solde"))
        ;
        $showMapper->end();
        $showMapper->with("Banque",array("class" => "col-lg-6"))
            ->add('number','text',array('label' => 'Numero du compte','required' => false))
            ->add('iBAN','text',array('label' => 'IBAN','required' => false))
            ->add('bank','text',array('label' => 'Banque','required' => false))
            ->add('owner','text',array('label' => 'Enregistré au nom de:','required' => false))
            ->end()
        ;
    }

    protected function configureFormFields(FormMapper $form){
        $form->with("Général",array("class" => "col-lg-6"));
        $form
            ->add('type','choice',array('choices' => Account::getTypes()))
            ->add('accountLabel','text',array('label' => "Nom du compte"))
            ->add('currency','choice',array('choices' => array(
                "USD" => "Dollar US",
                "CDF" => "Francs Congolais"
            ),'label' => "Devise"))
            ->add('country','text',array('label' => 'Pays'))
            #->add('initial','number',array('label' => 'Solde initial'))
        ;
        $form->end();
        $form->with("Banque",array("class" => "col-lg-6"))
            ->add('number','text',array('label' => 'Numero du compte','required' => false))
            ->add('iBAN','text',array('label' => 'IBAN','required' => false))
            ->add('bank','text',array('label' => 'Banque','required' => false))
            ->add('owner','text',array('label' => 'Enregistré au nom de:','required' => false))
        ->end()
        ;
    }

    protected function configureListFields(ListMapper $list){
        $list
            ->addIdentifier('accountLabel','text',array(
                'label' => "Nom",
                "route" => array("name" => "show")
            ))
            ->add('type','choice',array('choices' => Account::getTypes()))
            #->add('balance',null,array('label' => "solde"))
            ->add('currency','choice',array('choices' => array(
                "USD" => "Dollar US",
                "CDF" => "Francs Congolais"
            ),'label' => "Devise"))
            ->add("_action","actions",array(
                "actions" =>array(
                    "show" => array(),
                    "edit" => array(),
                    "delete" => array(),
                    "accountReport" => array(
                        'template' => "InvoiceBundle:CRUD:list__action_account_report.html.twig"
                    )
                )
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('accountLabel',null,array('label' => "Nom"))
            ->add('country',null,array('label' => 'Pays'))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('accountReport',$this->getRouterIdParameter().'/reports');
    }


} 