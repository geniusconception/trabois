<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 23/05/2017
 * Time: 22:16
 */

namespace InvoiceBundle\Admin;

use AppBundle\Admin\ReversedOrderAdmin;
use CorporateBundle\Model\ExploitantFiltred;
use InvoiceBundle\Entity\Invoice;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class InvoiceAdmin extends ReversedOrderAdmin implements ExploitantFiltred{

    protected $im;

    public function configureShowFields(ShowMapper $showMapper){
        $showMapper
            ->add('ref',null,array("label" => "Référence"))
            ->add('charged','boolean',array("label" => "facturé"))
            ->add('status','choice',array('choices' => Invoice::getAvailableStatus()))
            ->add('transmitter',"text",array('required' => true,"label" => "Emetteur"))
            ->add('target',"text",array('required' => true,"label" => "Adressée à"))
            ->add('dateFacture','date',array('format' => 'd-m-Y'))
            ->add('dateEcheance','date',array('format' => 'd-m-Y'))
            ->add('total')
            ->add('currency','choice',array('choices' => array(
                "USD" => "Dollar US",
                "CDF" => "Francs Congolais"
            ),'label' => "Devise"))
        ;
    }

    protected function configureFormFields(FormMapper $form){
        $form->with('Facture',array(
            "class" => "col-lg-6"
        ));
        /*if($this->getSubject() && !$this->getSubject()->getRef()){
            $form->add('ref','text',array("label" => "Référence"));
        }*/
            $form
                ->add('dateFacture','sonata_type_date_picker',array(
                    'label' => "Date facturation",
                    'format' => 'y-MM-dd'
                ))
                ->add('dateEcheance','sonata_type_date_picker',array(
                    'label' => "Date échéance",
                    'required' => false,
                    'format' => 'y-MM-dd'))
                ->add('charged',null,array("label" => "marquer comme facturé","required"=>false))
            ;
        $form->end();
        $form
            ->with("Général",array(
                "class" => "col-lg-6"
            ))
                #->add('transmitter',null,array("label" => "Emetteur"))
                ->add('target',null,array("label" => "Adressée à"))
                ->add('currency','choice',array('choices' => array(
                    "USD" => "Dollar US",
                    "CDF" => "Francs Congolais"
                ),'label' => "Devise"))
                ->add('note','textarea',array('required' => false,'label' => "Note/Commentaire"))
            ->end()
            ->with("Items",array(
                "class" => "col-lg-12"
            ))
                ->add('items',"sonata_type_collection",array(
                    'type_options' => array(
                        "delete" => true,
                    ),
                    'by_reference' => false,
                    'label' => 'Items'
                ),array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                ))
            ->end()
        ;
    }

    protected function configureListFields(ListMapper $list){
        $list
            ->addIdentifier('ref',null,array(
                "label" => "Référence",
                "route" => array("name" => "show")
            ))
            ->add('target',"text",array('label' => "Adressée à"))
            ->add('dateFacture','datetime',array('label' => "Facturation","format" => "d-m-Y"))
            ->add('status','choice',array(
                'choices' => Invoice::getAvailableStatus(),
                'template' => "SonataAdminBundle:CRUD:list_payment_status.html.twig"
            ))
            ->add('charged','boolean',array("label" => "facturé"))
            ->add('total',null,array('label' => "Total"))
            ->add('currency',null,array('label' => "Devise"))
            ->add("_action","actions",array(
                "actions" =>array(
                    "show" => array(),
                    "edit" => array(),
                    "payments" => array('template' => 'InvoiceBundle:CRUD:list__action_payments.html.twig'),
                    "payment" => array('template' => 'InvoiceBundle:CRUD:list__action_pay.html.twig'),
                    "print" => array('template' => 'InvoiceBundle:CRUD:list__action_print.html.twig'),
                    "delete" => array(),
                )
            ))
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('ref')
            ->add('transmitter')
            ->add('target')
            ->add('status',null,array(
                'label' => 'status'
                ),'choice',array(
                    'choices' => Invoice::getAvailableStatus()
                )
            )
            ->add('charged',null,array("label" => "facturé"))
        ;
    }

    public function configureActionButtons($action, $object = null){
        $list = parent::configureActionButtons($action, $object);
        if(
            in_array($action,array('show','edit',))
            //&& $this->canAccessObject('show', $object)
        ){
            $list['payments_list'] = array(
                'template' =>  'InvoiceBundle:CRUD:action_payments.html.twig'
            );
            $list['payment'] = array(
                'template' =>  'InvoiceBundle:CRUD:action_pay.html.twig'
            );
            $list['print'] = array(
                'template' =>  'InvoiceBundle:CRUD:action_print.html.twig'
            );
        }
        return $list;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('print', $this->getRouterIdParameter().'/print');
    }

    public function setInvoiceManager($im)
    {
        $this->im = $im;
    }

    public function prePersist($f)
    {
        $ref = $this->im->generateRef("FA", $f->getDateFacture());
        $f->setRef($ref);
    }

    public function getTemplate($name)
    {
        if($name == 'show')
            return "InvoiceBundle:CRUD:invoice.html.twig";
        return parent::getTemplate($name);
    }

    /**
     * @return string
     */
    public function getExploitantField()
    {
        return "target";
    }


} 