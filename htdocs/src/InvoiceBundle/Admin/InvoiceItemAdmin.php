<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 23/05/2017
 * Time: 20:54
 */

namespace InvoiceBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class InvoiceItemAdmin extends AbstractAdmin{

    protected function configureFormFields(FormMapper $form){
        $form
            ->add('libelle','text',array('label' => 'Désignation'))
            ->add('tva','number',array('label' => 'TVA'))
            ->add('sku','text',array('label' => 'Unité','required' => false))
            ->add('qte','integer',array('label' => 'Qte'))
            ->add('pu','number',array('label' => 'P.U'))
        ;
    }

    protected function configureListFields(ListMapper $list){
        $list
            ->addIdentifier("libelle",null,array(
                "route" => array("name" => "show")
            ))
            ->add("pu")
            ->add("qte")
            ->add("sku")
            ->add("tva")
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('libelle')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
    }
} 