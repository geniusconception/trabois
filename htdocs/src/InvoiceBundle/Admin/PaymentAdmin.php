<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 25/05/2017
 * Time: 18:17
 */

namespace InvoiceBundle\Admin;

use InvoiceBundle\Entity\Payment;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Doctrine\ORM\EntityRepository;


class PaymentAdmin extends AbstractAdmin{

    /**
     * @var \InvoiceBundle\Manager\InvoiceManager
     */
    private $invoiceManager;
    private $em;

    public function configureShowFields(ShowMapper $mapper){
        $mapper
            ->add('invoice',"text",array("label" => "Réf. Facture"))
            ->add("type",'choice',array("choices" => Payment::getTypes()))
            ->add('account',"text",array("label" => "Compte"))
            ->add("amount",'number',array("label" => "Montant"))
            ->add("date",'datetime',array("label" => "Date","format" => "d-m-Y"))
            ->add("note",null,array("label" => "Note",'required' => false))
        ;
    }

    public function configureFormFields(FormMapper $mapper){
        $subject = $this->getSubject();
        
        $total = $subject->getInvoice()->getTotal();

        $subject->setAmount($total);

        if(!$this->isChild())
            $mapper
                ->add('invoice',"text",array("label" => "Réf. Facture"))
                ;
        $mapper
            ->add("type",'choice',array("choices" => Payment::getTypes()))
            ->add('account',null,array("label" => "Compte"))
            ->add("amount",'number',array("label" => "Montant"))
            ->add("date",'sonata_type_datetime_picker',array("label" => "Date",'format' => 'y-MM-dd'))
            ->add("note",'textarea',array("label" => "Note",'required' => false))
        ;
    }

    protected function configureListFields(ListMapper $list){
        $list
            ->add('invoice',"text",array("label" => "Réf. Facture"))
            ->add("type",'choice',array("choices" => Payment::getTypes()))
            ->add('account',"text",array("label" => "Compte"))
            ->add("amount",'number',array("label" => "Montant"))
            ->add("date",'datetime',array("label" => "Date","format" => "d-m-Y"))
            ->add("_action","actions",array(
                "actions" =>array(
                    "show" => array(),
                    "edit" => array(),
                    "printpayment" => array(
                        'template' => 'InvoiceBundle:CRUD:list__action_printpayment.html.twig'
                    ),
                    "delete" => array()
                    
                )
            ))
        ;
    }

    public function getParentAssociationMapping()
    {
        return 'invoice';
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper){
        $datagridMapper
            ->add('invoice',null,array('label' => "Facture"))
            ->add('account',null,array('label' => "Compte"))
        ;
    }

    /**
     * @param \InvoiceBundle\Manager\InvoiceManager $invoiceManager
     */
    public function setInvoiceManager(\InvoiceBundle\Manager\InvoiceManager $invoiceManager)
    {
        $this->invoiceManager = $invoiceManager;
    }

    public function postPersist($object)
    {
        $this->tryPayment($object);
    }

    public function postUpdate($object)
    {
        $this->tryPayment($object);
    }

    protected function configureRoutes(RouteCollection $collection)//ETAPE 4
    {
        if(!$this->isChild()){
            $collection->clear();
        }
        $collection->add('printpayment', $this->getRouterIdParameter().'/printpayment');
    }

    private function tryPayment(Payment $p){
        $this->invoiceManager->tryPayment($p);
    }

} 