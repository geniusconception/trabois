<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 04/06/2017
 * Time: 22:57
 */

namespace InvoiceBundle\Block;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Block\ChartBlock;

class AccountReportBlock extends ChartBlock{

    public function configureSettings(OptionsResolver $resolver)
    {
        parent::configureSettings($resolver);
        $resolver->setDefault('account',null);
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null){
        $settings = $blockContext->getSettings();
        /** @var \InvoiceBundle\Chart\AccountChart $cb */
        $cb = $this->pool->getBuilder($settings['chart']);
        $cb->setAccount($settings['account']);
        $chart = $cb->buildChart($settings['chart'],$settings['chart_type'],$settings['chart_options']);
        return $this->renderPrivateResponse($settings['template'],array(
            "chart" => $chart,
            "title" => $settings['title'],
        ),$response);
    }
} 