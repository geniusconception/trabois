<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 04/06/2017
 * Time: 23:22
 */

namespace InvoiceBundle\Chart;


use AppBundle\Model\AbstractChart;

abstract class AccountChart extends AbstractChart{

    /**
     * @param \InvoiceBundle\Entity\Account $account
     */
    abstract public function setAccount($account);
} 