<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 04/06/2017
 * Time: 21:56
 */

namespace InvoiceBundle\Chart;


use AppBundle\Utils\RandomColorFactory;
use Mukadi\ChartJSBundle\Model\WorkerInterface;

class AccountMonthMovement extends AccountChart{

    /** @var \InvoiceBundle\Entity\Account  */
    protected $account =null;

    /**
     * @param WorkerInterface $wk ;
     */
    public function factory(WorkerInterface $wk)
    {
        $date = new \DateTime();
        $date = new \DateTime(sprintf("%d-%d-01",$date->format('Y'),$date->format('m')));
        $wk
            ->setModel("InvoiceBundle:Payment",'p')
            ->joinModel("p.account",'a')
            ->func("SUM(p.amount)",'amount')
            ->func("DATEFORMAT(p.date,'%e')",'day')
            ->dataset($wk->values('amount'),"total du jour",array(
                "borderWidth" => 1,
                "backgroundColor" => RandomColorFactory::getRandomColors(31),
            ))
            ->groupBy('day')
            ->labels($wk->values('day'))
            ->order(array(
                "p.date" => "ASC"
            ))
            ->conditions()
                ->_and()
                    ->set("p.account = :account",array("account" => $this->account))
                    ->set("p.date >= :date",array("date" => $date))
                ->end()
            ->endConditions()
        ;
    }

    /**
     * @param \InvoiceBundle\Entity\Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

} 