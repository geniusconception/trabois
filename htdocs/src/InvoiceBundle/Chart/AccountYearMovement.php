<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 05/06/2017
 * Time: 00:28
 */

namespace InvoiceBundle\Chart;

use AppBundle\Utils\RandomColorFactory;
use Mukadi\ChartJSBundle\Model\WorkerInterface;

class AccountYearMovement extends AccountChart{

    /** @var \InvoiceBundle\Entity\Account  */
    protected $account =null;

    /**
     * @param \InvoiceBundle\Entity\Account $account
     */
    public function setAccount($account)
    {
        $this->account = $account;
    }

    /**
     * @param WorkerInterface $wk ;
     */
    public function factory(WorkerInterface $wk)
    {
        $date = new \DateTime();
        $date = new \DateTime(sprintf("%d-01-01",$date->format('Y')));
        $wk
            ->setModel("InvoiceBundle:Payment",'p')
            ->joinModel("p.account",'a')
            ->func("SUM(p.amount)",'amount')
            ->func("MONTHNAME(p.date)",'month')
            ->dataset($wk->values('amount'),"total mensuel",array(
                "borderWidth" => 1,
                "backgroundColor" => RandomColorFactory::getRandomColors(31),
            ))
            ->groupBy('month')
            ->labels($wk->values('month'))
            ->order(array(
                "p.date" => "ASC"
            ))
            ->conditions()
                ->_and()
                    ->set("p.account = :account",array("account" => $this->account))
                    ->set("p.date >= :date",array("date" => $date))
                ->end()
            ->endConditions()
        ;
    }
} 