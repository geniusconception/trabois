<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 03/06/2017
 * Time: 14:48
 */

namespace InvoiceBundle\Chart;


use AppBundle\Model\AbstractChart;
use AppBundle\Utils\RandomColorFactory;
use Mukadi\ChartJSBundle\Model\WorkerInterface;

class AccountsActivity extends AbstractChart{

    protected $currency = "USD";
    protected $maxColors = 10;
    /**
     * @param WorkerInterface $wk ;
     */
    public function factory(WorkerInterface $wk)
    {
        $date = new \DateTime();
        $date = new \DateTime(sprintf("%d-01-01",$date->format('Y')));
        $wk
            ->setModel("InvoiceBundle:Payment","p")
            ->joinModel('p.account','a')
            ->func("SUM(p.amount)",'amount')
            ->func("a.accountLabel","compte")
            ->dataset($wk->values('amount'),"montant total",array(
                "backgroundColor" => RandomColorFactory::getRandomColors($this->maxColors),
            ))
            ->labels($wk->values("compte"))
            ->groupBy("compte")
            ->order(array(
                "p.date" => "ASC"
            ))
            ->conditions()
                ->_and()
                    ->set("a.currency = :currency",array("currency" => $this->currency))
                    ->set("p.date >= :date",array("date" => $date))
                ->end()
            ->endConditions()
        ;
    }

    /**
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * @param int $maxColors
     */
    public function setMaxColors($maxColors)
    {
        $this->maxColors = $maxColors;
    }


} 