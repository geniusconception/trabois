<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 03/06/2017
 * Time: 13:06
 */

namespace InvoiceBundle\Chart;


use AppBundle\Model\AbstractChart;
use AppBundle\Utils\RandomColorFactory;
use Mukadi\ChartJSBundle\Model\WorkerInterface;

class MonthlyPayment extends AbstractChart{

    protected $currency = "USD";

    /**
     * @param WorkerInterface $wk ;
     */
    public function factory(WorkerInterface $wk)
    {
        $date = new \DateTime();
        $date = new \DateTime(sprintf("%d-01-01",$date->format('Y')));
        $wk
            ->setModel("InvoiceBundle:Payment","p")
            ->joinModel('p.invoice','i')
            ->func('SUM(p.amount)','amount')
            ->func('MONTHNAME(p.date)','month')
            ->selector('i.currency','currency')
            ->dataset($wk->values('amount'),"total mensuel",array(
                "borderWidth" => 1,
                "backgroundColor" => RandomColorFactory::getRandomColors(12),
            ))
            ->groupBy('month')
            ->labels($wk->values('month'))
            ->order(array(
                "p.date" => "ASC"
            ))
            ->conditions()
                ->_and()
                    ->set("i.currency = :currency",array("currency" => $this->currency))
                    ->set("p.date >= :date",array("date" => $date))
                ->end()
            ->endConditions()
        ;
    }

    /**
     * @param mixed $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }
} 