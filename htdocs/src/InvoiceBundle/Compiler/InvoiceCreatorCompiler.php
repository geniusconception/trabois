<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 19/06/2017
 * Time: 13:50
 */

namespace InvoiceBundle\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class InvoiceCreatorCompiler implements CompilerPassInterface{
    /**
     * You can modify the container here before it is dumped to PHP code.
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if(!$container->has("invoice.manager.invoice_manager")){
            return;
        }
        $def = $container->findDefinition("invoice.manager.invoice_manager");
        $tagged = $container->findTaggedServiceIds("invoice_creator");

        foreach ($tagged as $id => $tags) {
            foreach ($tags as $attr) {
                $def->addMethodCall("addCreator",array($attr["prefix"], new Reference($id)));
            }
        }
    }


} 