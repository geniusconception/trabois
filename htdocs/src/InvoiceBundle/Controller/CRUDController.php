<?php

namespace InvoiceBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CRUDController extends Controller
{
    public function printAction($id)
    {
        /** @var \InvoiceBundle\Entity\Invoice $dmd */
        $invoice = $this->admin->getSubject();

        if (!$invoice) {
            throw new NotFoundHttpException(sprintf('unable to find the invoice with id : %s', $id));
        }

        $this->admin->checkAccess('show', $invoice);
        /** @var \InvoiceBundle\Manager\InvoiceManager $im */
        $im = $this->get("invoice.manager.invoice_manager");
        $im->toPDF($invoice);
    }

    public function accountReportAction($id)
    {
        /** @var \InvoiceBundle\Entity\Account $a */
        $a = $this->admin->getSubject();

        if (!$a) {
            throw new NotFoundHttpException(sprintf('unable to find account with id : %s', $id));
        }

        $this->admin->checkAccess('show', $a);

        return $this->render("InvoiceBundle:CRUD:account_report.html.twig", array(
            "account" => $a,
            "action" => "show"
        ));
    }

    public function printpaymentAction($id)//ETAPE 2
    {
        /** @var \InvoiceBundle\Entity\Invoice $dmd */
        $payment = $this->admin->getSubject();

        if (!$payment) {
            throw new NotFoundHttpException(sprintf('unable to find the payment with id : %s', $id));
        }

        $this->admin->checkAccess('show', $payment);

        /** @var \InvoiceBundle\Manager\InvoiceManager $im */
        $im = $this->get("invoice.manager.invoice_manager");
        $im->paymentToPDF($payment);
    }
}
