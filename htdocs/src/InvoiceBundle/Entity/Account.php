<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Account
 *
 * @ORM\Table(name="account",uniqueConstraints={@ORM\UniqueConstraint(name="label_currency",columns={"accountLabel","currency"})})
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\AccountRepository")
 * @UniqueEntity({"accountLabel","currency"})
 */
class Account
{
    const TYPE_CASH = 1;
    const TYPE_SAVING = 2;
    const TYPE_CREDIT_OR_CURRENT = 3;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="accountLabel", type="string", length=100)
     */
    private $accountLabel;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3)
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="balance", type="decimal", precision=10, scale=2)
     */
    private $balance;
    /**
     * @var string
     *
     * @ORM\Column(name="initial", type="decimal", precision=10, scale=2)
     */
    private $initial;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="IBAN", type="string", length=255, nullable=true, unique=true)
     */
    private $iBAN;

    /**
     * @var string
     *
     * @ORM\Column(name="bank", type="string", length=255, nullable=true)
     */
    private $bank;

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=255, nullable=true)
     */
    private $owner;

    public function __construct(){
        $this->balance = 0.0;
        $this->initial = 0.0;
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Account
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set accountLabel
     *
     * @param string $accountLabel
     * @return Account
     */
    public function setAccountLabel($accountLabel)
    {
        $this->accountLabel = $accountLabel;

        return $this;
    }

    /**
     * Get accountLabel
     *
     * @return string 
     */
    public function getAccountLabel()
    {
        return $this->accountLabel;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Account
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return Account
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set balance
     *
     * @param string $balance
     * @return Account
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return string 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Account
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set iBAN
     *
     * @param string $iBAN
     * @return Account
     */
    public function setIBAN($iBAN)
    {
        $this->iBAN = $iBAN;

        return $this;
    }

    /**
     * Get iBAN
     *
     * @return string 
     */
    public function getIBAN()
    {
        return $this->iBAN;
    }

    /**
     * Set bank
     *
     * @param string $bank
     * @return Account
     */
    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank
     *
     * @return string 
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set owner
     *
     * @param string $owner
     * @return Account
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string 
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @return string
     */
    public function getInitial()
    {
        return $this->initial;
    }

    /**
     * @param string $initial
     */
    public function setInitial($initial)
    {
        $this->initial = $initial;
    }


    public static function getTypes(){
        return array(
            self::TYPE_CASH => "Cash",
            self::TYPE_SAVING => "Epargne",
            self::TYPE_CREDIT_OR_CURRENT => "Crédit ou Courant",
        );
    }

    function __toString()
    {
        return ($this->getAccountLabel())? $this->getAccountLabel()." - ".$this->getCurrency(): "n/a";
    }


}
