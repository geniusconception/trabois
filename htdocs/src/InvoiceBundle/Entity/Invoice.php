<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TrajetBundle\Model\Checkpoint;

/**
 * Invoice
 *
 * @ORM\Table(name="invoice")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\InvoiceRepository")
 */
class Invoice implements Checkpoint
{
	const STATUS_UNSETTLED = 1;//Non Acquitt�
	const STATUS_PAID = 2;//Acquitt�
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ref", type="string", length=255, unique=true)
     */
    private $ref;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_facture", type="datetime")
     */
    private $dateFacture;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_echeance", type="datetime", nullable=true)
     */
    private $dateEcheance;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3)
     */
    private $currency;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

	 /**
     *
     * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
     * @ORM\JoinColumn(nullable=true)
     */
	private $transmitter;
	
	 /**
     *
     * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
     * @ORM\JoinColumn(nullable=true)
     */
	private $target;
	
	 /**
     * 
	 * @ORM\OneToMany(targetEntity="InvoiceBundle\Entity\InvoiceItem",mappedBy="invoice",orphanRemoval=true,cascade={"persist","remove"})
     */	
	private $items;

    /**
     * @var boolean
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    private $locked;

    /**
     * @var boolean
     *
     * @ORM\Column(name="charged", type="boolean")
     */
    private $charged;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ref
     *
     * @param string $ref
     * @return Invoice
     */
    public function setRef($ref)
    {
        $this->ref = $ref;

        return $this;
    }

    /**
     * Get ref
     *
     * @return string 
     */
    public function getRef()
    {
        return $this->ref;
    }

    /**
     * Set dateFacture
     *
     * @param \DateTime $dateFacture
     * @return Invoice
     */
    public function setDateFacture($dateFacture)
    {
        $this->dateFacture = $dateFacture;

        return $this;
    }

    /**
     * Get dateFacture
     *
     * @return \DateTime 
     */
    public function getDateFacture()
    {
        return $this->dateFacture;
    }

    /**
     * Set dateEcheance
     *
     * @param \DateTime $dateEcheance
     * @return Invoice
     */
    public function setDateEcheance($dateEcheance)
    {
        $this->dateEcheance = $dateEcheance;

        return $this;
    }

    /**
     * Get dateEcheance
     *
     * @return \DateTime 
     */
    public function getDateEcheance()
    {
        return $this->dateEcheance;
    }

    /**
     * Set currency
     *
     * @param string $currency
     * @return Invoice
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Invoice
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Invoice
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
		$this->dateFacture = new \DateTime();
		$this->status = self::STATUS_UNSETTLED;
        $this->locked = false;
        $this->charged = false;
    }

    /**
     * Set transmitter
     *
     * @param \CorporateBundle\Entity\Entreprise $transmitter
     * @return Invoice
     */
    public function setTransmitter(\CorporateBundle\Entity\Entreprise $transmitter = null)
    {
        $this->transmitter = $transmitter;

        return $this;
    }

    /**
     * Get transmitter
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getTransmitter()
    {
        return $this->transmitter;
    }

    /**
     * Set target
     *
     * @param \CorporateBundle\Entity\Entreprise $target
     * @return Invoice
     */
    public function setTarget(\CorporateBundle\Entity\Entreprise $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Add items
     *
     * @param \InvoiceBundle\Entity\InvoiceItem $items
     * @return Invoice
     */
    public function addItem(\InvoiceBundle\Entity\InvoiceItem $items)
    {
        $this->items[] = $items;
        $items->setInvoice($this);
        return $this;
    }

    /**
     * Remove items
     *
     * @param \InvoiceBundle\Entity\InvoiceItem $items
     */
    public function removeItem(\InvoiceBundle\Entity\InvoiceItem $items)
    {
        $this->items->removeElement($items);
        $items->setInvoice(null);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection|array $items
     */
    public function setItems($items){
        if(gettype($items) == 'array'){
            $items = new \Doctrine\Common\Collections\ArrayCollection($items);
        }
        /** @var InvoiceItem $i */
        foreach ($items as $i) {
            $i->setInvoice($this);
        }

        $this->items = $items;
    }

    public function getTotal(){
        $t = 0;
        /** @var InvoiceItem $i */
        foreach ($this->items as $i) {
            $t += $i->getTotal();
        }
        return $t;
    }

    public function __toString(){
        return ($this->getRef())? "Réf.: ".$this->getRef():"n/a";
    }

    public static function getAvailableStatus(){
        return array(
            self::STATUS_PAID => "Acquitté",
            self::STATUS_UNSETTLED => "Non payé",
        );
    }

    public function getName()
    {
       return "invoice";
    }

    public function isLocked()
    {
        return $this->locked;
    }

    public function setLocked($lock)
    {
        $this->locked = $lock;
    }

    public function getIdentifier()
    {
       return array("id" => $this->getId());
    }

    /**
     * @return boolean
     */
    public function isCharged()
    {
        return $this->charged;
    }

    /**
     * @param boolean $charged
     */
    public function setCharged($charged)
    {
        $this->charged = $charged;
    }


}
