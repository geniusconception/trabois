<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InvoiceItem
 *
 * @ORM\Table(name="invoice_item")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\InvoiceItemRepository")
 */
class InvoiceItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=255)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="tva", type="decimal", precision=10, scale=2)
     */
    private $tva;

    /**
     * @var string
     *
     * @ORM\Column(name="pu", type="decimal", precision=10, scale=2)
     */
    private $pu;

    /**
     * @var int
     *
     * @ORM\Column(name="qte", type="integer")
     */
    private $qte;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=20, nullable=true)
     */
    private $sku;
	
	 /**
     * @var Invoice
     *
     * @ORM\ManyToOne(targetEntity="InvoiceBundle\Entity\Invoice",inversedBy="items")
     * @ORM\JoinColumn(nullable=false)
     */
	private $invoice;

    /**
     * @param string $libelle
     * @param int $pu
     * @param int $qte
     * @param null $sku
     * @param float $tva
     */
    function __construct($libelle = "", $pu = 0, $qte = 1, $sku = null, $tva = 0.0)
    {
        $this->libelle = $libelle;
        $this->pu = $pu;
        $this->qte = $qte;
        $this->sku = $sku;
        $this->tva = $tva;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libelle
     *
     * @param string $libelle
     * @return InvoiceItem
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * Get libelle
     *
     * @return string 
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * Set tva
     *
     * @param string $tva
     * @return InvoiceItem
     */
    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva
     *
     * @return string 
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * Set pu
     *
     * @param string $pu
     * @return InvoiceItem
     */
    public function setPu($pu)
    {
        $this->pu = $pu;

        return $this;
    }

    /**
     * Get pu
     *
     * @return string 
     */
    public function getPu()
    {
        return $this->pu;
    }

    /**
     * Set qte
     *
     * @param integer $qte
     * @return InvoiceItem
     */
    public function setQte($qte)
    {
        $this->qte = $qte;

        return $this;
    }

    /**
     * Get qte
     *
     * @return integer 
     */
    public function getQte()
    {
        return $this->qte;
    }

    /**
     * Set sku
     *
     * @param string $sku
     * @return InvoiceItem
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string 
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set invoice
     *
     * @param \InvoiceBundle\Entity\Invoice $invoice
     * @return InvoiceItem
     */
    public function setInvoice(\InvoiceBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \InvoiceBundle\Entity\Invoice 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    function __toString()
    {
        return ($this->getId())? $this->libelle." ( s/total = ".$this->getTotal()." )":"n/a";
    }

    public function getTotal(){
        /** @var double $t */
        $t = $this->pu*$this->qte ;
        return $t + ($this->tva*$t/100);
    }

}
