<?php

namespace InvoiceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Payment
 *
 * @ORM\Table(name="payment")
 * @ORM\Entity(repositoryClass="InvoiceBundle\Repository\PaymentRepository")
 */
class Payment
{
    const TYPE_BANK_TRANSFER = 1;
    const TYPE_CASH = 2;
    const TYPE_CHECK = 3;
    const TYPE_DEBIT_PAYMENT_ORDER = 4;
    const TYPE_INTERNET_TRANSACTION = 5;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="smallint")
     */
    private $type;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="note", type="text", nullable=true)
     */
    private $note;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", length=255, nullable=true, unique=true)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="amount", type="decimal", precision=10, scale=2)
     */
    private $amount;
    /**
     * @ORM\ManyToOne(targetEntity="InvoiceBundle\Entity\Invoice")
     */
    private $invoice;
    /**
     * @var \InvoiceBundle\Entity\Account
     * @ORM\ManyToOne(targetEntity="InvoiceBundle\Entity\Account")
     */
    private $account;

    public function __construct(){
        $this->date = new \DateTime();
    }
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Payment
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Payment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set note
     *
     * @param string $note
     * @return Payment
     */
    public function setNote($note)
    {
        $this->note = $note;

        return $this;
    }

    /**
     * Get note
     *
     * @return string 
     */
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Payment
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set amount
     *
     * @param string $amount
     * @return Payment
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string 
     */
    public function getAmount()
    {
        return $this->amount;
    }

    public static function getTypes(){
        return array(
            self::TYPE_BANK_TRANSFER => "Bank Transfer",
            self::TYPE_CASH => "Cash",
            self::TYPE_CHECK => "Check",
            self::TYPE_DEBIT_PAYMENT_ORDER => "Debit Payment Order",
            self::TYPE_INTERNET_TRANSACTION => "Internet Transfert",
        );
    }

    /**
     * Set invoice
     *
     * @param \InvoiceBundle\Entity\Invoice $invoice
     * @return Payment
     */
    public function setInvoice(\InvoiceBundle\Entity\Invoice $invoice = null)
    {
        $this->invoice = $invoice;

        return $this;
    }

    /**
     * Get invoice
     *
     * @return \InvoiceBundle\Entity\Invoice 
     */
    public function getInvoice()
    {
        return $this->invoice;
    }

    /**
     * Set account
     *
     * @param \InvoiceBundle\Entity\Account $account
     * @return Payment
     */
    public function setAccount(\InvoiceBundle\Entity\Account $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \InvoiceBundle\Entity\Account 
     */
    public function getAccount()
    {
        return $this->account;
    }

    function __toString()
    {
        $d = $this->getDate();
        return ($this->getId())? "PA".$d->format('Y').$d->format('m')."-".$this->getId(): "n/a";
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context){
        if($this->getAccount() && $this->getInvoice()){
            $currency = $this->getInvoice()->getCurrency();
            if($this->getAccount()->getCurrency() != $currency)
                $context->buildViolation(sprintf("Veuillez séléctionner un compte en %s, svp",$currency))->atPath('account')->addViolation();
        }
    }

}
