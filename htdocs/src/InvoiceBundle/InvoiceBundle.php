<?php

namespace InvoiceBundle;

use InvoiceBundle\Compiler\InvoiceCreatorCompiler;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class InvoiceBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $container->addCompilerPass(new InvoiceCreatorCompiler());
    }

}
