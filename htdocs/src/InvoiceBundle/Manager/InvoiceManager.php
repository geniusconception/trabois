<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 25/05/2017
 * Time: 01:04
 */

namespace InvoiceBundle\Manager;


use AppBundle\Model\Compteur;
use Doctrine\ORM\EntityManagerInterface;
use InvoiceBundle\Entity\Invoice;
use InvoiceBundle\Entity\Payment;
use InvoiceBundle\Model\InvoiceCreator;
use InvoiceBundle\Model\InvoiceTemplate;
use \Twig_Environment;

class InvoiceManager implements InvoiceCreator{

    /** @var EntityManagerInterface  */    
    private $em;

    /** @var EntityManagerInterface  */
    private $twig;

    /** @var  Compteur */
    private $compteur;
    /**
     * @var array
     */
    private $creators;

    public function __construct(EntityManagerInterface $em,\Twig_Environment $twig,Compteur $compteur)
    {
        $this->em = $em;
        $this->twig = $twig;
        $this->compteur = $compteur;
        $this->creators = array();
    }

    /**
     * @param InvoiceTemplate $template
     * @param boolean $persist
     * @return Invoice
     */
    public function create(InvoiceTemplate $template,$persist = true){
        if(isset($this->creators[$template->getInvoicePrefix()])){
            /** @var InvoiceCreator $creator */
            $creator = $this->creators[$template->getInvoicePrefix()];
            $invoice = $creator->create($template);
        }else{
            $invoice = new Invoice();
            $invoice->setItems($template->getItems());
            $invoice->setCurrency($template->getCurrency());
            $invoice->setTarget($template->getTarget());
        }
        if($persist){
            $d = $invoice->getDateFacture();
            $ref = $this->generateRef($template->getInvoicePrefix(), $d);
            $invoice->setRef($ref);

            $this->em->persist($invoice);
            $this->em->flush();
        }

        return $invoice;
    }

    public function toPDF(Invoice $invoice){      
         
        $html = $this->twig->render('InvoiceBundle:Pdf:invoice.html.twig', array('invoice' => $invoice));

        $pdf = new \HTML2PDF("p","A4","fr");
        $pdf->writeHTML($html);
        $pdf->Output('Facture_'.$invoice->getRef().'.pdf');
        exit;

    }

    public function paymentToPDF(Payment $payment){//ETAPE 1      
         
        $html = $this->twig->render('InvoiceBundle:Pdf:payment.html.twig', array('payment' => $payment));

        $pdf = new \HTML2PDF("p","A4","fr");
        $pdf->writeHTML($html);
        $pdf->Output('Recu_'.$payment->getInvoice().'_'.$payment->getId().'.pdf');
        exit;
    }

    public function generateRef($prefix,\DateTime $d=null)
    {
        if($d==null)
        {
            $d = new \DateTime();
        }

        $ref = $prefix;
        $ref .= $d->format('Y').$d->format('m');
        $ref .= "-".$this->compteur->nextValue($prefix);

        return $ref;
    }

    public function tryPayment(Payment $p){
        if($invoice = $p->getInvoice()){
            $payments = $this->em->getRepository("InvoiceBundle:Payment")->findBy(array("invoice" => $invoice));
            $total = 0.0;
            /** @var \InvoiceBundle\Entity\Payment $py */
            foreach ($payments as $py) {
                $total += (double) $py->getAmount();
            }

            $status = ($total >= $invoice->getTotal())? Invoice::STATUS_PAID: Invoice::STATUS_UNSETTLED;
            $invoice->setStatus($status);
            $this->em->persist($invoice);
            $this->em->flush();
        }
    }

    /**
     * @param string $prefix
     * @param InvoiceCreator $creator
     */
    public function addCreator($prefix, InvoiceCreator $creator){
        $this->creators[$prefix] = $creator;
    }
} 