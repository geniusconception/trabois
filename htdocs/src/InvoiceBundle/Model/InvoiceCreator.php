<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 19/06/2017
 * Time: 13:39
 */

namespace InvoiceBundle\Model;

use InvoiceBundle\Entity\Invoice;


interface InvoiceCreator {

    /**
     * @param InvoiceTemplate $template
     * @return Invoice
     */
    public function create(InvoiceTemplate $template);
} 