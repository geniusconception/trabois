<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 25/05/2017
 * Time: 00:54
 */

namespace InvoiceBundle\Model;


interface InvoiceTemplate {

    /**
     * @return integer
     */
    public function getInvoiceId();

    /**
     * @return array
     */
    public function getItems();

    /**
     * @return string
     */
    public function getCurrency();

    /**
     * @return string
     */
    public function getInvoicePrefix();

    /**
     * @return \CorporateBundle\Entity\Entreprise
     */
    public function getTarget();
}