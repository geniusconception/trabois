<?php

namespace InvoiceBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * InvoiceItemRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class InvoiceItemRepository extends EntityRepository
{
}
