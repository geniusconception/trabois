<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 28/06/2017
 * Time: 22:37
 */

namespace InvoiceBundle\Security\Authorization\Voter;

use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class InvoiceVoter extends AclVoter{

    public function supportsClass($class)
    {
        return in_array($class,array('InvoiceBundle\Entity\Invoice'));
    }

    public function supportsAttribute($attribute)
    {
        return $attribute !== "VIEW" && $attribute !== "LIST";
    }

    public function vote(TokenInterface $token, $object, array $attributes){
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }
        foreach ($attributes as $attribute) {
            if ($this->supportsAttribute($attribute)) {
                /** @var \InvoiceBundle\Entity\Invoice $object */
                if ($object->isLocked()) {
                    return self::ACCESS_DENIED;
                }
            }
        }
        return self::ACCESS_ABSTAIN;
    }
} 