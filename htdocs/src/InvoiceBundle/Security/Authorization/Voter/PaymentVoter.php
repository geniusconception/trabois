<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 29/05/2017
 * Time: 20:09
 */

namespace InvoiceBundle\Security\Authorization\Voter;

use InvoiceBundle\Entity\Invoice;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Permission\PermissionMapInterface;
use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Sonata\AdminBundle\Admin\Pool;

class PaymentVoter extends AclVoter{

    /** @var Pool  */
    private $adminPool;

    public function __construct(Pool $pool, AclProviderInterface $aclProvider, ObjectIdentityRetrievalStrategyInterface $oidRetrievalStrategy, SecurityIdentityRetrievalStrategyInterface $sidRetrievalStrategy, PermissionMapInterface $permissionMap, LoggerInterface $logger = null, $allowIfObjectIdentityUnavailable = true)
    {
        parent::__construct($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap, $logger, $allowIfObjectIdentityUnavailable);
        $this->adminPool = $pool;
    }


    public function supportsClass($class)
    {
        return in_array($class,array('InvoiceBundle\Entity\Invoice'));
    }

    public function supportsAttribute($attribute)
    {
        return $attribute === "PAY" || $attribute === "EDIT" || $attribute === "DELETE";
    }

    public function vote(TokenInterface $token, $object, array $attributes)
    {
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }

        foreach ($attributes as $attribute) {
            if ($this->supportsAttribute($attribute)) {
                /** @var \InvoiceBundle\Entity\Invoice $object */
                if($object->getStatus() === Invoice::STATUS_PAID || $object->isLocked()){
                    return self::ACCESS_DENIED;
                }
                if($attribute === "PAY"){
                    $admin = $this->adminPool->getAdminByClass('InvoiceBundle\Entity\Payment');
                    if($admin === null)
                        return self::ACCESS_ABSTAIN;
                    if($admin->isGranted('CREATE') && $object->isCharged())
                        return self::ACCESS_GRANTED;
                    else
                        return self::ACCESS_DENIED;
                }
            }
        }

        return self::ACCESS_ABSTAIN;
    }


} 