<?php

namespace LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Table(name="location")
 * @ORM\Entity(repositoryClass="LocationBundle\Repository\LocationRepository")
 */
class Location
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     
    private $pays;

    
    private $ville;

     
    private $province;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pays
     *
     * @param \LocationBundle\Entity\Pays $pays
     * @return Location
     */
    public function setPays(\LocationBundle\Entity\Pays $pays = null)
    {
        $this->pays = $pays;

        return $this;
    }

    /**
     * Get pays
     *
     * @return \LocationBundle\Entity\Pays 
     */
    public function getPays()
    {
        return $this->pays;
    }

    /**
     * Set ville
     *
     * @param \LocationBundle\Entity\Ville $ville
     * @return Location
     */
    public function setVille(\LocationBundle\Entity\Ville $ville = null)
    {
        $this->ville = $ville;

        return $this;
    }

    /**
     * Get ville
     *
     * @return \LocationBundle\Entity\Ville 
     */
    public function getVille()
    {
        return $this->ville;
    }

    /**
     * Set province
     *
     * @param \LocationBundle\Entity\Province $province
     * @return Location
     */
    public function setProvince(\LocationBundle\Entity\Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return \LocationBundle\Entity\Province 
     */
    public function getProvince()
    {
        return $this->province;
    }

    function __toString()
    {
        return ($this->getId())? $this->getPays().' / '.$this->getProvince().' / '.$this->getVille():"n/a";
    }
}
