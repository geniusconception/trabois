<?php

namespace LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pays
 *
 * @ORM\Table(name="pays")
 * @ORM\Entity(repositoryClass="LocationBundle\Repository\CountryRepository")
 */
class Pays
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=3)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="libcountry", type="string", length=255)
     */
    private $libcountry;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libcountry
     *
     * @param string $libcountry
     * @return Pays
     */
    public function setLibcountry($libcountry)
    {
        $this->libcountry = $libcountry;

        return $this;
    }

    /**
     * Get libcountry
     *
     * @return string 
     */
    public function getLibcountry()
    {
        return $this->libcountry;
    }

    function __toString()
    {
        return ($this->getId())? $this->getLibcountry():"n/a";
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Pays
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }
}
