<?php

namespace LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Province
 *
 * @ORM\Table(name="province")
 * @ORM\Entity(repositoryClass="LocationBundle\Repository\ProvinceRepository")
 */
class Province
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="libprovince", type="string", length=255)
     */
    private $libprovince;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libprovince
     *
     * @param string $libprovince
     * @return Province
     */
    public function setLibprovince($libprovince)
    {
        $this->libprovince = $libprovince;

        return $this;
    }

    /**
     * Get libprovince
     *
     * @return string 
     */
    public function getLibprovince()
    {
        return $this->libprovince;
    }

    function __toString()
    {
        return ($this->getId())? $this->getLibprovince():"n/a";
    }
}
