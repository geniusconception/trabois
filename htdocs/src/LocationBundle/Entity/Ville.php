<?php

namespace LocationBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Ville
 *
 * @ORM\Table(name="ville")
 * @ORM\Entity(repositoryClass="LocationBundle\Repository\VilleRepository")
 */
class Ville
{
    const VILLE = 1;//ville
    const TERRITOIRE = 2;//territoire

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="Province")
    * 
    */
    private $province;

    /**
     * @var string
     *
     * @ORM\Column(name="libville", type="string", length=255)
     */
    private $libville;

     /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer")
     */
    private $type;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set libville
     *
     * @param string $libville
     * @return Ville
     */
    public function setLibville($libville)
    {
        $this->libville = $libville;

        return $this;
    }

    /**
     * Get libville
     *
     * @return string 
     */
    public function getLibville()
    {
        return $this->libville;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return Ville
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

     /**
     * Set province
     *
     * @param \LocationBundle\Entity\Province $province
     * @return Ville
     */
    public function setProvince(\LocationBundle\Entity\Province $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return \LocationBundle\Entity\Province 
     */
    public function getProvince()
    {
        return $this->province;
    }

    public static function getAvailableStatus(){
        return array(
            self::VILLE => "Ville",
            self::TERRITOIRE => "Territoire",
        );
    }

    function __toString()
    {
        return ($this->getType()==1)? "".ucfirst($this->getLibville()):"".ucfirst($this->getLibville());
    }

}
