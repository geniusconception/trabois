<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 26/07/2017
 * Time: 13:24
 */

namespace StatsBundle\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class StatsCompilerPass implements CompilerPassInterface{

    public function process(ContainerBuilder $container)
    {
        if(!$container->has("stats.stat_pool")){
            return;
        }
        $def = $container->findDefinition("stats.stat_pool");

        $tagged = $container->findTaggedServiceIds("stats.stats");

        foreach ($tagged as $id => $tags) {
            foreach ($tags as $attr) {
                $def->addMethodCall("addStats",[new Reference($id)]);
                $def->addMethodCall("link",[new Reference($id),$attr["admin"]]);
            }
        }
    }
} 