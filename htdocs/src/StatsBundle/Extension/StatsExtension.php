<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 26/07/2017
 * Time: 12:28
 */

namespace StatsBundle\Extension;

use Sonata\AdminBundle\Admin\AbstractAdminExtension;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Route\RouteCollection;

class StatsExtension extends AbstractAdminExtension{

    public function configureRoutes(AdminInterface $admin, RouteCollection $collection)
    {
        $collection->add('stats',"stats");
        $collection->add('generateStats',"stats/generate");
    }

    public function configureActionButtons(AdminInterface $admin, $list, $action, $object)
    {
        if(in_array($action,['list','generateStats'])){
            $list['stats'] = array(
                'template' => 'StatsBundle:Button:stats.html.twig',
            );
        }
        if(in_array($action,['stats','generateStats'])){
            $list['list'] = array(
                'template' => 'SonataAdminBundle:Button:list_button.html.twig',
            );
        }
        return $list;
    }
} 