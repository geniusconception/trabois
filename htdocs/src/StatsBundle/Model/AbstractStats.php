<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 27/07/2017
 * Time: 17:09
 */

namespace StatsBundle\Model;


abstract class AbstractStats implements StatsInterface{

    protected $defaultTableTemplate = "StatsBundle:Template:table.html.twig";
    /**
     * @var \Twig_Environment
     */
    protected $twig;

    function __construct(\Twig_Environment $twig)
    {
        $this->twig = $twig;
    }

    protected function getTableView(array $header,$entries, $template = null){
        $template = ($template)? : $this->defaultTableTemplate;
        $e = [];

        return $this->twig->render($template,[
            "header" => array_values($header),
            "entries" => $e,
        ]);
    }

    protected function attribute($object,$name){
        if(is_array($object)){
            return $object[$name];
        }
        $name = str_replace("_"," ",$name);
        $name = str_replace(" ","",ucwords($name));
        $name = 'get'.$name;
        if(method_exists($object,$name)){
            return $object->$name;
        }
        return null;
    }
} 