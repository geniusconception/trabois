<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 25/07/2017
 * Time: 23:50
 */

namespace StatsBundle\Model;

use Sonata\AdminBundle\Datagrid\ProxyQueryInterface;

interface StatsInterface {

    public function buildStats(ProxyQueryInterface $query);

    public function getDescription();

    public function getTitle();

    public function getName();
} 