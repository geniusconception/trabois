<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 26/07/2017
 * Time: 00:00
 */

namespace StatsBundle\Model;


use Sonata\AdminBundle\Admin\AdminInterface;

class StatsPool {

    /**
     * @var array
     */
    private $stats;
    /**
     * @var array
     */
    private $storage;

    public function __construct(){
        $this->stats = [];
        $this->storage = [];
    }

    /**
     * @param StatsInterface $stats
     */
    public function addStats(StatsInterface $stats){
        $this->stats[$stats->getName()] = $stats;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function statsExist($name){
        return isset($this->stats[$name]);
    }

    /**
     * @param $name
     * @return StatsInterface
     */
    public function getStats($name){
        return $this->stats[$name];
    }

    /**
     * @param StatsInterface $stats
     * @param string $adminCode
     */
    public function link(StatsInterface $stats, $adminCode){
        $this->storage[$adminCode][] = [
            "title" => $stats->getTitle(),
            "name" => $stats->getName(),
            "description" => $stats->getDescription(),
        ];
    }

    /**
     * @param string $code
     * @return array
     */
    public function getAdminStats($code){
        if(isset($this->storage[$code]))
            return $this->storage[$code];
        else
            return [];
    }
} 