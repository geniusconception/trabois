<?php

namespace TagBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use AppBundle\Manager\AffectionManager;
use Sonata\AdminBundle\Route\RouteCollection;
use AppBundle\Admin\ReversedOrderAdmin;
use CorporateBundle\Model\ExploitantFiltred;
use TagBundle\Entity\Request;


class RequestAdmin extends ReversedOrderAdmin implements ExploitantFiltred
{
     /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;
   
    /**
     * @var \AppBundle\Model\Compteur
     */
    private $compteur;
     
    /**
     * @var AffectionManager
     */
    private $affm;

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('reference')
            ->add('dateCreate')
            ->add('status',null,[
                "label" => "Status",
            ],'choice',[
                'choices' => Request::getStatuses()
            ])
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('reference',"text",array(
                "label" => "Référence",
                "route" => ['name' => 'show']
            ))
            ->add('dateCreate',"datetime",array("label" => "Date"))
            ->add('status','choice',array(
                "label" => "Etat",
                'choices' => Request::getStatuses(),
            ))
            ->add('approved',null,["label" => "Approuvée ?"])
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'validate' => [
                        'template' => "TagBundle:CRUD:list__action_validate.html.twig"
                    ]
                )
            ))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->add('approve',$this->getRouterIdParameter().'/approve');
        $collection->add('invoice',$this->getRouterIdParameter().'/invoice');
        $collection->add('submit',$this->getRouterIdParameter().'/submit');
        $collection->add('validate',$this->getRouterIdParameter().'/validate');
    }

    public function configureActionButtons($action, $object = null){
        $list = parent::configureActionButtons($action, $object);

        if(in_array($action,['show'])){
            $list['approve'] = [
                'template' => 'TagBundle:CRUD:action_approve.html.twig'
            ];

            $list['invoice'] = [
                'template' => 'TagBundle:CRUD:action_invoice.html.twig'
            ];

            $list['submit'] = [
                'template' => 'TagBundle:CRUD:action_submit.html.twig'
            ];

            $list['validate'] = [
                'template' => 'TagBundle:CRUD:action_validate.html.twig'
            ];
        }

        return $list;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
             ->add('items',"sonata_type_collection",array(
                    'type_options' => array(
                        "delete" => true,
                    ),
                    'by_reference' => false,
                    'label' => 'Renseignements sur les étiquettes à code-barres'
                ),array(
                    'edit' => 'inline',
                    'inline' => 'table',
                    'sortable' => 'position',
                ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('reference')
            ->add('dateCreate')
            ->add('status','choice',array(
                "label" => "Etat",
                'choices' => Request::getStatuses(),
            ))
            ->add('approved',null,["label" => "Approuvée ?"])
        ;
    }

    /**
     * @param \AppBundle\Model\Compteur $compteur
     */
    public function setCompteur($compteur)
    {
        $this->compteur = $compteur;
    }


    /**
     * @param mixed $tokenStorage
    */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }
 
    public function prePersist($object)
    {
        /** @var \TagBundle\Entity\Request $object */
        $object->setAgent($this->tokenStorage->getToken()->getUser());
        $exp = $this->getExploitant();
        $object->setExploitant($exp);
        /** @var \DateTime $d */
        $d = $object->getDateCreate();
        $prefix = "DME";
        $ref = $prefix;
        $ref .= $d->format('m').$d->format('Y');
        $ref .= "-".$this->compteur->nextValue($prefix);
        $object->setReference($ref);
    }

    /**
     * @param AffectionManager $affm
     */
    public function setAffm(AffectionManager $affm)
    {
        $this->affm = $affm;
    }

    /**
     * @return string
     */
    public function getExploitantField()
    {
        return "exploitant";
    }

    /**
     * @return \CorporateBundle\Entity\Entreprise|null
     */
    public function getExploitant(){
        return ($this->affm->isExploitant())? $this->affm->getAffectation()->getExploitant(): null;
    }

    public function getTemplate($name)
    {
        if($name == "show")
            return "TagBundle:CRUD:request.html.twig";
        return parent::getTemplate($name);
    }


}
