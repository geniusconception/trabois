<?php

namespace TagBundle\Admin;

use Doctrine\ORM\EntityRepository;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use TagBundle\Entity\RequestItem;
use AppBundle\Manager\AffectionManager;

class RequestItemAdmin extends AbstractAdmin
{
    /**
     * @var AffectionManager
     */
    private $affm;
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('tagType')
            ->add('refPCB')
            ->add('number')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('tagType','choice',array('choices' => RequestItem::getAvailableStatus(),'label' => "Type d'étiquettes"))
            ->add('refPCB',"text",array("label" => "N° Référence PCB ou ACIBO"))
            ->add('number',"number",array("label" => "Nombre"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $exp = $this->getExploitant();
        $formMapper
            ->add('tagType','choice',array('choices' => RequestItem::getAvailableStatus(),'label' => "Type d'étiquettes"))
            ->add('refPCB','Symfony\Bridge\Doctrine\Form\Type\EntityType',array(
                "label" => "N° Référence PCB ou ACIBO",
                "class" => "ExploitationBundle:Permis",
                "get_property" => 'reference',
                'query_builder' => function(EntityRepository $er) use ($exp){
                    return $er
                        ->createQueryBuilder('p')
                        ->leftJoin('p.inventory','i')
                        ->addSelect('i')
                        ->leftJoin('i.concession','c')
                        ->addSelect('c')
                        ->where("p.active = :apv AND i.approved = :apv AND c.entreprise = :exp")
                        ->setParameter('apv',true)
                        ->setParameter('exp',$exp)
                        ;
                }
            ))
            ->add('number',"number",array("label" => "Nombre"))           
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('tagType')
            ->add('refPCB')
            ->add('number')
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('list');
        $collection->remove('show');
    }

    /**
     * @param AffectionManager $affm
     */
    public function setAffm(AffectionManager $affm)
    {
        $this->affm = $affm;
    }

    /**
     * @return \CorporateBundle\Entity\Entreprise|null
     */
    public function getExploitant(){
        return ($this->affm->isExploitant())? $this->affm->getAffectation()->getExploitant(): null;
    }
}
