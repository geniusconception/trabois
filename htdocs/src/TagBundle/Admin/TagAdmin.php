<?php

namespace TagBundle\Admin;

use AppBundle\Model\EnableImportsInterface;
use CorporateBundle\Model\ExploitantFiltred;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Sonata\AdminBundle\Route\RouteCollection;
use TagBundle\Entity\Tag;

class TagAdmin extends AbstractAdmin implements EnableImportsInterface, ExploitantFiltred
{
    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('exploitant',null,array(
                'label'=>'Exploitant'
                ))
            ->add('code',null,array(
                'label'=>'Code'
                ))
            ->add('used',null,array(
                'label'=>'Etat'
                ))
            ->add('type',null,array(
                    'label' => 'Types'
                ),'choice',array(
                    'choices' => Tag::getTypes()
                )
            )
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('code',null,array(
                'label'=>'Code',
                'route' => array('name' => 'show')
            ))
            ->add('type','choice',array(
                'choices' => Tag::getTypes(),
            ))
            ->add('exploitant',null,array(
                'label'=>'Exploitant'
                ))
            ->add('target',null,array(
                'label'=>'Produit'
                ))
            ->add('used',null,array(
                'label'=>'utilisée ?'
                ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('exploitant','text',array(
                'label'=>'Exploitant'
                ))
            ->add('target','text',array(
                'label'=>'Cible'
                ))
            ->add('code','text',array(
                'label'=>'Code'
                ))
            ->add('used','text',array(
                'label'=>'Etat'
                ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('exploitant',null,array(
                'label'=>'Exploitant'
                ))
            ->add('target',null,array(
                'label'=>'Produit'
                ))
            ->add('code',null,array(
                'label'=>'Code'
                ))
            ->add('used',null,array(
                'label'=>'Utilisée ?'
                ))
        ;
    }

    /**
     * @param mixed $tokenStorage
     */
    public function setTokenStorage(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function prePersist($object)
    {
        $object->setUser($this->tokenStorage->getToken()->getUser());
    }

    protected function configureRoutes(RouteCollection $collection)
    {         
        $collection->clearExcept(array('list', 'show'));
    }

    /**
     * @return array
     */
    public function configureImportFields()
    {
        return array(
            'type',
            'code',
         );
    }

    /**
     * @return string
     */
    public function getExploitantField()
    {
        return "exploitant";
    }


}
