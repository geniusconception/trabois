<?php

namespace TagBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class TargetAdmin extends AbstractAdmin
{
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            /*->add('model',null,array(
                'label'=>'Modèle'
                ))
            ->add('identifier',null,array(
                'label'=>'Identifiant'
                ))
            ->add('description',null,array(
                'label'=>'Description'
                ))
            ->add('reference',null,array(
                'label'=>'Référence'
                ))*/
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            /*>add('model','text',array(
                'label'=>'Modèle'
                ))
            ->add('identifier','text',array(
                'label'=>'Identifiant'
                ))
            ->add('description','textarea',array(
                'label'=>'Description'
                ))
            ->add('reference','text',array(
                'label'=>'Référence'
                ))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                )
            ))*/
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('codeEtiquette','text',array(
                'label'=>'Modèle'
                ))
            ->add('identifier','text',array(
                'label'=>'Identifiant'
                ))
            ->add('description','textarea',array(
                'label'=>'Description'
                ))
            ->add('reference','text',array(
                'label'=>'Référence'
                ))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('model',null,array(
                'label'=>'Modèle'
                ))
            ->add('identifier',null,array(
                'label'=>'Identifiant'
                ))
            ->add('description',null,array(
                'label'=>'Description'
                ))
            ->add('reference',null,array(
                'label'=>'Référence'
                ))
        ;
    }

    protected function configureRoutes(RouteCollection $collection)
    {        
        $collection->clearExcept(array('list', 'view'));
    }
}
