<?php

namespace TagBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as CRUD;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use TagBundle\Entity\Tag;
use TagBundle\Form\RequestType;
use TagBundle\Manager\RequestManager;

class RequestController extends CRUD
{
    public function approveAction(Request $request)
    {
        $id = $request->get($this->admin->getIdParameter());
        /** @var \TagBundle\Entity\Request $r */
        $r = $this->admin->getObject($id);

        if (!$r) {
            throw new NotFoundHttpException(sprintf('unable to find the request tag with id : %s', $id));
        }

        if($r->isApproved()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autorisé à approuver cet enregistrement");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        if(!$this->admin->isGranted('APPROVE',$r))
            throw $this->createAccessDeniedException("vous n'êtes pas authoriser à effectuer cette opération");

        $url = $this->admin->generateObjectUrl('approve',$r);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            /** @var \TagBundle\Manager\RequestManager $manager */
            $manager = $this->get("tag.manager.request");

            if($i = $manager->approve($r)){
                $this->get("app.acl_manager")->saveAcl($i);
                $this->addFlash('sonata_flash_success', sprintf('La demande a été approuvée avec succès, la facture %s a été générée!',$i->getRef()));
            }else
                $this->addFlash('flash_create_error', "La demande n'a pas pu être approuvée");

            return $this->redirect($this->admin->generateUrl('show',[$this->admin->getIdParameter() => $id]));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $r,
            "message" => sprintf('Êtes-vous sûr de vouloir approuver la demande "%s" ?',$r->__toString()),
        ));
    }

    public function invoiceAction(Request $request){
        $id = $request->get($this->admin->getIdParameter());
        /** @var \TagBundle\Entity\Request $r */
        $r = $this->admin->getObject($id);

        if (!$r) {
            throw new NotFoundHttpException(sprintf('unable to find the request tag with id : %s', $id));
        }

        $this->admin->isGranted('show',$r);

        if(!$r->isApproved()){
            $this->addFlash('sonata_flash_error', "Impossible d'imprimer la facture, veuillez d'abord approuver la demande");
            return $this->redirect($this->admin->generateUrl('list'));
        }

        /** @var \InvoiceBundle\Manager\InvoiceManager $im */
        $im = $this->get("invoice.manager.invoice_manager");
        $invoice = $im->create($r,false);
        $im->toPDF($invoice);
    }

    public function submitAction(Request $request){
        $id = $request->get($this->admin->getIdParameter());
        /** @var \TagBundle\Entity\Request $r */
        $r = $this->admin->getObject($id);

        if (!$r) {
            throw new NotFoundHttpException(sprintf('unable to find the request tag with id : %s', $id));
        }

        if(!$this->isGranted('SUBMIT',$r))
            throw $this->createAccessDeniedException("vous n'êtes pas authoriser à effectuer cette opération");

        $this->admin->setSubject($r);
        $form = $this->createForm(new RequestType(),$r);
        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted()){
            /** @var \TagBundle\Manager\RequestManager $manager */
            $manager = $this->get("tag.manager.request");

            $manager->submit($r);
            $this->addFlash('sonata_flash_success', sprintf('La demande %s a été soumise avec succès!',$r->getReference()));
            return $this->redirect($this->admin->generateUrl('show',[$this->admin->getIdParameter() => $id]));
        }

        $view = $form->createView();
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, "custom_form.html.twig");

        return $this->render('TagBundle:CRUD:submit.html.twig',[
            "form" => $view,
            'object' => $r,
            'action' => 'delete',
        ]);
    }

    public function validateAction(Request $request){
        $id = $request->get($this->admin->getIdParameter());
        /** @var \TagBundle\Entity\Request $r */
        $r = $this->admin->getObject($id);

        if (!$r) {
            throw new NotFoundHttpException(sprintf('unable to find the request tag with id : %s', $id));
        }

        if(!$this->admin->isGranted('VALIDATE',$r))
            throw $this->createAccessDeniedException("vous n'êtes pas authoriser à effectuer cette opération");
        /** @var \TagBundle\Manager\RequestManager $manager */
        $manager = $this->get("tag.manager.request");
        $a = $manager->checkTagsAvailability($r);

        if($a[RequestManager::REQUEST_AVAILABILITY] === RequestManager::AVAILABILITY_TAG_MISSING){
            unset($a[RequestManager::REQUEST_AVAILABILITY]);
            foreach ($a as $key => $count) {
                $this->addFlash('sonata_flash_error', sprintf("Impossible de valider la demande, il manque <b>%d %s</b>",$count,Tag::getTypes()[$key]));
            }
            return $this->redirect($this->admin->generateUrl('show',[$this->admin->getIdParameter() => $id]));
        }

        $url = $this->admin->generateObjectUrl('validate',$r);
        $form = $this
            ->createFormBuilder(array())
            ->setMethod(Request::METHOD_POST)
            ->setAction($url)
            ->getForm();
        ;
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $succeed = $manager->validate($r);
            if($succeed)
                $this->addFlash('sonata_flash_success', sprintf('La demande a été validée avec succès'));
            else
                $this->addFlash('sonata_flash_error', "Une erreur s'est produite lors de la validation de la demande");
            return $this->redirect($this->admin->generateUrl('show',[$this->admin->getIdParameter() => $id]));
        }

        return $this->render("SonataAdminBundle:CRUD:confirm_action.html.twig",array(
            "form" => $form->createView(),
            'action' => 'delete',
            'object' => $r,
            "message" => sprintf('Êtes-vous sûr de vouloir valider la demande "%s" ?',$r->__toString()),
        ));
    }
}
