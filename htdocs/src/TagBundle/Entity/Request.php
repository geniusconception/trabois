<?php

namespace TagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use InvoiceBundle\Model\InvoiceTemplate;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Request
 *
 * @ORM\Table(name="request")
 * @ORM\Entity(repositoryClass="TagBundle\Repository\RequestRepository")
 */
class Request implements InvoiceTemplate
{
    const STATUS_OPENED = 1;
    const STATUS_SUBMITTED = 2;
    const STATUS_TERMINATED = 3;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255,unique=true)
     */
    private $reference;

    /**
     *
     * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
     * 
     */
    private $exploitant;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * 
     */
    private $agent;

    /**
     * @var \Application\Sonata\UserBundle\Entity\User
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     * @ORM\JoinColumn(nullable=true)
     */
    private $user;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateCreate", type="datetime")
     */
    private $dateCreate;

    /**
     * @Assert\Valid(traverse = true)
     * @ORM\OneToMany(targetEntity="TagBundle\Entity\RequestItem",mappedBy="request",orphanRemoval=true,cascade={"persist","remove"})
     */ 
    private $items;

    /**
     *
     * @ORM\OneToMany(targetEntity="TagBundle\Entity\RequestFile",mappedBy="request",orphanRemoval=true,cascade={"persist","remove"})
     */
    private $files;

    /**
     * @var bool
     *
     * @ORM\Column(name="approved", type="boolean", options={"default":false})
     */
    private $approved;
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->items = new \Doctrine\Common\Collections\ArrayCollection();
        $this->files = new \Doctrine\Common\Collections\ArrayCollection();
        $this->dateCreate = new \DateTime();
        $this->approved = false;
        $this->status = self::STATUS_OPENED;
    }

    /**
     * Set dateCreate
     *
     * @param \DateTime $dateCreate
     * @return Request
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;

        return $this;
    }

    /**
     * Get dateCreate
     *
     * @return \DateTime 
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * Add items
     *
     * @param \TagBundle\Entity\RequestItem $items
     * @return Request
     */
    public function addItem(\TagBundle\Entity\RequestItem $items)
    {
        $this->items[] = $items;
        $items->setRequest($this);

        return $this;
    }

    /**
     * Remove items
     *
     * @param \TagBundle\Entity\RequestItem $items
     */
    public function removeItem(\TagBundle\Entity\RequestItem $items)
    {
        $this->items->removeElement($items);
        $items->setRequest(null);
    }

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param \Doctrine\Common\Collections\Collection|array $items
     */
    public function setItems($items){
        if(gettype($items) == 'array'){
            $items = new \Doctrine\Common\Collections\ArrayCollection($items);
        }
        /** @var RequestItem $i */
        foreach ($items as $i) {
            $i->setRequest($this);
        }

        $this->items = $items;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Request
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set status
     *
     * @param int $status
     * @return Request
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function __toString(){
        return ($this->getReference())? "Réf.: ".$this->getReference():"n/a";
    }

    

    /**
     * Set exploitant
     *
     * @param \CorporateBundle\Entity\Entreprise $exploitant
     * @return Request
     */
    public function setExploitant(\CorporateBundle\Entity\Entreprise $exploitant = null)
    {
        $this->exploitant = $exploitant;

        return $this;
    }

    /**
     * Get exploitant
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getExploitant()
    {
        return $this->exploitant;
    }

    /**
     * Set agent
     *
     * @param \Application\Sonata\UserBundle\Entity\User $agent
     * @return Request
     */
    public function setAgent(\Application\Sonata\UserBundle\Entity\User $agent = null)
    {
        $this->agent = $agent;

        return $this;
    }

    /**
     * Get agent
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Request
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return integer
     */
    public function getInvoiceId()
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getCurrency()
    {
        return "";
    }

    /**
     * @return string
     */
    public function getInvoicePrefix()
    {
        return "TAGRQ";
    }

    /**
     * @return \CorporateBundle\Entity\Entreprise
     */
    public function getTarget()
    {
        return $this->exploitant;
    }

    /**
     * @return boolean
     */
    public function isApproved()
    {
        return $this->approved;
    }

    /**
     * @param boolean $approved
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;
    }

    public static function getStatuses(){
        return [
            self::STATUS_OPENED => "Ouverte",
            self::STATUS_SUBMITTED => "soumise",
            self::STATUS_TERMINATED => "cloturée",
        ];
    }


    /**
     * Get approved
     *
     * @return boolean 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Add files
     *
     * @param \TagBundle\Entity\RequestFile $files
     * @return Request
     */
    public function addFile(\TagBundle\Entity\RequestFile $files)
    {
        $this->files[] = $files;
        $files->setRequest($this);

        return $this;
    }

    /**
     * Remove files
     *
     * @param \TagBundle\Entity\RequestFile $files
     */
    public function removeFile(\TagBundle\Entity\RequestFile $files)
    {
        $files->setRequest(null);
        $this->files->removeElement($files);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getFiles()
    {
        return $this->files;
    }

    public function setFiles($files){
        if(gettype($files) == 'array'){
            $files = new \Doctrine\Common\Collections\ArrayCollection($files);
        }
        /** @var RequestFile $f */
        foreach ($files as $f) {
            $f->setRequest($this);
        }

        $this->files = $files;
    }
}
