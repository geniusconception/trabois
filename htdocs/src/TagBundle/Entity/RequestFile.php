<?php

namespace TagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints as Assert;
/**
 * RequestFile
 *
 * @ORM\Table(name="request_file")
 * @ORM\Entity(repositoryClass="TagBundle\Repository\RequestFileRepository")
 * @Vich\Uploadable
 */
class RequestFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255)
     */
    private $filename;
    /**
     * @var File
     * @Vich\UploadableField(mapping="annotation_attach", fileNameProperty="filename")
     */
    private $file;

    /**
     * @var \TagBundle\Entity\Request
     * @Assert\NotNull()
     * @ORM\ManyToOne(targetEntity="TagBundle\Entity\Request",inversedBy="files")
     * @ORM\JoinColumn(nullable=true)
     */
    private $request;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;

    public function __construct(){
        $this->updatedAt = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return RequestFile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filename
     *
     * @param string $filename
     * @return RequestFile
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string 
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set request
     *
     * @param \TagBundle\Entity\Request $request
     * @return RequestFile
     */
    public function setRequest(\TagBundle\Entity\Request $request = null)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return \TagBundle\Entity\Request 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return File
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param File $file
     */
    public function setFile($file)
    {
        if($file){
            $this->updatedAt = new \DateTime('now');
        }
        $this->file = $file;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

}
