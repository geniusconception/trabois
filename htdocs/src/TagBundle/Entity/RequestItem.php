<?php

namespace TagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * RequestItem
 *
 * @UniqueEntity(
 *  fields={"request","tagType","refPCB"},
 *  message = "Vous ne pouvez pas répeter une même étiquette pour le même permis de coupe."
 * )
 * @ORM\Table(name="request_item",
 *  uniqueConstraints={
 *      @ORM\UniqueConstraint(name="request_type_pcb",columns={"request_id","tagType","refPCB"})
 * })
 * @ORM\Entity(repositoryClass="TagBundle\Repository\RequestItemRepository")
 */
class RequestItem
{
    const TAG_ABATTAGE = 1;//Tag abattage
    const TAG_TRONCONNAGE = 2;//Tag tronçonnage
    const TAG_TRANSFORMATION = 3;//Tag transformation

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="tagType", type="integer")
     */
    private $tagType;

    /**
     * @var string
     *
     * @ORM\Column(name="refPCB", type="string", length=255)
     */
    private $refPCB;

    /**
     * @var int
     *
     * @ORM\Column(name="number", type="integer")
     */
    private $number;

    /**
     * @var \TagBundle\Entity\Request
     *
     * @ORM\ManyToOne(targetEntity="TagBundle\Entity\Request",inversedBy="items")
     * @ORM\JoinColumn(nullable=true)
     */
    private $request;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tagType
     *
     * @param int $tagType
     * @return RequestItem
     */
    public function setTagType($tagType)
    {
        $this->tagType = $tagType;

        return $this;
    }

    /**
     * Get tagType
     *
     * @return int
     */
    public function getTagType()
    {
        return $this->tagType;
    }

    /**
     * Set refPCB
     *
     * @param string $refPCB
     * @return RequestItem
     */
    public function setRefPCB($refPCB)
    {
        $this->refPCB = $refPCB;

        return $this;
    }

    /**
     * Get refPCB
     *
     * @return string 
     */
    public function getRefPCB()
    {
        return $this->refPCB;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return RequestItem
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    public static function getAvailableStatus(){
        return array(
            self::TAG_ABATTAGE => "Abattage",
            self::TAG_TRONCONNAGE => "Tronçonnage",
            self::TAG_TRANSFORMATION => "Transformation"
        );
    }

    function __toString()
    {
        return ($this->getId())? sprintf("%d étiquettes - %s",$this->number,self::getAvailableStatus()[$this->tagType]) : "n/a";
    }


    /**
     * Set request
     *
     * @param \TagBundle\Entity\Request|null $request
     * @return RequestItem
     */
    public function setRequest(\TagBundle\Entity\Request $request = null)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return \TagBundle\Entity\Request 
     */
    public function getRequest()
    {
        return $this->request;
    }
}
