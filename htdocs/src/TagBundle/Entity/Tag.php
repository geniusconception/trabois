<?php

namespace TagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Tag
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="TagBundle\Repository\TagRepository")
 */
class Tag
{
    const TYPE_PRESYSTEM = 0;
    const TYPE_ABATTAGE = 1;
    const TYPE_TRONCONNAGE = 2;
    const TYPE_SAISIE = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
    * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
    * 
    */
     private $exploitant;

    /**
    * @ORM\ManyToOne(targetEntity="TagBundle\Entity\Target", cascade={"all"})
    * 
    */
    private $target;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    private $code;

    /**
     * @var bool
     *
     * @ORM\Column(name="used", type="boolean", options={"default":false})
     */
    private $used;

    /**
     *
     * @ORM\ManyToOne(targetEntity="Application\Sonata\UserBundle\Entity\User")
     */
    private $user;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime",name="date",nullable=true)
     */
    private $date;
    /**
     * @var integer
     *
     * @ORM\Column(type="integer",name="type")
     */
    private $type;

    public function __construct(){
        $this->used = false;
        $this->date = new \DateTime();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exploitant
     *
     * @param string $exploitant
     * @return Tag
     */
    public function setExploitant($exploitant)
    {
        $this->exploitant = $exploitant;

        return $this;
    }

    /**
     * Get exploitant
     *
     * @return string 
     */
    public function getExploitant()
    {
        return $this->exploitant;
    }

    /**
     * Set target
     *
     * @param \TagBundle\Entity\Target $target
     * @return Tag
     */
    public function setTarget(\TagBundle\Entity\Target $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \TagBundle\Entity\Target 
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return Tag
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string 
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set user
     *
     * @param \Application\Sonata\UserBundle\Entity\User $user
     * @return Tag
     */
    public function setUser(\Application\Sonata\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Application\Sonata\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set used
     *
     * @param boolean $used
     * @return Tag
     */
    public function setUsed($used)
    {
        $this->used = $used;

        return $this;
    }

    /**
     * Get used
     *
     * @return boolean 
     */
    public function isUsed()
    {
        return $this->used;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Tag
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param int $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    public static function getTypes(){
        return array(
            self::TYPE_ABATTAGE => "Etiquettes d'abbatage",
            self::TYPE_PRESYSTEM => "Etiquettes pré-système",
            self::TYPE_TRONCONNAGE => "Etiquettes de tronçonnage",
            self::TYPE_SAISIE => "Etiquettes de saisie",
        );
    }

    function __toString()
    {
        return ($this->getId())? sprintf("[%s] %s",self::getTypes()[$this->type],$this->code) : "n-a";
    }



    /**
     * Get used
     *
     * @return boolean 
     */
    public function getUsed()
    {
        return $this->used;
    }

}
