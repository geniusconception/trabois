<?php

namespace TagBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Target
 *
 * @ORM\Table(name="target",indexes={
 *  @ORM\Index(name="ref_index",columns={"reference"}),
 *  @ORM\Index(name="model_index",columns={"model"}),
 *  @ORM\Index(name="ref_identifier",columns={"identifier"})
 * })
 * @ORM\Entity(repositoryClass="TagBundle\Repository\TargetRepository")
 */
class Target
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string", length=255)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="information", type="string", length=255)
     */
    private $information;

    /**
     * @var string
     *
     * @ORM\Column(name="reference", type="string", length=255)
     */
    private $reference;
    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=255)
     */
    private $owner;
    /**
     * @var string
     *
     * @ORM\Column(name="essence", type="string", length=255)
     */
    private $essence;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Target
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return Target
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string 
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @return string
     */
    public function getInformation()
    {
        return $this->information;
    }

    /**
     * @param string $information
     */
    public function setInformation($information)
    {
        $this->information = $information;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Target
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set owner
     *
     * @param string $owner
     * @return Target
     */
    public function setOwner($owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @return string
     */
    public function getEssence()
    {
        return $this->essence;
    }

    /**
     * @param string $essence
     */
    public function setEssence($essence)
    {
        $this->essence = $essence;
    }

    public function __toString(){
        if ($this->getId()){
            $info = unserialize($this->getInformation());
            $name = current(array_keys($info));
            $info = current(array_values($info));
            return sprintf("%s/%s/%s %s ",$this->getOwner(),$this->getReference(),$name,$info);
        }
        return "n/a";
    }
}
