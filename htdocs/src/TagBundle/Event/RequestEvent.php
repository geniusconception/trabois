<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 21/08/2017
 * Time: 15:12
 */

namespace TagBundle\Event;


use Symfony\Component\EventDispatcher\Event;
use TagBundle\Entity\Request;

class RequestEvent extends Event {

    /**
     * @var Request
     */
    private $request;

    function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }


} 