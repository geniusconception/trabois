<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 21/08/2017
 * Time: 15:10
 */

namespace TagBundle\Event;


class TagEvents {

    const REQUEST_APPROVED = "tag.request.approved";
    const REQUEST_SUBMITTED = "tag.request.submitted";
    const REQUEST_TERMINATED = "tag.request.terminated";

}