<?php

namespace TagBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class RequestType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('files',"collection",[
                'entry_type' => 'TagBundle\Form\RequestFileType' ,
                'allow_add' => true,
                'allow_delete' => true,
                'label' => 'Preuves de paiement & autres pièces-jointe',
                'by_reference' => false,
                'constraints' => [
                    new Assert\Valid(),
                ]
            ])
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TagBundle\Entity\Request'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'tagbundle_request';
    }


}
