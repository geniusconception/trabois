<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 21/08/2017
 * Time: 15:08
 */

namespace TagBundle\Manager;


use CorporateBundle\Entity\Entreprise;
use Doctrine\ORM\EntityManagerInterface;
use InvoiceBundle\Manager\InvoiceManager;
use TagBundle\Entity\Request;
use TagBundle\Event\RequestEvent;
use TagBundle\Event\TagEvents;

class RequestManager {

    const AVAILABILITY_TAG_AVAILABLE = 1;
    const AVAILABILITY_TAG_MISSING = 2;
    const REQUEST_AVAILABILITY = "availability";

    private $dispatcher;
    /**
     * @var InvoiceManager
     */
    private $invoiceManager;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    function __construct($dispatcher, InvoiceManager $invoiceManager, EntityManagerInterface $em)
    {
        $this->dispatcher = $dispatcher;
        $this->invoiceManager = $invoiceManager;
        $this->em = $em;
    }

    /**
     * @param Request $request
     * @return \InvoiceBundle\Entity\Invoice
     */
    public function approve(Request $request){
        $request->setApproved(true);
        $this->em->persist($request);
        $this->em->flush();

        $invoice = $this->invoiceManager->create($request);
        $this->dispatcher->dispatch(TagEvents::REQUEST_APPROVED,new RequestEvent($request));
        return $invoice;
    }

    public function submit(Request $request){
        $request->setStatus(Request::STATUS_SUBMITTED);
        $this->em->persist($request);
        $this->em->flush();
        $this->dispatcher->dispatch(TagEvents::REQUEST_SUBMITTED,new RequestEvent($request));
    }

    public function terminate(Request $request){
        $request->setStatus(Request::STATUS_TERMINATED);
        $this->em->persist($request);
        $this->em->flush();
        $this->dispatcher->dispatch(TagEvents::REQUEST_TERMINATED,new RequestEvent($request));
    }

    public function checkTagsAvailability(Request $request){
        $availibility = [];
        #[self::REQUEST_AVAILABILITY => self::AVAILABILITY_TAG_MISSING];
        /** @var \TagBundle\Entity\RequestItem $items */
        foreach ($request->getItems() as $items) {
            $count = $this->em
                ->createQuery("SELECT COUNT(t) FROM TagBundle:Tag t WHERE t.exploitant is NULL AND t.type = :type ")
                ->setParameter("type",$items->getTagType())
                ->getSingleScalarResult ()
            ;
            if($count < $items->getNumber()){
                $availibility[$items->getTagType()] = $items->getNumber() - $count;
            }
        }
        $availibility[self::REQUEST_AVAILABILITY] = (count($availibility) == 0)? self::AVAILABILITY_TAG_AVAILABLE: self::AVAILABILITY_TAG_MISSING;
        return $availibility;
    }

    public function validate(Request $request){
        $availibility = $this->checkTagsAvailability($request);
        if($availibility[self::REQUEST_AVAILABILITY] === self::AVAILABILITY_TAG_MISSING)
            return false;
        /** @var \TagBundle\Entity\RequestItem $items */
        foreach ($request->getItems() as $items) {
            $this->attributeTagSet($items->getNumber(),$items->getTagType(),$request->getExploitant());
        }
        $this->terminate($request);
        return true;
    }

    private function attributeTagSet($number, $type, Entreprise $to){
        $set = $this->em
            ->createQuery("SELECT t FROM TagBundle:Tag t WHERE t.exploitant is NULL AND t.type = :type")
            ->setParameter("type",$type)
            ->setMaxResults($number)
            ->getResult();
            ;
        /** @var \TagBundle\Entity\Tag $tag */
        foreach ($set as $tag) {
            $tag->setExploitant($to);
            $this->em->persist($tag);
        }
        $this->em->flush();
    }
} 