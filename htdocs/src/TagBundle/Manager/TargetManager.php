<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 29/06/2017
 * Time: 13:42
 */

namespace TagBundle\Manager;


use Doctrine\ORM\EntityManagerInterface;
use TagBundle\Entity\Target;
use TagBundle\Model\Taggable;

class TargetManager {
    /**
     * @var EntityManagerInterface
     */
    private $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Taggable $taggable
     * @return Target
     */
    public function handle(Taggable $taggable){;
        /** @var \TagBundle\Entity\Target $target */
        $target = new Target();

        $target->setIdentifier(serialize($taggable->getIdentifier()));
        $target->setInformation(serialize($taggable->getInformation()));
        $target->setReference($taggable->getTheReference());
        $target->setModel($taggable->getTagClass());
        $target->setOwner($taggable->getOperating());
        $target->setEssence($taggable->getTheEssence());
        /** @var \TagBundle\Entity\Tag $tag */
        $tag = $this->em->getRepository("TagBundle:Tag")->findOneBy(["code" => $taggable->getTag()]);
        if(!$tag)
            return null;
        $tag->setTarget($target);
        $tag->setUsed(true);

        $this->em->persist($tag);
        $this->em->flush();
        return $target;
    }

} 