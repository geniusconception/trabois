<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 29/06/2017
 * Time: 13:38
 */

namespace TagBundle\Model;


interface Taggable {
    /**
     * @return array
     */
    public function getIdentifier();

    /**
     * @return string
     */
    public function getInformation();

    /**
     * @return string
     */
    public function getTagClass();

    /**
     * @return string
     */
    public function getTheReference();

    /**
     * @return string
     */
    public function getOperating();

    /**
     * @return string
     */
    public function getTheEssence();

    /**
     * @return string
     */
    public function getTag();
} 