<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 21/08/2017
 * Time: 17:09
 */

namespace TagBundle\Security\Authorization\Voter;

use AppBundle\Manager\AffectionManager;
use AppBundle\Entity\Affectation;
use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Permission\PermissionMapInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use TagBundle\Entity\Request;

class RequestVoter extends AclVoter{

    /**
     * @var AffectionManager
     */
    private $affec;

    public function __construct(AffectionManager $affec,AclProviderInterface $aclProvider, ObjectIdentityRetrievalStrategyInterface $oidRetrievalStrategy, SecurityIdentityRetrievalStrategyInterface $sidRetrievalStrategy, PermissionMapInterface $permissionMap, LoggerInterface $logger = null, $allowIfObjectIdentityUnavailable = true)
    {
        parent::__construct($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap, $logger, $allowIfObjectIdentityUnavailable);
        $this->affec = $affec;
    }

    public function supportsClass($class)
    {
        return in_array($class,array('TagBundle\Entity\Request'));
    }

    public function supportsAttribute($attribute)
    {
        return in_array($attribute,array("VIEW",'APPROVE',"SUBMIT","EDIT","DELETE","VALIDATE"));
    }

    public function vote(TokenInterface $token, $object, array $attributes){
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }

        $isExploitant = $this->affec->isExploitant();

        foreach ($attributes as $attribute){
            if ($this->supportsAttribute($attribute)){
                /** @var \TagBundle\Entity\Request $object */
                if($attribute !== "VIEW" && $object->getStatus() === Request::STATUS_TERMINATED)
                    return self::ACCESS_DENIED;

                $a = $this->affec->getAffectation();

                if($isExploitant && ($object->getExploitant()->getId() !== $a->getExploitant()->getId()))
                    return self::ACCESS_DENIED;

                if($attribute == "VALIDATE"){
                    if($isExploitant)
                        return self::ACCESS_DENIED;
                    else
                        return ($object->getStatus() === Request::STATUS_SUBMITTED)? self::ACCESS_GRANTED: self::ACCESS_DENIED;
                }

                if($attribute == 'APPROVE')
                    return ($isExploitant && ($a->getPosition() === Affectation::POSITION_DIRECTOR) && !$object->isApproved())? self::ACCESS_GRANTED: self::ACCESS_DENIED;

                if($attribute == 'SUBMIT')
                    return ($isExploitant && $object->isApproved() && ($object->getStatus() === Request::STATUS_OPENED))? self::ACCESS_GRANTED: self::ACCESS_DENIED;
                if(in_array($attribute,["EDIT","DELETE"])){
                    if($isExploitant)
                        return ($object->isApproved())? self::ACCESS_DENIED:self::ACCESS_GRANTED;
                    else
                        self::ACCESS_DENIED;
                }
            }
        }

        return self::ACCESS_ABSTAIN;
    }
}