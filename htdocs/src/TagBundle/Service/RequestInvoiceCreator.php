<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 19/08/2017
 * Time: 18:13
 */

namespace TagBundle\Service;

use AppBundle\Model\Configurator;
use InvoiceBundle\Entity\Invoice;
use InvoiceBundle\Entity\InvoiceItem;
use InvoiceBundle\Model\InvoiceCreator;
use InvoiceBundle\Model\InvoiceTemplate;
use TagBundle\Entity\RequestItem;
use TagBundle\Entity\Request;

class RequestInvoiceCreator implements InvoiceCreator{

    /**
     * @var Configurator
     */
    private $config;

    function __construct(Configurator $config)
    {
        $this->config = $config;
    }

    /**
     * @param InvoiceTemplate $template
     * @return Invoice
     */
    public function create(InvoiceTemplate $template)
    {
        /** @var Request $rq */
        $rq = $template;
        $invoice = new Invoice();

        $invoice->setTransmitter($this->config->get('master'));
        $invoice->setTarget($rq->getTarget());
        $invoice->setCurrency($this->config->get('currency'));
        /** @var RequestItem $rqi */
        foreach ($rq->getItems() as $rqi) {
            $item = $this->getItem($rqi);
            $invoice->addItem($item);
        }

        return $invoice;
    }

    /**
     * @param RequestItem $r
     * @return InvoiceItem
     */
    private function getItem(RequestItem $r){
        $item = new InvoiceItem();
        $label = "étiquettes ";

        switch($r->getTagType()){
            case RequestItem::TAG_ABATTAGE:
                $amount = $this->config->get("abattage_tag_amount");
                $label .= "d'Abattage";
                break;
            case RequestItem::TAG_TRONCONNAGE:
                $amount = $this->config->get("tronconnage_tag_amount");
                $label .= "de Tronçonnage";
                break;
            case RequestItem::TAG_TRANSFORMATION:
                $amount = $this->config->get("transform_tag_amount");
                $label .= "de Transformation";
                break;
            default:
                $amount = 0.0;
                break;
        }

        $item->setQte($r->getNumber());
        $item->setLibelle($label);
        $item->setPu($amount);
        $item->setSku("étiquettes");

        return $item;
    }
} 