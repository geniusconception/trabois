<?php

namespace TrajetBundle\Controller;

use AppBundle\Controller\CoreController as Core;
use Symfony\Component\HttpFoundation\Request;

class CoreController extends Core
{
    public function searchAction(Request $request){
        $trace  = trim($request->query->get("trace"));
        $params = [];
        if($trace != ""){
            /** @var \TrajetBundle\Model\CheckpointManager $cm */
            $cm = $this->get("trajet.trajet_manager");
            if($cm->traceExist($trace))
                return $this->redirect($this->generateUrl("trajet_track",array("trace" => $trace)));
            else
                $params['title'] = sprintf('Le dossier avec comme référence "%s" est introuvable sur le système',$trace);
        }
        return $this->render("TrajetBundle:Core:search.html.twig",$params);
    }

    public function trackAction($trace, $circuit = null)
    {
        $steps = $this->getFomattedSteps($trace,$circuit);
        if(is_null($steps)){
            throw $this->createNotFoundException("Le dossier spécifié n'existe pas");
        }
        return $this->render('TrajetBundle:Core:tracker.html.twig',array(
            "trace" => $trace,
            "steps" => $steps,
        ));
    }

    private function getFomattedSteps($trace,$circuit){
        /** @var \TrajetBundle\Model\CheckpointManager $cm */
        $cm = $this->get("trajet.trajet_manager");
        $steps = $cm->findTrace($trace,array(
            "context" => "view",
            "order" => "ASC",
        ));

        if(($count = count($steps)) <= 0){
            return null;
        }
        if($circuit === null){
            /** @var \TrajetBundle\Entity\Step $s */
            $s = $steps[0];
            $circuit = $s->getTrajet();
        }

        $circuits = $this->getParameter("trajet.circuits");
        $circuits = array_values($circuits[$circuit]);

        $i = 1;
        return array_map(function($step,$header) use ($count,&$i){
            if($i == $count)
                $mod = "current";
            elseif($i < $count)
                $mod = "success";
            else
                $mod = "empty";
            $i++;

            return array(
                "header" =>$header["title"],
                "step" => $step,
                "modifier" => $mod,
            );
        },$steps,$circuits);
    }
}
