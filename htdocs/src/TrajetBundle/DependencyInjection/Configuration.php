<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 16/06/2017
 * Time: 16:06
 */

namespace TrajetBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface{

    /**
     * Generates the configuration tree builder.
     *
     * @return \Symfony\Component\Config\Definition\Builder\TreeBuilder The tree builder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('trajet');

        $rootNode
            ->children()
                ->arrayNode('checkpoints')
                    ->useAttributeAsKey('name')
                    ->prototype('scalar')
                        ->isRequired()->cannotBeEmpty()
                    ->end()
                ->end()
                ->arrayNode('filters')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->prototype('scalar')->end()
                    ->end()
                ->end()
                ->arrayNode('circuits')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->useAttributeAsKey('node')
                        ->prototype('array')
                            ->children()
                                ->scalarNode('title')->isRequired()->cannotBeEmpty()->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('templates')
                    ->useAttributeAsKey('checkpoint')
                    ->prototype('scalar')
                        ->isRequired()->cannotBeEmpty()
                    ->end()
                ->end()
                ->arrayNode('matches')
                    ->useAttributeAsKey('circuit')
                    ->prototype('scalar')
                        ->isRequired()->cannotBeEmpty()
                    ->end()
                ->end()
            ->end();

        return $treeBuilder;
    }


} 