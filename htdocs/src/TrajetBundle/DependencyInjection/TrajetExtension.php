<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 16/06/2017
 * Time: 20:56
 */

namespace TrajetBundle\DependencyInjection;


use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;
use Symfony\Component\Config\FileLocator;

class TrajetExtension extends Extension
{
    /**
     * Loads a specific configuration.
     *
     * @param array $configs An array of configuration values
     * @param ContainerBuilder $container A ContainerBuilder instance
     *
     * @throws \InvalidArgumentException When provided tag is not defined in this extension
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter("trajet.checkpoints", $config['checkpoints']);
        $this->circuitConfiguration($container,$config['circuits'],$config['matches']);
        $container->setParameter("trajet.templates", $config['templates']);

        $tm = $container->findDefinition("trajet.trajet_manager");

        $filters = $config['filters'];
        foreach ($filters as $name => $contexts) {
            $f = $container->findDefinition($name);
            $f->addMethodCall("setContexts",array($contexts));
            $tm->addMethodCall("addFilter",array(new Reference($name)));
        }

    }

    private function circuitConfiguration(ContainerBuilder $container,$circuits,$matches){
        $trajet = array();
        foreach ($matches as $name => $circuit) {
            $trajet[$name] = $circuits[$circuit];
        }

        $container->setParameter("trajet.circuits", $trajet);
    }

}