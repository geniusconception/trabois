<?php

namespace TrajetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Node
 *
 * @ORM\Table(name="node",indexes={@ORM\Index(name="search_idx", columns={"model", "identifier"})})
 * @ORM\Entity(repositoryClass="TrajetBundle\Repository\NodeRepository")
 */
class Node
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="identifier", type="string",length=255)
     */
    private $identifier;
    /**
     * @var boolean
     *
     * @ORM\Column(name="isSnapshot", type="boolean")
     */
    private $snapshot;
    /**
     * @var array
     *
     * @ORM\Column(name="capturedFields", type="array")
     */
    private $capturedFields;
    /**
     * @var object
     */
    private $data;

    public function __construct(){
        $this->data = null;
        $this->identifier = array();
        $this->snapshot = false;
        $this->capturedFields = array();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set model
     *
     * @param string $model
     * @return Node
     */
    public function setModel($model)
    {
        $this->model = $model;

        return $this;
    }

    /**
     * Get model
     *
     * @return string 
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set identifier
     *
     * @param string $identifier
     * @return Node
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return string
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @return object
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param object $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }


    /**
     * Set isSnapshot
     *
     * @param boolean $snapshot
     * @return Node
     */
    public function setSnapshot($snapshot)
    {
        $this->snapshot = $snapshot;

        return $this;
    }

    /**
     * Get isSnapshot
     *
     * @return boolean 
     */
    public function isSnapshot()
    {
        return $this->snapshot;
    }

    /**
     * Set capturedFields
     *
     * @param array $capturedFields
     * @return Node
     */
    public function setCapturedFields($capturedFields)
    {
        $this->capturedFields = $capturedFields;

        return $this;
    }

    /**
     * Get capturedFields
     *
     * @return array 
     */
    public function getCapturedFields()
    {
        return $this->capturedFields;
    }
}
