<?php

namespace TrajetBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Step
 *
 * @ORM\Table(name="step",uniqueConstraints={@ORM\UniqueConstraint(name="node_trace",columns={"node_id","trace"})})
 * @ORM\Entity(repositoryClass="TrajetBundle\Repository\StepRepository")
 */
class Step
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="trace", type="string",length=120)
     */
    private $trace;

    /**
     * @var string
     *
     * @ORM\Column(name="trajet", type="string", length=120)
     */
    private $trajet;

    /**
     *
     * @ORM\ManyToOne(targetEntity="TrajetBundle\Entity\Node",cascade={"persist"})
     */
    private $node;

    public function __construct(){
        $this->date = new \DateTime();
        $this->trajet = null;
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set trajet
     *
     * @param string $trajet
     * @return Step
     */
    public function setTrajet($trajet)
    {
        $this->trajet = $trajet;

        return $this;
    }

    /**
     * Get trajet
     *
     * @return string 
     */
    public function getTrajet()
    {
        return $this->trajet;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Step
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set trace
     *
     * @param string $trace
     * @return Step
     */
    public function setTrace($trace)
    {
        $this->trace = $trace;

        return $this;
    }

    /**
     * Get trace
     *
     * @return string 
     */
    public function getTrace()
    {
        return $this->trace;
    }

    /**
     * Set node
     *
     * @param \TrajetBundle\Entity\Node $node
     * @return Step
     */
    public function setNode(\TrajetBundle\Entity\Node $node = null)
    {
        $this->node = $node;

        return $this;
    }

    /**
     * Get node
     *
     * @return \TrajetBundle\Entity\Node 
     */
    public function getNode()
    {
        return $this->node;
    }
}
