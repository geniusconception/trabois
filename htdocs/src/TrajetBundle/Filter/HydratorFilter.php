<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 16/06/2017
 * Time: 15:21
 */

namespace TrajetBundle\Filter;


use Doctrine\ORM\EntityManagerInterface;
use TrajetBundle\Model\Filter;

class HydratorFilter extends Filter{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    function __construct(EntityManagerInterface $em, $contexts = array())
    {
        parent::__construct($contexts);
        $this->em = $em;
    }


    /**
     * @param \Traversable|array $list
     * @return \Traversable
     */
    public function filter($list)
    {
        $filtered = array();
        /**
         * @var  $key
         * @var \TrajetBundle\Entity\Step $s
         */
        foreach ($list as $key => $s) {
            $node = $s->getNode();
            $data = $this->em->getRepository($node->getModel())->findOneBy((array) unserialize($node->getIdentifier()));
            $node->setData($data);
            $s->setNode($node);

            $filtered[$key] = $s;
        }

        return $filtered;
    }


} 