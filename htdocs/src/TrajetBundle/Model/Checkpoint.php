<?php
namespace TrajetBundle\Model;	

	interface Checkpoint
	{
        /**
         * @return string
         */
		public function getName();

        /**
         * @return boolean
         */
		public function isLocked();

        /**
         * @param boolean $lock
         */
		public function setLocked($lock);

        /**
         * @return array
         */
		public function getIdentifier();
 	}
