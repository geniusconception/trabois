<?php
namespace TrajetBundle\Model;

use Doctrine\ORM\EntityManagerInterface;
use TrajetBundle\Entity\Node;
use TrajetBundle\Entity\Step;

class CheckpointManager
{

    /**
     * @var array
     */
    private $filters;
    /**
     * @var array
     */
    private $checkpoints;
    /**
     * @var array
     */
    private $trajets;
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var array
     */
    private $templates;

    public function __construct (EntityManagerInterface $em,$cp=array(),$trajets = array(), $templates = array())
    {
        $this->em = $em;
        $this->checkpoints = $cp;
        $this->trajets = $trajets;
        $this->templates = $templates;
    }

    /**
     * @param Checkpoint $cp
     * @param $trace
     * @param null|string $trajet
     * @return Checkpoint
     */
    public function addStep(Checkpoint $cp,$trace, $trajet)
    {
        $s = new Step();
        $s->setTrace($trace);
        $s->setTrajet($trajet);

        $node = $this->findOrCreateNode($cp);
        $s->setNode($node);
        $this->em->persist($s);
        $this->em->flush();

        return $cp;
    }

    /**
     * @param $trace
     * @param array $criteria
     * @return array|\Traversable
     */
    public function findTrace($trace, $criteria = array())
    {
        $context = (isset($criteria['context']))? $criteria['context']: null;
        $order = (isset($criteria['order']))? $criteria['order']: "DESC";
        $q = $this
            ->em->createQueryBuilder()
            ->from("TrajetBundle:Step","s")
            ->select("s")
            ->leftJoin("s.node","n")
            ->addSelect("n")
            ->orderBy("s.date",$order)
            ->where("s.trace = :trace")
            ->setParameter("trace",$trace)
            ->getQuery()
        ;
        $list = $q->getResult();
        /** @var FilterInterface $filter */
        foreach ($this->filters as $filter) {
            if(is_null($context) || $filter->hasContext($context)){
                $list = $filter->filter($list);
            }
        }

        return $list;
    }

    public function lock(Checkpoint $cp){
        $cp->setLocked(true);
        $this->em->persist($cp);
        $this->em->flush();
        return $cp;
    }

    public function addFilter(FilterInterface $filter){
        $this->filters[] = $filter;
    }

    public function findOrCreateNode(Checkpoint $cp){
        if($cp instanceof Snapshot)
            return $this->capture($cp);
        if(!isset($this->checkpoints[$cp->getName()]))
            throw new \InvalidArgumentException(sprintf('Unknown Checkpoint named "%s" in configuration',$cp->getName()));
        $model = $this->checkpoints[$cp->getName()];
        /** @var Node $node */
        $node = $this->em->getRepository("TrajetBundle:Node")->findOneBy(array(
            "model" => $model,
            "identifier" => serialize($cp->getIdentifier())
        ));
        if($node){
            $node->setData($cp);
        }else{
            $node = new Node();
            $node->setModel($model);
            $node->setIdentifier(serialize($cp->getIdentifier()));
            $node->setData($cp);
        }
        return $node;
    }

    /**
     * @param string|Node $node
     * @return string
     */
    public function getTemplate(Node $node){
        if(is_object($node)){
            $node = $node->getModel();
            $node = array_search($node,$this->checkpoints);
        }
        if(isset($this->templates[$node]))
            return $this->templates[$node];
        else
            return "TrajetBundle:Node:default.html.twig";
    }

    public function traceExist($trace){
        $r = $this->em->createQuery("SELECT s FROM TrajetBundle:Step s WHERE s.trace = :trace")->setParameter("trace",$trace)->getResult();
        return (count($r) > 0);
    }

    public function retreiveTrace(Checkpoint $cp,$trajet){
        $node = $this->findOrCreateNode($cp);
        $r = $this->em
            ->createQuery("SELECT s FROM TrajetBundle:Step s LEFT JOIN s.node n WHERE n.model = :model AND n.identifier = :id AND s.trajet = :trajet ORDER BY s.date DESC")
            ->setParameter("model",$node->getModel())
            ->setParameter('id',$node->getIdentifier())
            ->setParameter('trajet',$trajet)
            ->getResult()
        ;
        if(count($r) > 0){
            /** @var Step $step */
            $step = $r[0];
            return $step->getTrace();
        }
        return null;
    }

    public function capture(Snapshot $snapshot){
        if(!isset($this->checkpoints[$snapshot->getName()]))
            throw new \InvalidArgumentException(sprintf('Unknown Checkpoint named "%s" in configuration',$snapshot->getName()));
        $model = $this->checkpoints[$snapshot->getName()];
        $node = new Node();
        $node->setModel($model);
        $node->setIdentifier(serialize($snapshot->getIdentifier()));
        $node->setData($snapshot);
        $node->setSnapshot(true);

        $captured = [];
        foreach($snapshot->configureCaptureFields() as $attribute){
            $suffix = $this->methodSuffix($attribute);
            $method = "get".$suffix;
            $bMethod = "is".$suffix;
            $value = null;
            if(method_exists($snapshot,$method))
                $value = $snapshot->$method();
            elseif(method_exists($snapshot,$bMethod))
                $value = $snapshot->$bMethod();
            $captured[$attribute] = $value;
        }
        $node->setCapturedFields($captured);

        return $node;
    }

    private function methodSuffix($property){
        $property = str_replace("_"," ",$property);
        return str_replace(" ","",ucwords($property));
    }

    /**
     * @param string $circuit
     * @return bool
     */
    public function circuitExist($circuit){
        return isset($this->trajets[$circuit]);
    }

    /**
     * @param $circuit
     * @param string $current
     * @param bool $reversed
     * @return string|null
     */
    public function guessNextStep($circuit, $current = null, $reversed = false){
        if(!$this->circuitExist($circuit))
            throw new \InvalidArgumentException(sprintf("circuit %s does not exist",$circuit));
        $circuit = array_keys($this->trajets[$circuit]);
        $circuit = ($reversed)? array_reverse($circuit):$circuit;
        if(is_null($current))
            return array_shift($circuit);
        if(!in_array($current,$circuit))
            return null;
        $found = false;
        foreach($circuit as $step){
            if($found)
                return $step;
            if($step == $current)
                $found = true;
        }
        return null;
    }
}