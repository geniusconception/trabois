<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 16/06/2017
 * Time: 13:06
 */

namespace TrajetBundle\Model;


abstract class Filter implements FilterInterface{

    /**
     * @var array
     */
    protected $contexts;

    function __construct($contexts = array())
    {
        $this->contexts = $contexts;
    }


    /**
     * @param string $context
     */
    public function addContext($context)
    {
        if(! in_array($context,$this->contexts)){
            $this->contexts[] = $context;
        }
    }

    /**
     * @param string $context
     * @return boolean
     */
    public function hasContext($context)
    {
        return in_array($context,$this->contexts);
    }

    /**
     * @param array $context
     */
    public function setContexts(array $context)
    {
        $this->contexts = $context;
    }


} 