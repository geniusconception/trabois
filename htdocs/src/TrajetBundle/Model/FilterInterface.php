<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 16/06/2017
 * Time: 12:59
 */

namespace TrajetBundle\Model;


interface FilterInterface {

    /**
     * @param \Traversable|array $list
     * @return \Traversable
     */
    public function filter($list);

    /**
     * @param string $context
     */
    public function addContext($context);

    /**
     * @param string $context
     * @return boolean
     */
    public function hasContext($context);

    /**
     * @param array $context
     */
    public function setContexts(array $context);
} 