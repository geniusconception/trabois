<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 05/07/2017
 * Time: 01:59
 */

namespace TrajetBundle\Model;


interface Snapshot extends Checkpoint{

    /**
     * @return array
     */
    public function configureCaptureFields();
} 