<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 16/06/2017
 * Time: 23:27
 */

namespace TrajetBundle\Twig;


use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use TrajetBundle\Entity\Node;
use TrajetBundle\Model\CheckpointManager;

class TrajetExtension extends \Twig_Extension{

    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var \Twig_Environment
     */
    private $twig;
    /**
     * @var CheckpointManager
     */
    private $cm;

    function __construct(CheckpointManager $cm , \Twig_Environment $twig, RouterInterface $router)
    {
        $this->router = $router;
        $this->twig = $twig;
        $this->cm = $cm;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction("trajet_node",array($this,"renderNode"), array('is_safe' => array('html'))),
            new \Twig_SimpleFunction("tracker_path",array($this,"generateTracerUrl")),
        );
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter("toText",array($this,"toText")),
        );
    }

    /**
     * @param Node $node
     * @return string
     */
    public function renderNode(Node $node){
        return $this->twig->render($this->cm->getTemplate($node),array(
            "value" => $node->getData(),
        ));
    }

    /**
     * @param $trace
     * @param null $circuit
     * @return string
     */
    public function generateTracerUrl($trace,$circuit = null){
        return $this->router->generate('trajet_track',array(
            "trace" => $trace,
            "circuit" => $circuit,
        ), UrlGeneratorInterface::ABSOLUTE_PATH);
    }

    /**
     * @param object $object
     * @return string
     */
    public function toText($object){
        if(method_exists($object,"__toString")){
            return $object->__toString();
        }else
            return serialize($object);
    }
} 