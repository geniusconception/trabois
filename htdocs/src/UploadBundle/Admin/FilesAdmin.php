<?php

namespace UploadBundle\Admin;

use CorporateBundle\Model\ExploitantFiltred;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use AppBundle\Manager\AffectionManager;
use Sonata\AdminBundle\Route\RouteCollection;
use UploadBundle\Entity\Files;

class FilesAdmin extends AbstractAdmin implements ExploitantFiltred
{
    /**
     * @var array
     */
    protected $fileType;
    /**
     * @var AffectionManager
     */
    private $affm;
    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('titre')
            ->add('date')
        ;
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('titre',null,array('label'=>"Titre du fichier"))
            ->add('date',null,array('label'=>"date"))
            ->add('authenticated',null,array('label'=>"Authentifié ?"))
            ->add('_action', null, array(
                'actions' => array(
                    'show' => array(),
                    'edit' => array(),
                    'delete' => array(),
                    'link' => [
                        'template' => 'UploadBundle:CRUD:list__action_link.html.twig'
                    ]
                )
            ))
        ;
    }

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('titre','text',array('label'=>"Titre du fichier"))
            ;
        if(($subject = $this->getSubject()) && (! $subject->getId()))
            $formMapper->add('type','choice',array('label' => "Type",'choices' => $this->fileType));
        $formMapper
            ->add('comment','textarea',array('label'=>"Commentaire",'required'=>false))
            ->add('imageFile','file',array('label'=>"Attaché un fichier",'required' => false))
        ;
    }

    /**
     * @param ShowMapper $showMapper
     */
    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('titre')
            ->add('date')
            ->add('name',null,[
                'template' => "UploadBundle:File:link.html.twig"
            ])
            ->add('authenticated',null,array('label'=>"Authentifié ?"))
            ->add('valid','boolean',['label' => "valide ?"])
            ->add('validity','choice',[
                'label' => "Période de validité ?",
                'choices' => Files::getValidityList()
            ])
            ->add('validityStart','datetime',['label' => "Début de validité",'format' => 'd-M-Y'])
            ->add('validityEnd','datetime',['label' => "Fin de validité",'format' => 'd-M-Y'])
            ->add('comment',null,array('label'=>"Commentaire"))
        ;
    }

    /**
     * @param AffectionManager $affm
     */
    public function setAffm(AffectionManager $affm)
    {
        $this->affm = $affm;
    }

    public function prePersist($object){
        $exp = ($this->affm->isExploitant())? $this->affm->getAffectation()->getExploitant(): null;
        /** @var \UploadBundle\Entity\Files $object */
        $object->setExploitant($exp);
    }

    public function getExploitantField()
    {
        return "exploitant";
    }

    public function getFormBuilder()
    {
        if($this->getSubject()->getId()){
            $this->formOptions['validation_groups'] = "edition";
        }else{
            $this->formOptions['validation_groups'] = "creation";
        }
        return parent::getFormBuilder();
    }

    protected function configureRoutes(RouteCollection $collection){
        $collection->add('auth','{files}/auth/{d}');
    }

    public function fileTypeSetting(array $dossier, array $groups){
        $this->fileType = [];
        foreach ($groups as $key => $group) {
            if($key === "new"){
                foreach ($group['dossiers'] as $type) {
                    $this->fileType[$type] = $dossier[$type];
                }
                $this->fileType[Files::NO_AUTH_FILE] = "Autres";
                return;
            }
        }

    }

}
