<?php

namespace UploadBundle\Controller;

use DossierBundle\Entity\Dossier;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\Request;
use UploadBundle\Entity\Files;
use UploadBundle\Form\AuthFilesType;



class FilesController extends CRUDController
{
    public function authAction(Request $request,Files $files,Dossier $d)
    {
        /** @var \Sonata\AdminBundle\Admin\AdminInterface $admin */
        $admin = $this->container->get('sonata.admin.pool')->getAdminByClass(get_class($d));

        if($d->isLocked()){
            $this->addFlash('sonata_flash_error', "Vous n'êtes plus autgoriser d'effectuer cette opération ");
            return $this->redirect($admin->generateUrl('list'));
        }

        if(!$this->isGranted('AUTH_FILE',$d))
            throw $this->createAccessDeniedException("vous n'êtes pas authoriser à effectuer cette opération");

        $this->admin->setSubject($files);
        $form = $this->createForm(new AuthFilesType(),$files);
        $form->handleRequest($request);

        if($form->isValid() && $form->isSubmitted()){
            /** @var \DossierBundle\Manager\DossierManager $dossier_manager */
            $dossier_manager = $this->get("dossier.manager.dossier");
            /** @var \UploadBundle\Service\FileAuthenticator $auth */
            $auth = $this->get("upload.service.authenticator");

            $auth->auth($files);
            if($d = $dossier_manager->submit($d))
                $this->addFlash('sonata_flash_success', sprintf('Le fichier "%s" a été authenifié !',$files));
            else
                $this->addFlash('flash_create_error', "Une erreur s'est produite lors de l'opération");

            return $this->redirect($admin->generateUrl('show',[$admin->getIdParameter() => $d->getId()]));
        }

        $view = $form->createView();
        $this->get('twig')->getExtension('form')->renderer->setTheme($view, $this->admin->getFormTheme());

        return $this->render('UploadBundle:CRUD:auth.html.twig',[
            "form" => $view,
            'object' => $files,
            'action' => 'delete',
        ]);
    }
}
