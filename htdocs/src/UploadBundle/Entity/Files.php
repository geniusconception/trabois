<?php

namespace UploadBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Vich\UploaderBundle\Naming\NamerInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
/**
 * Files
 *
 * @ORM\Table(name="files")
 * @ORM\Entity(repositoryClass="UploadBundle\Repository\FilesRepository")
 * @Vich\Uploadable
 * @ORM\HasLifecycleCallbacks()
 */
class Files
{
    const NO_AUTH_FILE = "NO_AUTH";
    const VALIDITY_UNDEFINED=1;
    const VALIDITY_DEFINED=2;
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

     /**
     *
     * @Assert\NotNull(groups={"creation"})
     * @Vich\UploadableField(mapping="document_attach", fileNameProperty="name")
     * 
     * @var File
     */
    private $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=10)
     */
    private $type;

    /**
     * @var string
     * @Assert\NotNull
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;
    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedAt;
    /**
     * @var \CorporateBundle\Entity\Entreprise
     * @ORM\ManyToOne(targetEntity="CorporateBundle\Entity\Entreprise")
     */
    private $exploitant;
    /**
     * @var boolean
     * @ORM\Column(name="authenticated",type="boolean",options={"default": false},nullable=false)
     */
    private $authenticated;
    /**
     * @var integer
     * @Assert\Choice(callback="getValidityValues",groups={"auth"})
     * @ORM\Column(name="validity",type="smallint",options={"default": 0},nullable=false)
     */
    private $validity;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $validityStart;
    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime",nullable=true)
     */
    private $validityEnd;

    public function __construct()
    {
        $this->date = new \Datetime();
        $this->updatedAt = new \DateTime();
        $this->authenticated = false;
        $this->validity = self::VALIDITY_UNDEFINED;
        $this->validityStart = null;
        $this->validityEnd = null;
        $this->type = self::NO_AUTH_FILE;
        return $this;
    }

    function __toString()
    {
        return ($this->getId())? $this->getTitre(): "n/a";
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * If manually uploading a file (i.e. not using Symfony Form) ensure an instance
     * of 'UploadedFile' is injected into this setter to trigger the  update. If this
     * bundle's configuration parameter 'inject_on_load' is set to 'true' this setter
     * must be able to accept an instance of 'File' as the bundle will inject one here
     * during Doctrine hydration.
     *
     * @param File|\Symfony\Component\HttpFoundation\File\UploadedFile $image
     *
     * @return Product
     */
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        if ($image) {
            $this->updatedAt = new \DateTime('now');
        }
        
        return $this;
    }

    /**
     * @return File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Files
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return Files
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set titre
     *
     * @param string $titre
     * @return Files
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string 
     */
    public function getTitre()
    {
        return $this->titre;
    }


    /**
     * Set comment
     *
     * @param string $comment
     * @return Files
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set exploitant
     *
     * @param \CorporateBundle\Entity\Entreprise $exploitant
     * @return Files
     */
    public function setExploitant(\CorporateBundle\Entity\Entreprise $exploitant)
    {
        $this->exploitant = $exploitant;

        return $this;
    }

    /**
     * Get exploitant
     *
     * @return \CorporateBundle\Entity\Entreprise 
     */
    public function getExploitant()
    {
        return $this->exploitant;
    }

    /**
     * @return boolean
     */
    public function isAuthenticated()
    {
        return $this->authenticated;
    }

    /**
     * @param boolean $authenticated
     */
    public function setAuthenticated($authenticated)
    {
        $this->authenticated = $authenticated;
    }

    /**
     * @return int
     */
    public function getValidity()
    {
        return $this->validity;
    }

    /**
     * @param int $validity
     */
    public function setValidity($validity)
    {
        $this->validity = $validity;
    }

    public static function getValidityList(){
        return [
            self::VALIDITY_UNDEFINED => "indéfinie",
            self::VALIDITY_DEFINED => "définie",
        ];
    }

    public static function getValidityValues(){
        return array_keys(self::getValidityList());
    }

    /**
     * @return \DateTime
     */
    public function getValidityEnd()
    {
        return $this->validityEnd;
    }

    /**
     * @param \DateTime $validityEnd
     */
    public function setValidityEnd($validityEnd)
    {
        $this->validityEnd = $validityEnd;
    }

    /**
     * @return \DateTime
     */
    public function getValidityStart()
    {
        return $this->validityStart;
    }

    /**
     * @param \DateTime $validityStart
     */
    public function setValidityStart($validityStart)
    {
        $this->validityStart = $validityStart;
    }

    public function isValid(){
        if(!$this->validity)
            return true;
        $today =  new \Datetime();
        return ($today >= $this->validityStart) && ($today <= $this->validityEnd);
    }

    /**
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context){
        if(! $this->isValid()){
            $context
                ->buildViolation("Ce document est hors de sa période de validité")
                ->atPath('name')
                ->addViolation()
            ;
        }
    }

    /**
     * @Assert\Callback(groups={"auth"})
     */
    public function authValidation(ExecutionContextInterface $context){
        if($this->validity == self::VALIDITY_DEFINED && is_null($this->validityStart)){
            $context
                ->buildViolation("Veuillez spécifier la date de début de validité")
                ->atPath("validityStart")
                ->addViolation()
            ;
        }
        if($this->validity == self::VALIDITY_DEFINED && is_null($this->validityEnd)){
            $context
                ->buildViolation("Veuillez spécifier la date de fin de validité")
                ->atPath("validityEnd")
                ->addViolation()
            ;
        }
        if($this->validityEnd < $this->validityStart){
            $context
                ->buildViolation("La fin de validité ne doit pas être antérieure à son début")
                ->atPath("validityEnd")
                ->addViolation()
            ;
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @ORM\PrePersist
     */
    public function authenticationSetting(){
        $this->authenticated = ($this->type === self::NO_AUTH_FILE);
    }
}
