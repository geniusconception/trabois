<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 09/08/2017
 * Time: 21:21
 */

namespace UploadBundle\Event;


use Symfony\Component\EventDispatcher\Event;
use UploadBundle\Entity\Files;

class FilesEvent extends Event{
    /**
     * @var Files
     */
    private $file;

    function __construct(Files $file)
    {
        $this->file = $file;
    }

    /**
     * @return Files
     */
    public function getFile()
    {
        return $this->file;
    }
} 