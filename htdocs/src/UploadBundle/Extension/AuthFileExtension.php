<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 09/08/2017
 * Time: 21:43
 */

namespace UploadBundle\Extension;


use DossierBundle\Admin\DossierAdmin;
use DossierBundle\Extension\AbstractDossierExtension;
use Sonata\AdminBundle\Form\FormMapper;
use Doctrine\ORM\EntityRepository;

class AuthFileExtension extends AbstractDossierExtension{

    private function isTypeSupported($type){
        return in_array($type,["AUTHPA","AUTHPAO","AUTHPS","AUTHPGP","AUTHRESE","AUTHPGQ"]);
    }

    public function configureDossierDataFormFields(FormMapper $mapper, $dossierType, DossierAdmin $admin)
    {
        if( $this->isTypeSupported($dossierType) ){
            /** @var \DossierBundle\Entity\Dossier $d */
            $exp = $admin->getExploitant();
            $mapper
                ->remove('annexes')
                ->add('data','sonata_type_immutable_array',[
                'label' => false,
                'keys'=>[
                    ['file','Symfony\Bridge\Doctrine\Form\Type\EntityType',[
                        'class' => 'UploadBundle:Files',
                        'get_property' => 'id',
                        'label' => 'Document à authentifier',
                        'query_builder' => function(EntityRepository $er) use ($exp){
                            return $er
                                ->createQueryBuilder('f')
                                ->leftJoin('f.exploitant','exp')
                                ->addSelect('exp')
                                ->where("f.authenticated = :auth AND exp = :exp")
                                ->setParameter('auth',false)
                                ->setParameter('exp',$exp)
                                ;
                        }
                    ]]
                ]
            ]);
        }
    }

    public function getTemplate($name, $type = null)
    {
        if($name == 'show' && $this->isTypeSupported($type))
            return "UploadBundle:File:auth.html.twig";
        return parent::getTemplate($name, $type);
    }


} 