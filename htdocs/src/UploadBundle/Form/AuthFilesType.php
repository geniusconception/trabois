<?php

namespace UploadBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UploadBundle\Entity\Files;

class AuthFilesType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('validity','sonata_type_choice_field_mask',[
                'label' => "Période de validité ?",
                'choices' => Files::getValidityList(),
                'map' => [
                    Files::VALIDITY_DEFINED => ["validityStart",'validityEnd'],
                ]
            ])
            ->add('validityStart','sonata_type_date_picker',['required' =>false,'label' => "Début de validité",'format' => 'y-MM-dd'])
            ->add('validityEnd','sonata_type_date_picker',['required' =>false,'label' => "Fin de validité",'format' => 'y-MM-dd'])
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UploadBundle\Entity\Files',
            'validation_groups' => 'auth'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'uploadbundle_files';
    }


}
