<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 10/08/2017
 * Time: 12:15
 */

namespace UploadBundle\Security\Authorization\Voter;

use AppBundle\Manager\AffectionManager;
use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Permission\PermissionMapInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

class AuthFileVoter extends AclVoter {

    /**
     * @var AffectionManager
     */
    private $affec;

    public function __construct(AffectionManager $affec,AclProviderInterface $aclProvider, ObjectIdentityRetrievalStrategyInterface $oidRetrievalStrategy, SecurityIdentityRetrievalStrategyInterface $sidRetrievalStrategy, PermissionMapInterface $permissionMap, LoggerInterface $logger = null, $allowIfObjectIdentityUnavailable = true)
    {
        parent::__construct($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap, $logger, $allowIfObjectIdentityUnavailable);
        $this->affec = $affec;
    }

    public function supportsClass($class)
    {
        return $class == 'DossierBundle\Entity\Dossier';
    }

    public function supportsAttribute($attribute){
        return in_array($attribute,["AUTH_FILE","VIEW_AUTH_FILE"]);
    }

    public function vote(TokenInterface $token, $object, array $attributes){
        if (!$this->supportsClass(get_class($object))) {
            return self::ACCESS_ABSTAIN;
        }
        /** @var \DossierBundle\Entity\Dossier  $object */
        if(strpos($object->getType(),"AUTH") !== 0)
            return self::ACCESS_ABSTAIN;

        $isExploitant = $this->affec->isExploitant();

        foreach ($attributes as $attribute) {
            if ($this->supportsAttribute($attribute)){
                if($attribute == "VIEW_AUTH_FILE"){
                    return ($isExploitant)? self::ACCESS_GRANTED:self::ACCESS_DENIED;
                }

                if((!$isExploitant) && $attribute == "AUTH_FILE"){
                    return ((!$object->isLocked()) && ($this->affec->isRole($object->getStep())) )? self::ACCESS_GRANTED: self::ACCESS_DENIED;
                }

                return self::ACCESS_DENIED;
            }
        }

       return self::ACCESS_ABSTAIN;
    }

} 