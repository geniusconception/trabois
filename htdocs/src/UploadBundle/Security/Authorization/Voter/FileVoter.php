<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 14/08/2017
 * Time: 16:55
 */

namespace UploadBundle\Security\Authorization\Voter;

use AppBundle\Manager\AffectionManager;
use Symfony\Component\Security\Acl\Voter\AclVoter;
use Symfony\Component\Security\Acl\Model\AclProviderInterface;
use Symfony\Component\Security\Acl\Model\ObjectIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Model\SecurityIdentityRetrievalStrategyInterface;
use Symfony\Component\Security\Acl\Permission\PermissionMapInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use UploadBundle\Entity\Files;

class FileVoter extends AclVoter{

    /**
     * @var AffectionManager
     */
    private $affec;

    public function __construct(AffectionManager $affec,AclProviderInterface $aclProvider, ObjectIdentityRetrievalStrategyInterface $oidRetrievalStrategy, SecurityIdentityRetrievalStrategyInterface $sidRetrievalStrategy, PermissionMapInterface $permissionMap, LoggerInterface $logger = null, $allowIfObjectIdentityUnavailable = true)
    {
        parent::__construct($aclProvider, $oidRetrievalStrategy, $sidRetrievalStrategy, $permissionMap, $logger, $allowIfObjectIdentityUnavailable);
        $this->affec = $affec;
    }

    public function supportsClass($class)
    {
        return $class == 'UploadBundle\Entity\Files' || $class == 'UploadBundle\Admin\FilesAdmin';
    }

    public function supportsAttribute($attribute){
        return in_array($attribute,["EDIT","LIST","DELETE"]);
    }

    public function vote(TokenInterface $token, $object, array $attributes){
        $class = get_class($object);
        if (!$this->supportsClass($class))
            return self::ACCESS_ABSTAIN;

        $isExploitant = $this->affec->isExploitant();

        foreach ($attributes as $attribute){
            if ($this->supportsAttribute($attribute)){
                if($attribute === "LIST" && $class == 'UploadBundle\Admin\FilesAdmin')
                    return ($isExploitant)? self::ACCESS_GRANTED:self::ACCESS_DENIED;
                elseif(in_array($attribute,["EDIT","DELETE"]) && $class == 'UploadBundle\Entity\Files')
                    return ($object->isAuthenticated() && $object->getType() !== Files::NO_AUTH_FILE)? self::ACCESS_DENIED:self::ACCESS_GRANTED;
            }
        }

        return self::ACCESS_ABSTAIN;
    }

} 