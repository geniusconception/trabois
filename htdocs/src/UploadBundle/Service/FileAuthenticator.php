<?php
/**
 * Created by PhpStorm.
 * User: Olivier
 * Date: 09/08/2017
 * Time: 21:14
 */

namespace UploadBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use UploadBundle\Entity\Files;
use UploadBundle\Event\FilesEvent;
use UploadBundle\Event\FilesEvents;

class FileAuthenticator {

    /**
     * @var EntityManagerInterface
     */
    private $em;

    private $dispatcher;

    function __construct($dispatcher,EntityManagerInterface $em)
    {
        $this->dispatcher = $dispatcher;
        $this->em = $em;
    }

    public function auth(Files $file){
        $file->setAuthenticated(true);
        $this->em->persist($file);
        $this->em->flush();
        $this->dispatcher->dispatch(FilesEvents::FILE_AUTHENTICATED,new FilesEvent($file));
        return $file;
    }


} 