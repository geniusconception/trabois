$(function() {
    //google.maps.event.addDomListener(window, 'load', initialize);
 });

	/**********************************************
	 * carte Google Maps
	 **********************************************/
		var map;
		var /*src = $("#map-canvas").data("kml");*/src = 'http://mica-ong.org/kml/kml/Limite_AAC15_Sud.kml';
			

	function initialize(){
 			//var myLatLng = new google.maps.LatLng(-33.872,151.252);{lat: 3.24472422, lng: 19.76303101}
			var mapOptions={
				zoom: 11,
				center: $("#map-canvas").data("centre"), 
				mapTypeId: google.maps.MapTypeId.SATELLITE
 			};
			var map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
			
		 // Define the LatLng coordinates for the outer path.
		  var outerCoords = $("#map-canvas").data("coords");
		  map.data.add({geometry: new google.maps.Data.MultiPoint(outerCoords)});
		  map.data.setStyle({
			    icon: $("#map-canvas").data("icon")
 		  });
		  loadKmlLayer(src, map);
	}

	function loadKmlLayer(src, map) {
        var kmlLayer = new google.maps.KmlLayer(src, {
          suppressInfoWindows: true,
          preserveViewport: false,
          map: map
        });
    }